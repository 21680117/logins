// export type AnimationType =
//   | "INSTANT"
//   | "MOVE_IN"
//   | "MOVE_OUT"
//   | "SLIDE_IN"
//   | "SLIDE_OUT"
//   | "PUSH"
//   | "DISSOLVE"
//   | "SMART_ANIMATE";

// export type Direction = "LEFT" | "RIGHT" | "TOP" | "BOTTOM";

export const getAnimationClass = (type, active, single,direction) => {
  let classString = "";
  switch (type) {
    case "MOVE_IN":
      classString += " move-animation";
      switch (direction) {
        case "LEFT":
          classString += active
            ? ` ${single ? "a" : "b"}-move-default`
            : ` ${single ? "a" : "b"}-move-right`;
          break;
        case "RIGHT":
          classString += active
            ? ` ${single ? "a" : "b"}-move-default`
            : ` ${single ? "a" : "b"}-move-left`;
          break;
        case "TOP":
          classString += active
            ? ` ${single ? "a" : "b"}-move-default`
            : ` ${single ? "a" : "b"}-move-bottom`;
          break;
        case "BOTTOM":
          classString += active
            ? ` ${single ? "a" : "b"}-move-default`
            : ` ${single ? "a" : "b"}-move-top`;
          break;
        default:
          classString += active
            ? ` ${single ? "a" : "b"}-move-default`
            : ` ${single ? "a" : "b"}-move-right`;
          break;
      }
      break;
    case "MOVE_OUT":
      classString += " move-animation a-element-front";
      switch (direction) {
        case "LEFT":
          classString += active ? " a-move-left" : " a-move-default";
          break;
        case "RIGHT":
          classString += active ? " a-move-right" : " a-move-default";
          break;
        case "TOP":
          classString += active ? " a-move-top" : " a-move-default";
          break;
        case "BOTTOM":
          classString += active ? " a-move-bottom" : " a-move-default";
          break;
        default:
          classString += active ? " a-move-left" : " a-move-default";
          break;
      }
      break;
    case "DISSOLVE":
      classString += " fade-animation";
      classString += active
        ? ` ${single ? "a" : "b"}-fade-in-end`
        : ` ${single ? "a" : "b"}-fade-in-start`;
      break;
    case "PUSH":
      classString += " move-animation";
      switch (direction) {
        case "LEFT":
          classString += active
            ? " a-move-left b-move-default"
            : " a-move-default b-move-right";
          break;
        case "RIGHT":
          classString += active
            ? " a-move-right b-move-default"
            : " a-move-default b-move-left";
          break;
        case "TOP":
          classString += active
            ? " a-move-top b-move-default"
            : " a-move-default b-move-bottom";
          break;
        case "BOTTOM":
          classString += active
            ? " a-move-bottom b-move-default"
            : " a-move-default b-move-top";
          break;
        default:
          classString += active
            ? " a-move-left b-move-default"
            : " a-move-default b-move-right";
          break;
      }
      break;
    case "SLIDE_IN":
      classString += " move-animation";
      switch (direction) {
        case "LEFT":
          classString += active
            ? " a-move-left-slow b-move-default"
            : " a-move-default b-move-right";
          break;
        case "RIGHT":
          classString += active
            ? " a-move-right-slow b-move-default"
            : " a-move-default b-move-left";
          break;
        case "TOP":
          classString += active
            ? " a-move-top-slow b-move-default"
            : " a-move-default b-move-bottom";
          break;
        case "BOTTOM":
          classString += active
            ? " a-move-bottom-slow b-move-default"
            : " a-move-default b-move-top";
          break;
        default:
          classString += active
            ? " a-move-left-slow b-move-default"
            : " a-move-default b-move-right";
          break;
      }
      break;
    case "SLIDE_OUT":
      classString += " move-animation a-element-front";
      switch (direction) {
        case "LEFT":
          classString += active
            ? " a-move-left b-move-default"
            : " a-move-default b-move-right-slow";
          break;
        case "RIGHT":
          classString += active
            ? " a-move-right b-move-default"
            : " a-move-default b-move-left-slow";
          break;
        case "TOP":
          classString += active
            ? " a-move-top b-move-default"
            : " a-move-default b-move-bottom-slow";
          break;
        case "BOTTOM":
          classString += active
            ? " a-move-bottom b-move-default"
            : " a-move-default b-move-top-slow";
          break;
        default:
          classString += active
            ? " a-move-left b-move-default"
            : " a-move-default b-move-right-slow";
          break;
      }
      break;
    case "SMART_ANIMATE":
      classString += " smart-animation";
      break;
    default:
      if (!single && !active) {
        classString += " a-element-front";
      }
      break;
  }
  return classString;
};
