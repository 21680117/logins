import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Desktopsix.css'





const Desktopsix = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        

        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition nodeRef={nodeRef} in={true} appear={true} timeout={10} classNames={SingletoneNavigation.getTransitionInstance()?.Desktopsix?.cssClass || '' }>

    <div id="id_oneoneone_sevenseven" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } desktopsix ${ props.cssClass } `} style={ { ...{ ...{}, transitionDuration: `${((SingletoneNavigation.getTransitionInstance()?.Desktopsix?.duration || 0) * 1000).toFixed(0)}ms` }, ...props.style }} onClick={ props.DesktopsixonClick } onMouseEnter={ props.DesktopsixonMouseEnter } onMouseOver={ props.DesktopsixonMouseOver } onKeyPress={ props.DesktopsixonKeyPress } onDrag={ props.DesktopsixonDrag } onMouseLeave={ props.DesktopsixonMouseLeave } onMouseUp={ props.DesktopsixonMouseUp } onMouseDown={ props.DesktopsixonMouseDown } onKeyDown={ props.DesktopsixonKeyDown } onChange={ props.DesktopsixonChange } ondelay={ props.Desktopsixondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} classNames={transaction['frameeight']?.animationClass || {}}>

          <div id="id_oneoneone_seveneight" className={` frame frameeight ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameeightStyle , transitionDuration: transaction['frameeight']?.duration, transitionTimingFunction: transaction['frameeight']?.timingFunction } } onClick={ props.FrameeightonClick } onMouseEnter={ props.FrameeightonMouseEnter } onMouseOver={ props.FrameeightonMouseOver } onKeyPress={ props.FrameeightonKeyPress } onDrag={ props.FrameeightonDrag } onMouseLeave={ props.FrameeightonMouseLeave } onMouseUp={ props.FrameeightonMouseUp } onMouseDown={ props.FrameeightonMouseDown } onKeyDown={ props.FrameeightonKeyDown } onChange={ props.FrameeightonChange } ondelay={ props.Frameeightondelay }>
            <CSSTransition in={_in} appear={true} classNames={transaction['colegiojaponesdemorelos']?.animationClass || {}}>

              <span id="id_oneoneone_sevennigth"  className={` text colegiojaponesdemorelos    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.COLEGIOJAPONESDEMORELOSStyle , transitionDuration: transaction['colegiojaponesdemorelos']?.duration, transitionTimingFunction: transaction['colegiojaponesdemorelos']?.timingFunction }} onClick={ props.COLEGIOJAPONESDEMORELOSonClick } onMouseEnter={ props.COLEGIOJAPONESDEMORELOSonMouseEnter } onMouseOver={ props.COLEGIOJAPONESDEMORELOSonMouseOver } onKeyPress={ props.COLEGIOJAPONESDEMORELOSonKeyPress } onDrag={ props.COLEGIOJAPONESDEMORELOSonDrag } onMouseLeave={ props.COLEGIOJAPONESDEMORELOSonMouseLeave } onMouseUp={ props.COLEGIOJAPONESDEMORELOSonMouseUp } onMouseDown={ props.COLEGIOJAPONESDEMORELOSonMouseDown } onKeyDown={ props.COLEGIOJAPONESDEMORELOSonKeyDown } onChange={ props.COLEGIOJAPONESDEMORELOSonChange } ondelay={ props.COLEGIOJAPONESDEMORELOSondelay } >{props.COLEGIOJAPONESDEMORELOS0 || `COLEGIO JAPONES DE MORELOS`}</span>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} classNames={transaction['framenigth']?.animationClass || {}}>

          <div id="id_oneoneone_eightzero" className={` frame framenigth ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FramenigthStyle , transitionDuration: transaction['framenigth']?.duration, transitionTimingFunction: transaction['framenigth']?.timingFunction } } onClick={ props.FramenigthonClick } onMouseEnter={ props.FramenigthonMouseEnter } onMouseOver={ props.FramenigthonMouseOver } onKeyPress={ props.FramenigthonKeyPress } onDrag={ props.FramenigthonDrag } onMouseLeave={ props.FramenigthonMouseLeave } onMouseUp={ props.FramenigthonMouseUp } onMouseDown={ props.FramenigthonMouseDown } onKeyDown={ props.FramenigthonKeyDown } onChange={ props.FramenigthonChange } ondelay={ props.Framenigthondelay }>
            <CSSTransition in={_in} appear={true} classNames={transaction['notojapanesebargainbutton']?.animationClass || {}}>

              <div id="id_oneoneone_eightone" className={` frame notojapanesebargainbutton ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.NotojapanesebargainbuttonStyle , transitionDuration: transaction['notojapanesebargainbutton']?.duration, transitionTimingFunction: transaction['notojapanesebargainbutton']?.timingFunction } } onClick={ props.NotojapanesebargainbuttononClick } onMouseEnter={ props.NotojapanesebargainbuttononMouseEnter } onMouseOver={ props.NotojapanesebargainbuttononMouseOver } onKeyPress={ props.NotojapanesebargainbuttononKeyPress } onDrag={ props.NotojapanesebargainbuttononDrag } onMouseLeave={ props.NotojapanesebargainbuttononMouseLeave } onMouseUp={ props.NotojapanesebargainbuttononMouseUp } onMouseDown={ props.NotojapanesebargainbuttononMouseDown } onKeyDown={ props.NotojapanesebargainbuttononKeyDown } onChange={ props.NotojapanesebargainbuttononChange } ondelay={ props.Notojapanesebargainbuttonondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_oneoneone_eighttwoo" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="84.375" height="84.375">

                    <path d="M42.1875 84.375C65.487 84.375 84.375 65.487 84.375 42.1875C84.375 18.888 65.487 0 42.1875 0C18.888 0 0 18.888 0 42.1875C0 65.487 18.888 84.375 42.1875 84.375Z" />
                  </svg>
                </CSSTransition>
                <CSSTransition in={_in} appear={true} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_oneoneone_eightthree" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="78.890625" height="78.890625">

                    <path d="M39.4453 78.8906C61.2304 78.8906 78.8906 61.2304 78.8906 39.4453C78.8906 17.6603 61.2304 0 39.4453 0C17.6603 0 0 17.6603 0 39.4453C0 61.2304 17.6603 78.8906 39.4453 78.8906Z" />
                  </svg>
                </CSSTransition>
                <CSSTransition in={_in} appear={true} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_oneoneone_eightfour" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="27.075927734375" height="23.75604248046875">

                    <path d="M3.52479 10.6523C6.68885 5.66016 13.4388 1.51172 20.4701 0.246093C22.2279 -0.0351565 23.9857 -0.175781 25.4623 0.386719C26.5873 0.808594 27.5014 1.86328 26.8685 3.05859C26.3764 4.04297 25.0404 4.46484 23.9857 4.81641C17.3899 6.9929 11.7023 11.2957 7.81385 17.0508C6.4076 19.1602 4.29822 24.9961 1.69666 23.5195C-1.04553 21.9023 -0.483026 16.8398 3.52479 10.6523L3.52479 10.6523Z" />
                  </svg>
                </CSSTransition>
                <CSSTransition in={_in} appear={true} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_oneoneone_eightfive" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="57.9921875" height="57.3336181640625">

                    <path d="M19.3198 16.9039C18.0541 18.8727 16.6479 20.9118 15.1713 22.8805C14.9745 23.1196 14.876 23.4149 14.8901 23.7243L14.8901 55.9274C14.8901 56.7008 14.2573 57.3336 13.4838 57.3336L10.601 57.3336C9.82758 57.3336 9.19477 56.7008 9.19477 55.9274L9.19477 33.2164C9.19202 33.0318 9.15292 32.8494 9.0797 32.6799C9.00647 32.5103 8.90057 32.3568 8.76802 32.2282C8.63548 32.0996 8.47889 31.9983 8.30721 31.9302C8.13552 31.8621 7.95211 31.8285 7.76743 31.8313C7.40883 31.8383 7.05727 31.9789 6.80415 32.2321C5.89008 33.1461 4.90571 33.9196 3.99165 34.7633C3.84998 34.8821 3.68628 34.9718 3.50991 35.0273C3.33354 35.0828 3.14796 35.1029 2.96379 35.0866C2.77962 35.0703 2.60048 35.0178 2.43661 34.9322C2.27275 34.8465 2.12738 34.7294 2.00883 34.5875C1.93149 34.4891 1.86118 34.3836 1.81196 34.2711C1.31977 33.2868 0.757272 32.0914 0.194772 31.1071C-0.163822 30.4953 -0.0161657 29.7149 0.546334 29.2789C5.95894 25.1628 10.621 20.1439 14.3276 14.443C14.6932 13.8453 15.4385 13.6063 16.0854 13.8805L18.6166 14.9352C19.3127 15.1672 19.6854 15.9196 19.4534 16.6157C19.4182 16.7211 19.376 16.8125 19.3198 16.9039ZM18.1948 3.40394C14.3658 8.2768 9.86837 12.5849 4.8354 16.2008C4.20962 16.6578 3.33071 16.5172 2.87368 15.8914C2.84555 15.8563 2.82446 15.8141 2.79633 15.7789C2.37446 15.0055 1.81196 14.1618 1.31977 13.3883C0.897897 12.7414 1.05258 11.8766 1.67133 11.4196C5.89008 8.46644 10.3901 4.318 12.9916 0.591438C13.3432 0.0640947 14.0182 -0.139812 14.6088 0.0992503L17.4916 1.22425C18.2299 1.45628 18.6448 2.25081 18.4057 2.98909C18.3565 3.13675 18.2862 3.27738 18.1948 3.40394ZM56.5854 40.5993L50.3979 40.5993C49.6245 40.5993 48.9916 41.2321 48.9916 42.0055L48.9916 51.2868C48.9916 54.1696 48.4291 55.6461 46.3198 56.4196C44.4213 57.2633 41.7495 57.3336 37.812 57.3336C37.2002 57.3336 36.6588 56.9328 36.476 56.3493C36.2651 55.6461 35.9838 54.8024 35.7026 54.0289C35.4284 53.3047 35.801 52.4891 36.5252 52.2219C36.687 52.1586 36.8627 52.1305 37.0385 52.1305C39.2885 52.2008 41.187 52.2008 41.8901 52.1305C42.8041 52.0602 43.0854 51.8493 43.0854 51.1461L43.0854 42.0055C43.0854 41.2321 42.4526 40.5993 41.6791 40.5993L20.2338 40.5993C19.4604 40.5993 18.8276 39.9664 18.8276 39.193L18.8276 36.943C18.8276 36.1696 19.4604 35.5367 20.2338 35.5367L41.6791 35.5367C42.4526 35.5367 43.0854 34.9039 43.0854 34.1305L43.0854 32.3024C43.0854 31.5289 42.4526 30.8961 41.6791 30.8961L21.7807 30.8961C21.0073 30.8961 20.3745 30.2633 20.3745 29.4899L20.3745 27.4508C20.3745 26.6774 21.0073 26.0446 21.7807 26.0446L55.3901 26.0446C56.1635 26.0446 56.7963 26.6774 56.7963 27.4508L56.7963 29.4899C56.7963 30.2633 56.1635 30.8961 55.3901 30.8961L50.3979 30.8961C49.6245 30.8961 48.9916 31.5289 48.9916 32.3024L48.9916 34.1305C48.9916 34.9039 49.6245 35.5367 50.3979 35.5367L56.5854 35.5367C57.3588 35.5367 57.9916 36.1696 57.9916 36.943L57.9916 39.193C58.0127 39.9453 57.4221 40.5782 56.6698 40.5993L56.5854 40.5993ZM28.6713 41.7946C30.8581 43.8125 32.876 45.9993 34.7182 48.3336C35.1893 48.9243 35.0909 49.7821 34.5073 50.2532C34.4868 50.2734 34.463 50.2901 34.437 50.3024L32.3276 51.7789C31.6948 52.2289 30.8159 52.0742 30.3659 51.4414C30.3659 51.4344 30.3588 51.4344 30.3588 51.4274C28.6576 49.1243 26.8018 46.9396 24.8041 44.8883C24.2487 44.3117 24.2698 43.4047 24.8463 42.8493C24.9026 42.8 24.9588 42.7508 25.0151 42.7086L26.8432 41.5133C27.4479 41.211 28.1791 41.3235 28.6713 41.7946ZM52.8588 22.5289L25.2963 22.5289C24.5229 22.5289 23.8901 21.8961 23.8901 21.1227L23.8901 2.84144C23.8901 2.068 24.5229 1.43519 25.2963 1.43519L52.8588 1.43519C53.6323 1.43519 54.2651 2.068 54.2651 2.84144L54.2651 21.1227C54.2651 21.4956 54.1169 21.8533 53.8532 22.1171C53.5895 22.3808 53.2318 22.5289 52.8588 22.5289ZM46.9526 5.72425L30.9916 5.72425C30.2182 5.72425 29.5854 6.35706 29.5854 7.1305L29.5854 8.53675C29.5854 9.31019 30.2182 9.943 30.9916 9.943L46.9526 9.943C47.726 9.943 48.3588 9.31019 48.3588 8.53675L48.3588 7.1305C48.3588 6.35706 47.726 5.72425 46.9526 5.72425ZM46.9526 13.9508L30.9916 13.9508C30.2182 13.9508 29.5854 14.5836 29.5854 15.3571L29.5854 16.7633C29.5854 17.5368 30.2182 18.1696 30.9916 18.1696L46.9526 18.1696C47.726 18.1696 48.3588 17.5368 48.3588 16.7633L48.3588 15.3571C48.3588 14.5836 47.726 13.9508 46.9526 13.9508Z" />
                  </svg>
                </CSSTransition>
                <CSSTransition in={_in} appear={true} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_oneoneone_eightsix" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="27.075927734375" height="23.75604248046875">

                    <path d="M3.52479 10.6523C6.68885 5.66016 13.4388 1.51172 20.4701 0.246093C22.2279 -0.0351565 23.9857 -0.175781 25.4623 0.386719C26.5873 0.808594 27.5014 1.86328 26.8685 3.05859C26.3764 4.04297 25.0404 4.46484 23.9857 4.81641C17.3899 6.9929 11.7023 11.2957 7.81385 17.0508C6.4076 19.1602 4.29822 24.9961 1.69666 23.5195C-1.04553 21.9023 -0.483026 16.8398 3.52479 10.6523L3.52479 10.6523Z" />
                  </svg>
                </CSSTransition>
              </div>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} classNames={transaction['frameonefour']?.animationClass || {}}>

          <div id="id_oneoneone_nigthzero" className={` frame frameonefour ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonefourStyle , transitionDuration: transaction['frameonefour']?.duration, transitionTimingFunction: transaction['frameonefour']?.timingFunction } } onClick={ props.FrameonefouronClick } onMouseEnter={ props.FrameonefouronMouseEnter } onMouseOver={ props.FrameonefouronMouseOver } onKeyPress={ props.FrameonefouronKeyPress } onDrag={ props.FrameonefouronDrag } onMouseLeave={ props.FrameonefouronMouseLeave } onMouseUp={ props.FrameonefouronMouseUp } onMouseDown={ props.FrameonefouronMouseDown } onKeyDown={ props.FrameonefouronKeyDown } onChange={ props.FrameonefouronChange } ondelay={ props.Frameonefourondelay }>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameoneeight']?.animationClass || {}}>

              <div id="id_oneoneone_nigthone" className={` frame frameoneeight ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameoneeightStyle , transitionDuration: transaction['frameoneeight']?.duration, transitionTimingFunction: transaction['frameoneeight']?.timingFunction } } onClick={ props.FrameoneeightonClick } onMouseEnter={ props.FrameoneeightonMouseEnter } onMouseOver={ props.FrameoneeightonMouseOver } onKeyPress={ props.FrameoneeightonKeyPress } onDrag={ props.FrameoneeightonDrag } onMouseLeave={ props.FrameoneeightonMouseLeave } onMouseUp={ props.FrameoneeightonMouseUp } onMouseDown={ props.FrameoneeightonMouseDown } onKeyDown={ props.FrameoneeightonKeyDown } onChange={ props.FrameoneeightonChange } ondelay={ props.Frameoneeightondelay }>

              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameoneseven']?.animationClass || {}}>

              <div id="id_oneoneone_oneoneeight" className={` frame frameoneseven ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonesevenStyle , transitionDuration: transaction['frameoneseven']?.duration, transitionTimingFunction: transaction['frameoneseven']?.timingFunction } } onClick={ props.FrameonesevenonClick } onMouseEnter={ props.FrameonesevenonMouseEnter } onMouseOver={ props.FrameonesevenonMouseOver } onKeyPress={ props.FrameonesevenonKeyPress } onDrag={ props.FrameonesevenonDrag } onMouseLeave={ props.FrameonesevenonMouseLeave } onMouseUp={ props.FrameonesevenonMouseUp } onMouseDown={ props.FrameonesevenonMouseDown } onKeyDown={ props.FrameonesevenonKeyDown } onChange={ props.FrameonesevenonChange } ondelay={ props.Frameonesevenondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['usuario']?.animationClass || {}}>

                  <span id="id_oneoneone_oneonenigth"  className={` text usuario    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.USUARIOStyle , transitionDuration: transaction['usuario']?.duration, transitionTimingFunction: transaction['usuario']?.timingFunction }} onClick={ props.USUARIOonClick } onMouseEnter={ props.USUARIOonMouseEnter } onMouseOver={ props.USUARIOonMouseOver } onKeyPress={ props.USUARIOonKeyPress } onDrag={ props.USUARIOonDrag } onMouseLeave={ props.USUARIOonMouseLeave } onMouseUp={ props.USUARIOonMouseUp } onMouseDown={ props.USUARIOonMouseDown } onKeyDown={ props.USUARIOonKeyDown } onChange={ props.USUARIOonChange } ondelay={ props.USUARIOondelay } >{props.USUARIO0 || `USUARIO`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frametwoofour']?.animationClass || {}}>

              <div id="id_oneoneone_onetwoozero" className={` frame frametwoofour ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrametwoofourStyle , transitionDuration: transaction['frametwoofour']?.duration, transitionTimingFunction: transaction['frametwoofour']?.timingFunction } } onClick={ props.FrametwoofouronClick } onMouseEnter={ props.FrametwoofouronMouseEnter } onMouseOver={ props.FrametwoofouronMouseOver } onKeyPress={ props.FrametwoofouronKeyPress } onDrag={ props.FrametwoofouronDrag } onMouseLeave={ props.FrametwoofouronMouseLeave } onMouseUp={ props.FrametwoofouronMouseUp } onMouseDown={ props.FrametwoofouronMouseDown } onKeyDown={ props.FrametwoofouronKeyDown } onChange={ props.FrametwoofouronChange } ondelay={ props.Frametwoofourondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['ingresar']?.animationClass || {}}>

                  <span id="id_oneoneone_onetwooone"  className={` text ingresar    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.INGRESARStyle , transitionDuration: transaction['ingresar']?.duration, transitionTimingFunction: transaction['ingresar']?.timingFunction }} onClick={ props.INGRESARonClick } onMouseEnter={ props.INGRESARonMouseEnter } onMouseOver={ props.INGRESARonMouseOver } onKeyPress={ props.INGRESARonKeyPress } onDrag={ props.INGRESARonDrag } onMouseLeave={ props.INGRESARonMouseLeave } onMouseUp={ props.INGRESARonMouseUp } onMouseDown={ props.INGRESARonMouseDown } onKeyDown={ props.INGRESARonKeyDown } onChange={ props.INGRESARonChange } ondelay={ props.INGRESARondelay } >{props.INGRESAR0 || `INGRESAR`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frametwoofive']?.animationClass || {}}>

              <div id="id_oneoneone_onetwootwoo" className={` frame frametwoofive ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrametwoofiveStyle , transitionDuration: transaction['frametwoofive']?.duration, transitionTimingFunction: transaction['frametwoofive']?.timingFunction } } onClick={ props.FrametwoofiveonClick } onMouseEnter={ props.FrametwoofiveonMouseEnter } onMouseOver={ props.FrametwoofiveonMouseOver } onKeyPress={ props.FrametwoofiveonKeyPress } onDrag={ props.FrametwoofiveonDrag } onMouseLeave={ props.FrametwoofiveonMouseLeave } onMouseUp={ props.FrametwoofiveonMouseUp } onMouseDown={ props.FrametwoofiveonMouseDown } onKeyDown={ props.FrametwoofiveonKeyDown } onChange={ props.FrametwoofiveonChange } ondelay={ props.Frametwoofiveondelay }>

              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameoneeight']?.animationClass || {}}>

              <div id="id_oneoneone_onetwoosix" className={` frame frameoneeight ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameoneeightStyle , transitionDuration: transaction['frameoneeight']?.duration, transitionTimingFunction: transaction['frameoneeight']?.timingFunction } } onClick={ props.FrameoneeightonClick } onMouseEnter={ props.FrameoneeightonMouseEnter } onMouseOver={ props.FrameoneeightonMouseOver } onKeyPress={ props.FrameoneeightonKeyPress } onDrag={ props.FrameoneeightonDrag } onMouseLeave={ props.FrameoneeightonMouseLeave } onMouseUp={ props.FrameoneeightonMouseUp } onMouseDown={ props.FrameoneeightonMouseDown } onKeyDown={ props.FrameoneeightonKeyDown } onChange={ props.FrameoneeightonChange } ondelay={ props.Frameoneeightondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['id']?.animationClass || {}}>

                  <span id="id_oneoneone_onetwooseven"  className={` text id    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.IDStyle , transitionDuration: transaction['id']?.duration, transitionTimingFunction: transaction['id']?.timingFunction }} onClick={ props.IDonClick } onMouseEnter={ props.IDonMouseEnter } onMouseOver={ props.IDonMouseOver } onKeyPress={ props.IDonKeyPress } onDrag={ props.IDonDrag } onMouseLeave={ props.IDonMouseLeave } onMouseUp={ props.IDonMouseUp } onMouseDown={ props.IDonMouseDown } onKeyDown={ props.IDonKeyDown } onChange={ props.IDonChange } ondelay={ props.IDondelay } >{props.ID0 || `ID`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameoneeight']?.animationClass || {}}>

              <div id="id_oneoneone_onetwoofive" className={` frame frameoneeight ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameoneeightStyle , transitionDuration: transaction['frameoneeight']?.duration, transitionTimingFunction: transaction['frameoneeight']?.timingFunction } } onClick={ props.FrameoneeightonClick } onMouseEnter={ props.FrameoneeightonMouseEnter } onMouseOver={ props.FrameoneeightonMouseOver } onKeyPress={ props.FrameoneeightonKeyPress } onDrag={ props.FrameoneeightonDrag } onMouseLeave={ props.FrameoneeightonMouseLeave } onMouseUp={ props.FrameoneeightonMouseUp } onMouseDown={ props.FrameoneeightonMouseDown } onKeyDown={ props.FrameoneeightonKeyDown } onChange={ props.FrameoneeightonChange } ondelay={ props.Frameoneeightondelay }>

              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frametwooseven']?.animationClass || {}}>

              <div id="id_oneoneone_onetwooeight" className={` frame frametwooseven ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrametwoosevenStyle , transitionDuration: transaction['frametwooseven']?.duration, transitionTimingFunction: transaction['frametwooseven']?.timingFunction } } onClick={ props.FrametwoosevenonClick } onMouseEnter={ props.FrametwoosevenonMouseEnter } onMouseOver={ props.FrametwoosevenonMouseOver } onKeyPress={ props.FrametwoosevenonKeyPress } onDrag={ props.FrametwoosevenonDrag } onMouseLeave={ props.FrametwoosevenonMouseLeave } onMouseUp={ props.FrametwoosevenonMouseUp } onMouseDown={ props.FrametwoosevenonMouseDown } onKeyDown={ props.FrametwoosevenonKeyDown } onChange={ props.FrametwoosevenonChange } ondelay={ props.Frametwoosevenondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['buscar']?.animationClass || {}}>

                  <span id="id_oneoneone_onetwoonigth"  className={` text buscar    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.BUSCARStyle , transitionDuration: transaction['buscar']?.duration, transitionTimingFunction: transaction['buscar']?.timingFunction }} onClick={ props.BUSCARonClick } onMouseEnter={ props.BUSCARonMouseEnter } onMouseOver={ props.BUSCARonMouseOver } onKeyPress={ props.BUSCARonKeyPress } onDrag={ props.BUSCARonDrag } onMouseLeave={ props.BUSCARonMouseLeave } onMouseUp={ props.BUSCARonMouseUp } onMouseDown={ props.BUSCARonMouseDown } onKeyDown={ props.BUSCARonKeyDown } onChange={ props.BUSCARonChange } ondelay={ props.BUSCARondelay } >{props.BUSCAR0 || `BUSCAR`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frametwooeight']?.animationClass || {}}>

              <div id="id_oneoneone_onethreezero" className={` frame frametwooeight ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrametwooeightStyle , transitionDuration: transaction['frametwooeight']?.duration, transitionTimingFunction: transaction['frametwooeight']?.timingFunction } } onClick={ props.FrametwooeightonClick } onMouseEnter={ props.FrametwooeightonMouseEnter } onMouseOver={ props.FrametwooeightonMouseOver } onKeyPress={ props.FrametwooeightonKeyPress } onDrag={ props.FrametwooeightonDrag } onMouseLeave={ props.FrametwooeightonMouseLeave } onMouseUp={ props.FrametwooeightonMouseUp } onMouseDown={ props.FrametwooeightonMouseDown } onKeyDown={ props.FrametwooeightonKeyDown } onChange={ props.FrametwooeightonChange } ondelay={ props.Frametwooeightondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['apellido']?.animationClass || {}}>

                  <span id="id_oneoneone_onethreefive"  className={` text apellido    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.APELLIDOStyle , transitionDuration: transaction['apellido']?.duration, transitionTimingFunction: transaction['apellido']?.timingFunction }} onClick={ props.APELLIDOonClick } onMouseEnter={ props.APELLIDOonMouseEnter } onMouseOver={ props.APELLIDOonMouseOver } onKeyPress={ props.APELLIDOonKeyPress } onDrag={ props.APELLIDOonDrag } onMouseLeave={ props.APELLIDOonMouseLeave } onMouseUp={ props.APELLIDOonMouseUp } onMouseDown={ props.APELLIDOonMouseDown } onKeyDown={ props.APELLIDOonKeyDown } onChange={ props.APELLIDOonChange } ondelay={ props.APELLIDOondelay } >{props.APELLIDO0 || `APELLIDO`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['framethreeone']?.animationClass || {}}>

              <div id="id_oneonezero_threetwooeight" className={` frame framethreeone ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FramethreeoneStyle , transitionDuration: transaction['framethreeone']?.duration, transitionTimingFunction: transaction['framethreeone']?.timingFunction } } onClick={ props.FramethreeoneonClick } onMouseEnter={ props.FramethreeoneonMouseEnter } onMouseOver={ props.FramethreeoneonMouseOver } onKeyPress={ props.FramethreeoneonKeyPress } onDrag={ props.FramethreeoneonDrag } onMouseLeave={ props.FramethreeoneonMouseLeave } onMouseUp={ props.FramethreeoneonMouseUp } onMouseDown={ props.FramethreeoneonMouseDown } onKeyDown={ props.FramethreeoneonKeyDown } onChange={ props.FramethreeoneonChange } ondelay={ props.Framethreeoneondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['puesto']?.animationClass || {}}>

                  <span id="id_oneonezero_threetwoonigth"  className={` text puesto    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.PUESTOStyle , transitionDuration: transaction['puesto']?.duration, transitionTimingFunction: transaction['puesto']?.timingFunction }} onClick={ props.PUESTOonClick } onMouseEnter={ props.PUESTOonMouseEnter } onMouseOver={ props.PUESTOonMouseOver } onKeyPress={ props.PUESTOonKeyPress } onDrag={ props.PUESTOonDrag } onMouseLeave={ props.PUESTOonMouseLeave } onMouseUp={ props.PUESTOonMouseUp } onMouseDown={ props.PUESTOonMouseDown } onKeyDown={ props.PUESTOonKeyDown } onChange={ props.PUESTOonChange } ondelay={ props.PUESTOondelay } >{props.PUESTO0 || `PUESTO`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frametwoonigth']?.animationClass || {}}>

              <div id="id_oneoneone_onethreetwoo" className={` frame frametwoonigth ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrametwoonigthStyle , transitionDuration: transaction['frametwoonigth']?.duration, transitionTimingFunction: transaction['frametwoonigth']?.timingFunction } } onClick={ props.FrametwoonigthonClick } onMouseEnter={ props.FrametwoonigthonMouseEnter } onMouseOver={ props.FrametwoonigthonMouseOver } onKeyPress={ props.FrametwoonigthonKeyPress } onDrag={ props.FrametwoonigthonDrag } onMouseLeave={ props.FrametwoonigthonMouseLeave } onMouseUp={ props.FrametwoonigthonMouseUp } onMouseDown={ props.FrametwoonigthonMouseDown } onKeyDown={ props.FrametwoonigthonKeyDown } onChange={ props.FrametwoonigthonChange } ondelay={ props.Frametwoonigthondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['nombre']?.animationClass || {}}>

                  <span id="id_oneoneone_onethreethree"  className={` text nombre    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.NOMBREStyle , transitionDuration: transaction['nombre']?.duration, transitionTimingFunction: transaction['nombre']?.timingFunction }} onClick={ props.NOMBREonClick } onMouseEnter={ props.NOMBREonMouseEnter } onMouseOver={ props.NOMBREonMouseOver } onKeyPress={ props.NOMBREonKeyPress } onDrag={ props.NOMBREonDrag } onMouseLeave={ props.NOMBREonMouseLeave } onMouseUp={ props.NOMBREonMouseUp } onMouseDown={ props.NOMBREonMouseDown } onKeyDown={ props.NOMBREonKeyDown } onChange={ props.NOMBREonChange } ondelay={ props.NOMBREondelay } >{props.NOMBRE0 || `NOMBRE`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['framethreezero']?.animationClass || {}}>

              <div id="id_oneoneone_onethreesix" className={` frame framethreezero ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FramethreezeroStyle , transitionDuration: transaction['framethreezero']?.duration, transitionTimingFunction: transaction['framethreezero']?.timingFunction } } onClick={ props.FramethreezeroonClick } onMouseEnter={ props.FramethreezeroonMouseEnter } onMouseOver={ props.FramethreezeroonMouseOver } onKeyPress={ props.FramethreezeroonKeyPress } onDrag={ props.FramethreezeroonDrag } onMouseLeave={ props.FramethreezeroonMouseLeave } onMouseUp={ props.FramethreezeroonMouseUp } onMouseDown={ props.FramethreezeroonMouseDown } onKeyDown={ props.FramethreezeroonKeyDown } onChange={ props.FramethreezeroonChange } ondelay={ props.Framethreezeroondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['frameoneeight']?.animationClass || {}}>

                  <div id="id_oneoneone_onethreeseven" className={` frame frameoneeight ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameoneeightStyle , transitionDuration: transaction['frameoneeight']?.duration, transitionTimingFunction: transaction['frameoneeight']?.timingFunction } } onClick={ props.FrameoneeightonClick } onMouseEnter={ props.FrameoneeightonMouseEnter } onMouseOver={ props.FrameoneeightonMouseOver } onKeyPress={ props.FrameoneeightonKeyPress } onDrag={ props.FrameoneeightonDrag } onMouseLeave={ props.FrameoneeightonMouseLeave } onMouseUp={ props.FrameoneeightonMouseUp } onMouseDown={ props.FrameoneeightonMouseDown } onKeyDown={ props.FrameoneeightonKeyDown } onChange={ props.FrameoneeightonChange } ondelay={ props.Frameoneeightondelay }>

                  </div>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameoneeight']?.animationClass || {}}>

              <div id="id_oneoneone_onethreenigth" className={` frame frameoneeight ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameoneeightStyle , transitionDuration: transaction['frameoneeight']?.duration, transitionTimingFunction: transaction['frameoneeight']?.timingFunction } } onClick={ props.FrameoneeightonClick } onMouseEnter={ props.FrameoneeightonMouseEnter } onMouseOver={ props.FrameoneeightonMouseOver } onKeyPress={ props.FrameoneeightonKeyPress } onDrag={ props.FrameoneeightonDrag } onMouseLeave={ props.FrameoneeightonMouseLeave } onMouseUp={ props.FrameoneeightonMouseUp } onMouseDown={ props.FrameoneeightonMouseDown } onKeyDown={ props.FrameoneeightonKeyDown } onChange={ props.FrameoneeightonChange } ondelay={ props.Frameoneeightondelay }>

              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['framethreetwoo']?.animationClass || {}}>

              <div id="id_oneonezero_threethreezero" className={` frame framethreetwoo ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FramethreetwooStyle , transitionDuration: transaction['framethreetwoo']?.duration, transitionTimingFunction: transaction['framethreetwoo']?.timingFunction } } onClick={ props.FramethreetwooonClick } onMouseEnter={ props.FramethreetwooonMouseEnter } onMouseOver={ props.FramethreetwooonMouseOver } onKeyPress={ props.FramethreetwooonKeyPress } onDrag={ props.FramethreetwooonDrag } onMouseLeave={ props.FramethreetwooonMouseLeave } onMouseUp={ props.FramethreetwooonMouseUp } onMouseDown={ props.FramethreetwooonMouseDown } onKeyDown={ props.FramethreetwooonKeyDown } onChange={ props.FramethreetwooonChange } ondelay={ props.Framethreetwooondelay }>

              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frametwooseven']?.animationClass || {}}>

              <div id="id_oneonezero_threefoursix" className={` frame frametwooseven ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrametwoosevenStyle , transitionDuration: transaction['frametwooseven']?.duration, transitionTimingFunction: transaction['frametwooseven']?.timingFunction } } onClick={ props.FrametwoosevenonClick } onMouseEnter={ props.FrametwoosevenonMouseEnter } onMouseOver={ props.FrametwoosevenonMouseOver } onKeyPress={ props.FrametwoosevenonKeyPress } onDrag={ props.FrametwoosevenonDrag } onMouseLeave={ props.FrametwoosevenonMouseLeave } onMouseUp={ props.FrametwoosevenonMouseUp } onMouseDown={ props.FrametwoosevenonMouseDown } onKeyDown={ props.FrametwoosevenonKeyDown } onChange={ props.FrametwoosevenonChange } ondelay={ props.Frametwoosevenondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['onetwooonefourfivenigthfourone']?.animationClass || {}}>
                  <img id="id_oneonezero_threefourseven" className={` rectangle onetwooonefourfivenigthfourone ${ props.onClick ? 'cursor' : '' } `} style={{ ...{},  ...props.onetwooonefourfivenigthfouroneStyle , transitionDuration: transaction['onetwooonefourfivenigthfourone']?.duration, transitionTimingFunction: transaction['onetwooonefourfivenigthfourone']?.timingFunction }} onClick={ props.OnetwooonefourfivenigthfouroneonClick } onMouseEnter={ props.OnetwooonefourfivenigthfouroneonMouseEnter } onMouseOver={ props.OnetwooonefourfivenigthfouroneonMouseOver } onKeyPress={ props.OnetwooonefourfivenigthfouroneonKeyPress } onDrag={ props.OnetwooonefourfivenigthfouroneonDrag } onMouseLeave={ props.OnetwooonefourfivenigthfouroneonMouseLeave } onMouseUp={ props.OnetwooonefourfivenigthfouroneonMouseUp } onMouseDown={ props.OnetwooonefourfivenigthfouroneonMouseDown } onKeyDown={ props.OnetwooonefourfivenigthfouroneonKeyDown } onChange={ props.OnetwooonefourfivenigthfouroneonChange } ondelay={ props.Onetwooonefourfivenigthfouroneondelay } src={props.Onetwooonefourfivenigthfourone0 || "https://cdn.uncodie.com/6u2XSDo7Ms6u8T8UBz7LVQ/12457674480f3e27b5ad555034d20ed7813a9c95.png" } />
                </CSSTransition>
                <CSSTransition in={_in} appear={true} classNames={transaction['idnombreapellidopuestoeliminar']?.animationClass || {}}>

                  <span id="id_oneonezero_threefournigth"  className={` text idnombreapellidopuestoeliminar    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.IDNOMBREAPELLIDOPUESTOELIMINARStyle , transitionDuration: transaction['idnombreapellidopuestoeliminar']?.duration, transitionTimingFunction: transaction['idnombreapellidopuestoeliminar']?.timingFunction }} onClick={ props.IDNOMBREAPELLIDOPUESTOELIMINARonClick } onMouseEnter={ props.IDNOMBREAPELLIDOPUESTOELIMINARonMouseEnter } onMouseOver={ props.IDNOMBREAPELLIDOPUESTOELIMINARonMouseOver } onKeyPress={ props.IDNOMBREAPELLIDOPUESTOELIMINARonKeyPress } onDrag={ props.IDNOMBREAPELLIDOPUESTOELIMINARonDrag } onMouseLeave={ props.IDNOMBREAPELLIDOPUESTOELIMINARonMouseLeave } onMouseUp={ props.IDNOMBREAPELLIDOPUESTOELIMINARonMouseUp } onMouseDown={ props.IDNOMBREAPELLIDOPUESTOELIMINARonMouseDown } onKeyDown={ props.IDNOMBREAPELLIDOPUESTOELIMINARonKeyDown } onChange={ props.IDNOMBREAPELLIDOPUESTOELIMINARonChange } ondelay={ props.IDNOMBREAPELLIDOPUESTOELIMINARondelay } >{props.IDNOMBREAPELLIDOPUESTOELIMINAR0 || `ID            NOMBRE             APELLIDO              PUESTO           ELIMINAR`}</span>

                </CSSTransition>
                <CSSTransition in={_in} appear={true} classNames={transaction['onefulanitoveradocente']?.animationClass || {}}>

                  <span id="id_oneonezero_threefivezero"  className={` text onefulanitoveradocente    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.oneFULANITOVERADocenteStyle , transitionDuration: transaction['onefulanitoveradocente']?.duration, transitionTimingFunction: transaction['onefulanitoveradocente']?.timingFunction }} onClick={ props.OneFULANITOVERADocenteonClick } onMouseEnter={ props.OneFULANITOVERADocenteonMouseEnter } onMouseOver={ props.OneFULANITOVERADocenteonMouseOver } onKeyPress={ props.OneFULANITOVERADocenteonKeyPress } onDrag={ props.OneFULANITOVERADocenteonDrag } onMouseLeave={ props.OneFULANITOVERADocenteonMouseLeave } onMouseUp={ props.OneFULANITOVERADocenteonMouseUp } onMouseDown={ props.OneFULANITOVERADocenteonMouseDown } onKeyDown={ props.OneFULANITOVERADocenteonKeyDown } onChange={ props.OneFULANITOVERADocenteonChange } ondelay={ props.OneFULANITOVERADocenteondelay } >{props.OneFULANITOVERADocente0 || `1            FULANITO             VERA               Docente`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} classNames={transaction['frametwoofour']?.animationClass || {}}>

          <div id="id_oneonezero_threeonesix" className={` frame frametwoofour ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrametwoofourStyle , transitionDuration: transaction['frametwoofour']?.duration, transitionTimingFunction: transaction['frametwoofour']?.timingFunction } } onClick={ props.FrametwoofouronClick } onMouseEnter={ props.FrametwoofouronMouseEnter } onMouseOver={ props.FrametwoofouronMouseOver } onKeyPress={ props.FrametwoofouronKeyPress } onDrag={ props.FrametwoofouronDrag } onMouseLeave={ props.FrametwoofouronMouseLeave } onMouseUp={ props.FrametwoofouronMouseUp } onMouseDown={ props.FrametwoofouronMouseDown } onKeyDown={ props.FrametwoofouronKeyDown } onChange={ props.FrametwoofouronChange } ondelay={ props.Frametwoofourondelay }>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameonetwoo']?.animationClass || {}}>

              <div id="id_oneonezero_threeoneseven" className={` frame frameonetwoo ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonetwooStyle , transitionDuration: transaction['frameonetwoo']?.duration, transitionTimingFunction: transaction['frameonetwoo']?.timingFunction } } onClick={ props.FrameonetwooonClick } onMouseEnter={ props.FrameonetwooonMouseEnter } onMouseOver={ props.FrameonetwooonMouseOver } onKeyPress={ props.FrameonetwooonKeyPress } onDrag={ props.FrameonetwooonDrag } onMouseLeave={ props.FrameonetwooonMouseLeave } onMouseUp={ props.FrameonetwooonMouseUp } onMouseDown={ props.FrameonetwooonMouseDown } onKeyDown={ props.FrameonetwooonKeyDown } onChange={ props.FrameonetwooonChange } ondelay={ props.Frameonetwooondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['reportes']?.animationClass || {}}>

                  <span id="id_oneonezero_threeoneeight"  className={` text reportes    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.REPORTESStyle , transitionDuration: transaction['reportes']?.duration, transitionTimingFunction: transaction['reportes']?.timingFunction }} onClick={ props.REPORTESonClick } onMouseEnter={ props.REPORTESonMouseEnter } onMouseOver={ props.REPORTESonMouseOver } onKeyPress={ props.REPORTESonKeyPress } onDrag={ props.REPORTESonDrag } onMouseLeave={ props.REPORTESonMouseLeave } onMouseUp={ props.REPORTESonMouseUp } onMouseDown={ props.REPORTESonMouseDown } onKeyDown={ props.REPORTESonKeyDown } onChange={ props.REPORTESonChange } ondelay={ props.REPORTESondelay } >{props.REPORTES0 || `REPORTES`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameonefour']?.animationClass || {}}>

              <div id="id_oneonezero_threeonenigth" className={` frame frameonefour cursor ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonefourStyle , transitionDuration: transaction['frameonefour']?.duration, transitionTimingFunction: transaction['frameonefour']?.timingFunction } } onClick={ props.FrameonefouronClick || function(e){ const offset=0; window.scrollTo({ behavior: "smooth" , top: document.getElementById("id_oneoneone_nigthzero").getBoundingClientRect().top - document.body.getBoundingClientRect().top - offset }); }} onMouseEnter={ props.FrameonefouronMouseEnter } onMouseOver={ props.FrameonefouronMouseOver } onKeyPress={ props.FrameonefouronKeyPress } onDrag={ props.FrameonefouronDrag } onMouseLeave={ props.FrameonefouronMouseLeave } onMouseUp={ props.FrameonefouronMouseUp } onMouseDown={ props.FrameonefouronMouseDown } onKeyDown={ props.FrameonefouronKeyDown } onChange={ props.FrameonefouronChange } ondelay={ props.Frameonefourondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['usuarios']?.animationClass || {}}>

                  <span id="id_oneonezero_threetwoozero"  className={` text usuarios    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.USUARIOSStyle , transitionDuration: transaction['usuarios']?.duration, transitionTimingFunction: transaction['usuarios']?.timingFunction }} onClick={ props.USUARIOSonClick } onMouseEnter={ props.USUARIOSonMouseEnter } onMouseOver={ props.USUARIOSonMouseOver } onKeyPress={ props.USUARIOSonKeyPress } onDrag={ props.USUARIOSonDrag } onMouseLeave={ props.USUARIOSonMouseLeave } onMouseUp={ props.USUARIOSonMouseUp } onMouseDown={ props.USUARIOSonMouseDown } onKeyDown={ props.USUARIOSonKeyDown } onChange={ props.USUARIOSonChange } ondelay={ props.USUARIOSondelay } >{props.USUARIOS0 || `USUARIOS`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameonetwoo']?.animationClass || {}}>

              <div id="id_oneonezero_threetwooone" className={` frame frameonetwoo cursor ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonetwooStyle , transitionDuration: transaction['frameonetwoo']?.duration, transitionTimingFunction: transaction['frameonetwoo']?.timingFunction } } onClick={ props.FrameonetwooonClick || function(e){ SingletoneNavigation.setTransitionInstance(null, 'Desktopone' ); history.push('/'); }} onMouseEnter={ props.FrameonetwooonMouseEnter } onMouseOver={ props.FrameonetwooonMouseOver } onKeyPress={ props.FrameonetwooonKeyPress } onDrag={ props.FrameonetwooonDrag } onMouseLeave={ props.FrameonetwooonMouseLeave } onMouseUp={ props.FrameonetwooonMouseUp } onMouseDown={ props.FrameonetwooonMouseDown } onKeyDown={ props.FrameonetwooonKeyDown } onChange={ props.FrameonetwooonChange } ondelay={ props.Frameonetwooondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['salir']?.animationClass || {}}>

                  <span id="id_oneonezero_threetwootwoo"  className={` text salir    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.SALIRStyle , transitionDuration: transaction['salir']?.duration, transitionTimingFunction: transaction['salir']?.timingFunction }} onClick={ props.SALIRonClick } onMouseEnter={ props.SALIRonMouseEnter } onMouseOver={ props.SALIRonMouseOver } onKeyPress={ props.SALIRonKeyPress } onDrag={ props.SALIRonDrag } onMouseLeave={ props.SALIRonMouseLeave } onMouseUp={ props.SALIRonMouseUp } onMouseDown={ props.SALIRonMouseDown } onKeyDown={ props.SALIRonKeyDown } onChange={ props.SALIRonChange } ondelay={ props.SALIRondelay } >{props.SALIR0 || `SALIR`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['menu']?.animationClass || {}}>

              <span id="id_oneonezero_threetwoothree"  className={` text menu   cursor ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.MENUStyle , transitionDuration: transaction['menu']?.duration, transitionTimingFunction: transaction['menu']?.timingFunction }} onClick={ props.MENUonClick || function(e){ SingletoneNavigation.setTransitionInstance(null, 'Desktopfive');
history.push('/4'); }} onMouseEnter={ props.MENUonMouseEnter } onMouseOver={ props.MENUonMouseOver } onKeyPress={ props.MENUonKeyPress } onDrag={ props.MENUonDrag } onMouseLeave={ props.MENUonMouseLeave } onMouseUp={ props.MENUonMouseUp } onMouseDown={ props.MENUonMouseDown } onKeyDown={ props.MENUonKeyDown } onChange={ props.MENUonChange } ondelay={ props.MENUondelay } >{props.MENU0 || `MENU`}</span>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameoneone']?.animationClass || {}}>

              <div id="id_oneonezero_threetwoofour" className={` frame frameoneone cursor ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameoneoneStyle , transitionDuration: transaction['frameoneone']?.duration, transitionTimingFunction: transaction['frameoneone']?.timingFunction } } onClick={ props.FrameoneoneonClick || function(e){ SingletoneNavigation.setTransitionInstance(null, 'Desktopfour' ); history.push('/3'); }} onMouseEnter={ props.FrameoneoneonMouseEnter } onMouseOver={ props.FrameoneoneonMouseOver } onKeyPress={ props.FrameoneoneonKeyPress } onDrag={ props.FrameoneoneonDrag } onMouseLeave={ props.FrameoneoneonMouseLeave } onMouseUp={ props.FrameoneoneonMouseUp } onMouseDown={ props.FrameoneoneonMouseDown } onKeyDown={ props.FrameoneoneonKeyDown } onChange={ props.FrameoneoneonChange } ondelay={ props.Frameoneoneondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['personal']?.animationClass || {}}>

                  <span id="id_oneonezero_threetwoofive"  className={` text personal    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.PERSONALStyle , transitionDuration: transaction['personal']?.duration, transitionTimingFunction: transaction['personal']?.timingFunction }} onClick={ props.PERSONALonClick } onMouseEnter={ props.PERSONALonMouseEnter } onMouseOver={ props.PERSONALonMouseOver } onKeyPress={ props.PERSONALonKeyPress } onDrag={ props.PERSONALonDrag } onMouseLeave={ props.PERSONALonMouseLeave } onMouseUp={ props.PERSONALonMouseUp } onMouseDown={ props.PERSONALonMouseDown } onKeyDown={ props.PERSONALonKeyDown } onChange={ props.PERSONALonChange } ondelay={ props.PERSONALondelay } >{props.PERSONAL0 || `PERSONAL`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameonethree']?.animationClass || {}}>

              <div id="id_oneonezero_threetwoosix" className={` frame frameonethree cursor ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonethreeStyle , transitionDuration: transaction['frameonethree']?.duration, transitionTimingFunction: transaction['frameonethree']?.timingFunction } } onClick={ props.FrameonethreeonClick || function(e){ SingletoneNavigation.setTransitionInstance(null, 'Desktopeight' ); history.push('/7'); }} onMouseEnter={ props.FrameonethreeonMouseEnter } onMouseOver={ props.FrameonethreeonMouseOver } onKeyPress={ props.FrameonethreeonKeyPress } onDrag={ props.FrameonethreeonDrag } onMouseLeave={ props.FrameonethreeonMouseLeave } onMouseUp={ props.FrameonethreeonMouseUp } onMouseDown={ props.FrameonethreeonMouseDown } onKeyDown={ props.FrameonethreeonKeyDown } onChange={ props.FrameonethreeonChange } ondelay={ props.Frameonethreeondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['puestos']?.animationClass || {}}>

                  <span id="id_oneonezero_threetwooseven"  className={` text puestos    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.PUESTOSStyle , transitionDuration: transaction['puestos']?.duration, transitionTimingFunction: transaction['puestos']?.timingFunction }} onClick={ props.PUESTOSonClick } onMouseEnter={ props.PUESTOSonMouseEnter } onMouseOver={ props.PUESTOSonMouseOver } onKeyPress={ props.PUESTOSonKeyPress } onDrag={ props.PUESTOSonDrag } onMouseLeave={ props.PUESTOSonMouseLeave } onMouseUp={ props.PUESTOSonMouseUp } onMouseDown={ props.PUESTOSonMouseDown } onKeyDown={ props.PUESTOSonKeyDown } onChange={ props.PUESTOSonChange } ondelay={ props.PUESTOSondelay } >{props.PUESTOS0 || `PUESTOS`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} classNames={transaction['frameoneseven']?.animationClass || {}}>

          <div id="id_oneoneone_nigthsix" className={` frame frameoneseven ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonesevenStyle , transitionDuration: transaction['frameoneseven']?.duration, transitionTimingFunction: transaction['frameoneseven']?.timingFunction } } onClick={ props.FrameonesevenonClick } onMouseEnter={ props.FrameonesevenonMouseEnter } onMouseOver={ props.FrameonesevenonMouseOver } onKeyPress={ props.FrameonesevenonKeyPress } onDrag={ props.FrameonesevenonDrag } onMouseLeave={ props.FrameonesevenonMouseLeave } onMouseUp={ props.FrameonesevenonMouseUp } onMouseDown={ props.FrameonesevenonMouseDown } onKeyDown={ props.FrameonesevenonKeyDown } onChange={ props.FrameonesevenonChange } ondelay={ props.Frameonesevenondelay }>
            <CSSTransition in={_in} appear={true} classNames={transaction['contrasea']?.animationClass || {}}>

              <span id="id_oneoneone_nigthseven"  className={` text contrasea    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.CONTRASEAStyle , transitionDuration: transaction['contrasea']?.duration, transitionTimingFunction: transaction['contrasea']?.timingFunction }} onClick={ props.CONTRASEAonClick } onMouseEnter={ props.CONTRASEAonMouseEnter } onMouseOver={ props.CONTRASEAonMouseOver } onKeyPress={ props.CONTRASEAonKeyPress } onDrag={ props.CONTRASEAonDrag } onMouseLeave={ props.CONTRASEAonMouseLeave } onMouseUp={ props.CONTRASEAonMouseUp } onMouseDown={ props.CONTRASEAonMouseDown } onKeyDown={ props.CONTRASEAonKeyDown } onChange={ props.CONTRASEAonChange } ondelay={ props.CONTRASEAondelay } >{props.CONTRASEA0 || `CONTRASEÑA`}</span>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} classNames={transaction['frameonefive']?.animationClass || {}}>

          <div id="id_oneoneone_nigtheight" className={` frame frameonefive ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonefiveStyle , transitionDuration: transaction['frameonefive']?.duration, transitionTimingFunction: transaction['frameonefive']?.timingFunction } } onClick={ props.FrameonefiveonClick } onMouseEnter={ props.FrameonefiveonMouseEnter } onMouseOver={ props.FrameonefiveonMouseOver } onKeyPress={ props.FrameonefiveonKeyPress } onDrag={ props.FrameonefiveonDrag } onMouseLeave={ props.FrameonefiveonMouseLeave } onMouseUp={ props.FrameonefiveonMouseUp } onMouseDown={ props.FrameonefiveonMouseDown } onKeyDown={ props.FrameonefiveonKeyDown } onChange={ props.FrameonefiveonChange } ondelay={ props.Frameonefiveondelay }>
            <CSSTransition in={_in} appear={true} classNames={transaction['administradordeusuarios']?.animationClass || {}}>

              <span id="id_oneoneone_nigthnigth"  className={` text administradordeusuarios    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.ADMINISTRADORDEUSUARIOSStyle , transitionDuration: transaction['administradordeusuarios']?.duration, transitionTimingFunction: transaction['administradordeusuarios']?.timingFunction }} onClick={ props.ADMINISTRADORDEUSUARIOSonClick } onMouseEnter={ props.ADMINISTRADORDEUSUARIOSonMouseEnter } onMouseOver={ props.ADMINISTRADORDEUSUARIOSonMouseOver } onKeyPress={ props.ADMINISTRADORDEUSUARIOSonKeyPress } onDrag={ props.ADMINISTRADORDEUSUARIOSonDrag } onMouseLeave={ props.ADMINISTRADORDEUSUARIOSonMouseLeave } onMouseUp={ props.ADMINISTRADORDEUSUARIOSonMouseUp } onMouseDown={ props.ADMINISTRADORDEUSUARIOSonMouseDown } onKeyDown={ props.ADMINISTRADORDEUSUARIOSonKeyDown } onChange={ props.ADMINISTRADORDEUSUARIOSonChange } ondelay={ props.ADMINISTRADORDEUSUARIOSondelay } >{props.ADMINISTRADORDEUSUARIOS0 || `ADMINISTRADOR DE USUARIOS`}</span>

            </CSSTransition>
          </div>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Desktopsix.propTypes = {
    style: PropTypes.any,
COLEGIOJAPONESDEMORELOS0: PropTypes.any,
USUARIO0: PropTypes.any,
INGRESAR0: PropTypes.any,
ID0: PropTypes.any,
BUSCAR0: PropTypes.any,
APELLIDO0: PropTypes.any,
PUESTO0: PropTypes.any,
NOMBRE0: PropTypes.any,
Onetwooonefourfivenigthfourone0: PropTypes.any,
IDNOMBREAPELLIDOPUESTOELIMINAR0: PropTypes.any,
OneFULANITOVERADocente0: PropTypes.any,
REPORTES0: PropTypes.any,
USUARIOS0: PropTypes.any,
SALIR0: PropTypes.any,
MENU0: PropTypes.any,
PERSONAL0: PropTypes.any,
PUESTOS0: PropTypes.any,
CONTRASEA0: PropTypes.any,
ADMINISTRADORDEUSUARIOS0: PropTypes.any,
DesktopsixonClick: PropTypes.any,
DesktopsixonMouseEnter: PropTypes.any,
DesktopsixonMouseOver: PropTypes.any,
DesktopsixonKeyPress: PropTypes.any,
DesktopsixonDrag: PropTypes.any,
DesktopsixonMouseLeave: PropTypes.any,
DesktopsixonMouseUp: PropTypes.any,
DesktopsixonMouseDown: PropTypes.any,
DesktopsixonKeyDown: PropTypes.any,
DesktopsixonChange: PropTypes.any,
Desktopsixondelay: PropTypes.any,
FrameeightonClick: PropTypes.any,
FrameeightonMouseEnter: PropTypes.any,
FrameeightonMouseOver: PropTypes.any,
FrameeightonKeyPress: PropTypes.any,
FrameeightonDrag: PropTypes.any,
FrameeightonMouseLeave: PropTypes.any,
FrameeightonMouseUp: PropTypes.any,
FrameeightonMouseDown: PropTypes.any,
FrameeightonKeyDown: PropTypes.any,
FrameeightonChange: PropTypes.any,
Frameeightondelay: PropTypes.any,
COLEGIOJAPONESDEMORELOSonClick: PropTypes.any,
COLEGIOJAPONESDEMORELOSonMouseEnter: PropTypes.any,
COLEGIOJAPONESDEMORELOSonMouseOver: PropTypes.any,
COLEGIOJAPONESDEMORELOSonKeyPress: PropTypes.any,
COLEGIOJAPONESDEMORELOSonDrag: PropTypes.any,
COLEGIOJAPONESDEMORELOSonMouseLeave: PropTypes.any,
COLEGIOJAPONESDEMORELOSonMouseUp: PropTypes.any,
COLEGIOJAPONESDEMORELOSonMouseDown: PropTypes.any,
COLEGIOJAPONESDEMORELOSonKeyDown: PropTypes.any,
COLEGIOJAPONESDEMORELOSonChange: PropTypes.any,
COLEGIOJAPONESDEMORELOSondelay: PropTypes.any,
FramenigthonClick: PropTypes.any,
FramenigthonMouseEnter: PropTypes.any,
FramenigthonMouseOver: PropTypes.any,
FramenigthonKeyPress: PropTypes.any,
FramenigthonDrag: PropTypes.any,
FramenigthonMouseLeave: PropTypes.any,
FramenigthonMouseUp: PropTypes.any,
FramenigthonMouseDown: PropTypes.any,
FramenigthonKeyDown: PropTypes.any,
FramenigthonChange: PropTypes.any,
Framenigthondelay: PropTypes.any,
NotojapanesebargainbuttononClick: PropTypes.any,
NotojapanesebargainbuttononMouseEnter: PropTypes.any,
NotojapanesebargainbuttononMouseOver: PropTypes.any,
NotojapanesebargainbuttononKeyPress: PropTypes.any,
NotojapanesebargainbuttononDrag: PropTypes.any,
NotojapanesebargainbuttononMouseLeave: PropTypes.any,
NotojapanesebargainbuttononMouseUp: PropTypes.any,
NotojapanesebargainbuttononMouseDown: PropTypes.any,
NotojapanesebargainbuttononKeyDown: PropTypes.any,
NotojapanesebargainbuttononChange: PropTypes.any,
Notojapanesebargainbuttonondelay: PropTypes.any,
VectoronClick: PropTypes.any,
VectoronMouseEnter: PropTypes.any,
VectoronMouseOver: PropTypes.any,
VectoronKeyPress: PropTypes.any,
VectoronDrag: PropTypes.any,
VectoronMouseLeave: PropTypes.any,
VectoronMouseUp: PropTypes.any,
VectoronMouseDown: PropTypes.any,
VectoronKeyDown: PropTypes.any,
VectoronChange: PropTypes.any,
Vectorondelay: PropTypes.any,
FrameonefouronClick: PropTypes.any,
FrameonefouronMouseEnter: PropTypes.any,
FrameonefouronMouseOver: PropTypes.any,
FrameonefouronKeyPress: PropTypes.any,
FrameonefouronDrag: PropTypes.any,
FrameonefouronMouseLeave: PropTypes.any,
FrameonefouronMouseUp: PropTypes.any,
FrameonefouronMouseDown: PropTypes.any,
FrameonefouronKeyDown: PropTypes.any,
FrameonefouronChange: PropTypes.any,
Frameonefourondelay: PropTypes.any,
FrameoneeightonClick: PropTypes.any,
FrameoneeightonMouseEnter: PropTypes.any,
FrameoneeightonMouseOver: PropTypes.any,
FrameoneeightonKeyPress: PropTypes.any,
FrameoneeightonDrag: PropTypes.any,
FrameoneeightonMouseLeave: PropTypes.any,
FrameoneeightonMouseUp: PropTypes.any,
FrameoneeightonMouseDown: PropTypes.any,
FrameoneeightonKeyDown: PropTypes.any,
FrameoneeightonChange: PropTypes.any,
Frameoneeightondelay: PropTypes.any,
FrameonesevenonClick: PropTypes.any,
FrameonesevenonMouseEnter: PropTypes.any,
FrameonesevenonMouseOver: PropTypes.any,
FrameonesevenonKeyPress: PropTypes.any,
FrameonesevenonDrag: PropTypes.any,
FrameonesevenonMouseLeave: PropTypes.any,
FrameonesevenonMouseUp: PropTypes.any,
FrameonesevenonMouseDown: PropTypes.any,
FrameonesevenonKeyDown: PropTypes.any,
FrameonesevenonChange: PropTypes.any,
Frameonesevenondelay: PropTypes.any,
USUARIOonClick: PropTypes.any,
USUARIOonMouseEnter: PropTypes.any,
USUARIOonMouseOver: PropTypes.any,
USUARIOonKeyPress: PropTypes.any,
USUARIOonDrag: PropTypes.any,
USUARIOonMouseLeave: PropTypes.any,
USUARIOonMouseUp: PropTypes.any,
USUARIOonMouseDown: PropTypes.any,
USUARIOonKeyDown: PropTypes.any,
USUARIOonChange: PropTypes.any,
USUARIOondelay: PropTypes.any,
FrametwoofouronClick: PropTypes.any,
FrametwoofouronMouseEnter: PropTypes.any,
FrametwoofouronMouseOver: PropTypes.any,
FrametwoofouronKeyPress: PropTypes.any,
FrametwoofouronDrag: PropTypes.any,
FrametwoofouronMouseLeave: PropTypes.any,
FrametwoofouronMouseUp: PropTypes.any,
FrametwoofouronMouseDown: PropTypes.any,
FrametwoofouronKeyDown: PropTypes.any,
FrametwoofouronChange: PropTypes.any,
Frametwoofourondelay: PropTypes.any,
INGRESARonClick: PropTypes.any,
INGRESARonMouseEnter: PropTypes.any,
INGRESARonMouseOver: PropTypes.any,
INGRESARonKeyPress: PropTypes.any,
INGRESARonDrag: PropTypes.any,
INGRESARonMouseLeave: PropTypes.any,
INGRESARonMouseUp: PropTypes.any,
INGRESARonMouseDown: PropTypes.any,
INGRESARonKeyDown: PropTypes.any,
INGRESARonChange: PropTypes.any,
INGRESARondelay: PropTypes.any,
FrametwoofiveonClick: PropTypes.any,
FrametwoofiveonMouseEnter: PropTypes.any,
FrametwoofiveonMouseOver: PropTypes.any,
FrametwoofiveonKeyPress: PropTypes.any,
FrametwoofiveonDrag: PropTypes.any,
FrametwoofiveonMouseLeave: PropTypes.any,
FrametwoofiveonMouseUp: PropTypes.any,
FrametwoofiveonMouseDown: PropTypes.any,
FrametwoofiveonKeyDown: PropTypes.any,
FrametwoofiveonChange: PropTypes.any,
Frametwoofiveondelay: PropTypes.any,
IDonClick: PropTypes.any,
IDonMouseEnter: PropTypes.any,
IDonMouseOver: PropTypes.any,
IDonKeyPress: PropTypes.any,
IDonDrag: PropTypes.any,
IDonMouseLeave: PropTypes.any,
IDonMouseUp: PropTypes.any,
IDonMouseDown: PropTypes.any,
IDonKeyDown: PropTypes.any,
IDonChange: PropTypes.any,
IDondelay: PropTypes.any,
FrametwoosevenonClick: PropTypes.any,
FrametwoosevenonMouseEnter: PropTypes.any,
FrametwoosevenonMouseOver: PropTypes.any,
FrametwoosevenonKeyPress: PropTypes.any,
FrametwoosevenonDrag: PropTypes.any,
FrametwoosevenonMouseLeave: PropTypes.any,
FrametwoosevenonMouseUp: PropTypes.any,
FrametwoosevenonMouseDown: PropTypes.any,
FrametwoosevenonKeyDown: PropTypes.any,
FrametwoosevenonChange: PropTypes.any,
Frametwoosevenondelay: PropTypes.any,
BUSCARonClick: PropTypes.any,
BUSCARonMouseEnter: PropTypes.any,
BUSCARonMouseOver: PropTypes.any,
BUSCARonKeyPress: PropTypes.any,
BUSCARonDrag: PropTypes.any,
BUSCARonMouseLeave: PropTypes.any,
BUSCARonMouseUp: PropTypes.any,
BUSCARonMouseDown: PropTypes.any,
BUSCARonKeyDown: PropTypes.any,
BUSCARonChange: PropTypes.any,
BUSCARondelay: PropTypes.any,
FrametwooeightonClick: PropTypes.any,
FrametwooeightonMouseEnter: PropTypes.any,
FrametwooeightonMouseOver: PropTypes.any,
FrametwooeightonKeyPress: PropTypes.any,
FrametwooeightonDrag: PropTypes.any,
FrametwooeightonMouseLeave: PropTypes.any,
FrametwooeightonMouseUp: PropTypes.any,
FrametwooeightonMouseDown: PropTypes.any,
FrametwooeightonKeyDown: PropTypes.any,
FrametwooeightonChange: PropTypes.any,
Frametwooeightondelay: PropTypes.any,
APELLIDOonClick: PropTypes.any,
APELLIDOonMouseEnter: PropTypes.any,
APELLIDOonMouseOver: PropTypes.any,
APELLIDOonKeyPress: PropTypes.any,
APELLIDOonDrag: PropTypes.any,
APELLIDOonMouseLeave: PropTypes.any,
APELLIDOonMouseUp: PropTypes.any,
APELLIDOonMouseDown: PropTypes.any,
APELLIDOonKeyDown: PropTypes.any,
APELLIDOonChange: PropTypes.any,
APELLIDOondelay: PropTypes.any,
FramethreeoneonClick: PropTypes.any,
FramethreeoneonMouseEnter: PropTypes.any,
FramethreeoneonMouseOver: PropTypes.any,
FramethreeoneonKeyPress: PropTypes.any,
FramethreeoneonDrag: PropTypes.any,
FramethreeoneonMouseLeave: PropTypes.any,
FramethreeoneonMouseUp: PropTypes.any,
FramethreeoneonMouseDown: PropTypes.any,
FramethreeoneonKeyDown: PropTypes.any,
FramethreeoneonChange: PropTypes.any,
Framethreeoneondelay: PropTypes.any,
PUESTOonClick: PropTypes.any,
PUESTOonMouseEnter: PropTypes.any,
PUESTOonMouseOver: PropTypes.any,
PUESTOonKeyPress: PropTypes.any,
PUESTOonDrag: PropTypes.any,
PUESTOonMouseLeave: PropTypes.any,
PUESTOonMouseUp: PropTypes.any,
PUESTOonMouseDown: PropTypes.any,
PUESTOonKeyDown: PropTypes.any,
PUESTOonChange: PropTypes.any,
PUESTOondelay: PropTypes.any,
FrametwoonigthonClick: PropTypes.any,
FrametwoonigthonMouseEnter: PropTypes.any,
FrametwoonigthonMouseOver: PropTypes.any,
FrametwoonigthonKeyPress: PropTypes.any,
FrametwoonigthonDrag: PropTypes.any,
FrametwoonigthonMouseLeave: PropTypes.any,
FrametwoonigthonMouseUp: PropTypes.any,
FrametwoonigthonMouseDown: PropTypes.any,
FrametwoonigthonKeyDown: PropTypes.any,
FrametwoonigthonChange: PropTypes.any,
Frametwoonigthondelay: PropTypes.any,
NOMBREonClick: PropTypes.any,
NOMBREonMouseEnter: PropTypes.any,
NOMBREonMouseOver: PropTypes.any,
NOMBREonKeyPress: PropTypes.any,
NOMBREonDrag: PropTypes.any,
NOMBREonMouseLeave: PropTypes.any,
NOMBREonMouseUp: PropTypes.any,
NOMBREonMouseDown: PropTypes.any,
NOMBREonKeyDown: PropTypes.any,
NOMBREonChange: PropTypes.any,
NOMBREondelay: PropTypes.any,
FramethreezeroonClick: PropTypes.any,
FramethreezeroonMouseEnter: PropTypes.any,
FramethreezeroonMouseOver: PropTypes.any,
FramethreezeroonKeyPress: PropTypes.any,
FramethreezeroonDrag: PropTypes.any,
FramethreezeroonMouseLeave: PropTypes.any,
FramethreezeroonMouseUp: PropTypes.any,
FramethreezeroonMouseDown: PropTypes.any,
FramethreezeroonKeyDown: PropTypes.any,
FramethreezeroonChange: PropTypes.any,
Framethreezeroondelay: PropTypes.any,
FramethreetwooonClick: PropTypes.any,
FramethreetwooonMouseEnter: PropTypes.any,
FramethreetwooonMouseOver: PropTypes.any,
FramethreetwooonKeyPress: PropTypes.any,
FramethreetwooonDrag: PropTypes.any,
FramethreetwooonMouseLeave: PropTypes.any,
FramethreetwooonMouseUp: PropTypes.any,
FramethreetwooonMouseDown: PropTypes.any,
FramethreetwooonKeyDown: PropTypes.any,
FramethreetwooonChange: PropTypes.any,
Framethreetwooondelay: PropTypes.any,
OnetwooonefourfivenigthfouroneonClick: PropTypes.any,
OnetwooonefourfivenigthfouroneonMouseEnter: PropTypes.any,
OnetwooonefourfivenigthfouroneonMouseOver: PropTypes.any,
OnetwooonefourfivenigthfouroneonKeyPress: PropTypes.any,
OnetwooonefourfivenigthfouroneonDrag: PropTypes.any,
OnetwooonefourfivenigthfouroneonMouseLeave: PropTypes.any,
OnetwooonefourfivenigthfouroneonMouseUp: PropTypes.any,
OnetwooonefourfivenigthfouroneonMouseDown: PropTypes.any,
OnetwooonefourfivenigthfouroneonKeyDown: PropTypes.any,
OnetwooonefourfivenigthfouroneonChange: PropTypes.any,
Onetwooonefourfivenigthfouroneondelay: PropTypes.any,
IDNOMBREAPELLIDOPUESTOELIMINARonClick: PropTypes.any,
IDNOMBREAPELLIDOPUESTOELIMINARonMouseEnter: PropTypes.any,
IDNOMBREAPELLIDOPUESTOELIMINARonMouseOver: PropTypes.any,
IDNOMBREAPELLIDOPUESTOELIMINARonKeyPress: PropTypes.any,
IDNOMBREAPELLIDOPUESTOELIMINARonDrag: PropTypes.any,
IDNOMBREAPELLIDOPUESTOELIMINARonMouseLeave: PropTypes.any,
IDNOMBREAPELLIDOPUESTOELIMINARonMouseUp: PropTypes.any,
IDNOMBREAPELLIDOPUESTOELIMINARonMouseDown: PropTypes.any,
IDNOMBREAPELLIDOPUESTOELIMINARonKeyDown: PropTypes.any,
IDNOMBREAPELLIDOPUESTOELIMINARonChange: PropTypes.any,
IDNOMBREAPELLIDOPUESTOELIMINARondelay: PropTypes.any,
OneFULANITOVERADocenteonClick: PropTypes.any,
OneFULANITOVERADocenteonMouseEnter: PropTypes.any,
OneFULANITOVERADocenteonMouseOver: PropTypes.any,
OneFULANITOVERADocenteonKeyPress: PropTypes.any,
OneFULANITOVERADocenteonDrag: PropTypes.any,
OneFULANITOVERADocenteonMouseLeave: PropTypes.any,
OneFULANITOVERADocenteonMouseUp: PropTypes.any,
OneFULANITOVERADocenteonMouseDown: PropTypes.any,
OneFULANITOVERADocenteonKeyDown: PropTypes.any,
OneFULANITOVERADocenteonChange: PropTypes.any,
OneFULANITOVERADocenteondelay: PropTypes.any,
FrameonetwooonClick: PropTypes.any,
FrameonetwooonMouseEnter: PropTypes.any,
FrameonetwooonMouseOver: PropTypes.any,
FrameonetwooonKeyPress: PropTypes.any,
FrameonetwooonDrag: PropTypes.any,
FrameonetwooonMouseLeave: PropTypes.any,
FrameonetwooonMouseUp: PropTypes.any,
FrameonetwooonMouseDown: PropTypes.any,
FrameonetwooonKeyDown: PropTypes.any,
FrameonetwooonChange: PropTypes.any,
Frameonetwooondelay: PropTypes.any,
REPORTESonClick: PropTypes.any,
REPORTESonMouseEnter: PropTypes.any,
REPORTESonMouseOver: PropTypes.any,
REPORTESonKeyPress: PropTypes.any,
REPORTESonDrag: PropTypes.any,
REPORTESonMouseLeave: PropTypes.any,
REPORTESonMouseUp: PropTypes.any,
REPORTESonMouseDown: PropTypes.any,
REPORTESonKeyDown: PropTypes.any,
REPORTESonChange: PropTypes.any,
REPORTESondelay: PropTypes.any,
USUARIOSonClick: PropTypes.any,
USUARIOSonMouseEnter: PropTypes.any,
USUARIOSonMouseOver: PropTypes.any,
USUARIOSonKeyPress: PropTypes.any,
USUARIOSonDrag: PropTypes.any,
USUARIOSonMouseLeave: PropTypes.any,
USUARIOSonMouseUp: PropTypes.any,
USUARIOSonMouseDown: PropTypes.any,
USUARIOSonKeyDown: PropTypes.any,
USUARIOSonChange: PropTypes.any,
USUARIOSondelay: PropTypes.any,
SALIRonClick: PropTypes.any,
SALIRonMouseEnter: PropTypes.any,
SALIRonMouseOver: PropTypes.any,
SALIRonKeyPress: PropTypes.any,
SALIRonDrag: PropTypes.any,
SALIRonMouseLeave: PropTypes.any,
SALIRonMouseUp: PropTypes.any,
SALIRonMouseDown: PropTypes.any,
SALIRonKeyDown: PropTypes.any,
SALIRonChange: PropTypes.any,
SALIRondelay: PropTypes.any,
MENUonClick: PropTypes.any,
MENUonMouseEnter: PropTypes.any,
MENUonMouseOver: PropTypes.any,
MENUonKeyPress: PropTypes.any,
MENUonDrag: PropTypes.any,
MENUonMouseLeave: PropTypes.any,
MENUonMouseUp: PropTypes.any,
MENUonMouseDown: PropTypes.any,
MENUonKeyDown: PropTypes.any,
MENUonChange: PropTypes.any,
MENUondelay: PropTypes.any,
FrameoneoneonClick: PropTypes.any,
FrameoneoneonMouseEnter: PropTypes.any,
FrameoneoneonMouseOver: PropTypes.any,
FrameoneoneonKeyPress: PropTypes.any,
FrameoneoneonDrag: PropTypes.any,
FrameoneoneonMouseLeave: PropTypes.any,
FrameoneoneonMouseUp: PropTypes.any,
FrameoneoneonMouseDown: PropTypes.any,
FrameoneoneonKeyDown: PropTypes.any,
FrameoneoneonChange: PropTypes.any,
Frameoneoneondelay: PropTypes.any,
PERSONALonClick: PropTypes.any,
PERSONALonMouseEnter: PropTypes.any,
PERSONALonMouseOver: PropTypes.any,
PERSONALonKeyPress: PropTypes.any,
PERSONALonDrag: PropTypes.any,
PERSONALonMouseLeave: PropTypes.any,
PERSONALonMouseUp: PropTypes.any,
PERSONALonMouseDown: PropTypes.any,
PERSONALonKeyDown: PropTypes.any,
PERSONALonChange: PropTypes.any,
PERSONALondelay: PropTypes.any,
FrameonethreeonClick: PropTypes.any,
FrameonethreeonMouseEnter: PropTypes.any,
FrameonethreeonMouseOver: PropTypes.any,
FrameonethreeonKeyPress: PropTypes.any,
FrameonethreeonDrag: PropTypes.any,
FrameonethreeonMouseLeave: PropTypes.any,
FrameonethreeonMouseUp: PropTypes.any,
FrameonethreeonMouseDown: PropTypes.any,
FrameonethreeonKeyDown: PropTypes.any,
FrameonethreeonChange: PropTypes.any,
Frameonethreeondelay: PropTypes.any,
PUESTOSonClick: PropTypes.any,
PUESTOSonMouseEnter: PropTypes.any,
PUESTOSonMouseOver: PropTypes.any,
PUESTOSonKeyPress: PropTypes.any,
PUESTOSonDrag: PropTypes.any,
PUESTOSonMouseLeave: PropTypes.any,
PUESTOSonMouseUp: PropTypes.any,
PUESTOSonMouseDown: PropTypes.any,
PUESTOSonKeyDown: PropTypes.any,
PUESTOSonChange: PropTypes.any,
PUESTOSondelay: PropTypes.any,
CONTRASEAonClick: PropTypes.any,
CONTRASEAonMouseEnter: PropTypes.any,
CONTRASEAonMouseOver: PropTypes.any,
CONTRASEAonKeyPress: PropTypes.any,
CONTRASEAonDrag: PropTypes.any,
CONTRASEAonMouseLeave: PropTypes.any,
CONTRASEAonMouseUp: PropTypes.any,
CONTRASEAonMouseDown: PropTypes.any,
CONTRASEAonKeyDown: PropTypes.any,
CONTRASEAonChange: PropTypes.any,
CONTRASEAondelay: PropTypes.any,
FrameonefiveonClick: PropTypes.any,
FrameonefiveonMouseEnter: PropTypes.any,
FrameonefiveonMouseOver: PropTypes.any,
FrameonefiveonKeyPress: PropTypes.any,
FrameonefiveonDrag: PropTypes.any,
FrameonefiveonMouseLeave: PropTypes.any,
FrameonefiveonMouseUp: PropTypes.any,
FrameonefiveonMouseDown: PropTypes.any,
FrameonefiveonKeyDown: PropTypes.any,
FrameonefiveonChange: PropTypes.any,
Frameonefiveondelay: PropTypes.any,
ADMINISTRADORDEUSUARIOSonClick: PropTypes.any,
ADMINISTRADORDEUSUARIOSonMouseEnter: PropTypes.any,
ADMINISTRADORDEUSUARIOSonMouseOver: PropTypes.any,
ADMINISTRADORDEUSUARIOSonKeyPress: PropTypes.any,
ADMINISTRADORDEUSUARIOSonDrag: PropTypes.any,
ADMINISTRADORDEUSUARIOSonMouseLeave: PropTypes.any,
ADMINISTRADORDEUSUARIOSonMouseUp: PropTypes.any,
ADMINISTRADORDEUSUARIOSonMouseDown: PropTypes.any,
ADMINISTRADORDEUSUARIOSonKeyDown: PropTypes.any,
ADMINISTRADORDEUSUARIOSonChange: PropTypes.any,
ADMINISTRADORDEUSUARIOSondelay: PropTypes.any
}
export default Desktopsix;