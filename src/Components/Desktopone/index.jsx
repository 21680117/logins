import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import Frametwoo from 'Components/Frametwoo'
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Desktopone.css'





const Desktopone = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        

        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition nodeRef={nodeRef} in={true} appear={true} timeout={10} classNames={SingletoneNavigation.getTransitionInstance()?.Desktopone?.cssClass || '' }>

    <div id="id_twoo_five" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } desktopone ${ props.cssClass } `} style={ { ...{ ...{}, transitionDuration: `${((SingletoneNavigation.getTransitionInstance()?.Desktopone?.duration || 0) * 1000).toFixed(0)}ms` }, ...props.style }} onClick={ props.DesktoponeonClick } onMouseEnter={ props.DesktoponeonMouseEnter } onMouseOver={ props.DesktoponeonMouseOver } onKeyPress={ props.DesktoponeonKeyPress } onDrag={ props.DesktoponeonDrag } onMouseLeave={ props.DesktoponeonMouseLeave } onMouseUp={ props.DesktoponeonMouseUp } onMouseDown={ props.DesktoponeonMouseDown } onKeyDown={ props.DesktoponeonKeyDown } onChange={ props.DesktoponeonChange } ondelay={ props.Desktoponeondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} classNames={transaction['groupone']?.animationClass || {}}>

          <div id="id_oneseveneight_three" className={` group groupone ${ props.onClick ? 'cursor' : '' } `} style={{ ...{},  ...props.GrouponeStyle , transitionDuration: transaction['groupone']?.duration, transitionTimingFunction: transaction['groupone']?.timingFunction }} onClick={ props.GrouponeonClick } onMouseEnter={ props.GrouponeonMouseEnter } onMouseOver={ props.GrouponeonMouseOver } onKeyPress={ props.GrouponeonKeyPress } onDrag={ props.GrouponeonDrag } onMouseLeave={ props.GrouponeonMouseLeave } onMouseUp={ props.GrouponeonMouseUp } onMouseDown={ props.GrouponeonMouseDown } onKeyDown={ props.GrouponeonKeyDown } onChange={ props.GrouponeonChange } ondelay={ props.Grouponeondelay }>
            <CSSTransition in={_in} appear={true} classNames={transaction['frametwoo']?.animationClass || {}}>
              <Frametwoo { ...{ ...props, style: props.FrametwooStyle ? props.FrametwooStyle['178:2'] : false } } cssClass={"C_oneseveneight_twoo "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        classNames={transaction['frameone']?.animationClass || {}}
    >
    
                    <div id="id_onefive_six" className={` frame frameone ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameoneStyle , transitionDuration: transaction['frameone']?.duration, transitionTimingFunction: transaction['frameone']?.timingFunction } } onClick={ props.FrameoneonClick } onMouseEnter={ props.FrameoneonMouseEnter } onMouseOver={ props.FrameoneonMouseOver } onKeyPress={ props.FrameoneonKeyPress } onDrag={ props.FrameoneonDrag } onMouseLeave={ props.FrameoneonMouseLeave } onMouseUp={ props.FrameoneonMouseUp } onMouseDown={ props.FrameoneonMouseDown } onKeyDown={ props.FrameoneonKeyDown } onChange={ props.FrameoneonChange } ondelay={ props.Frameoneondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['framethree']?.animationClass || {}}>

                  <div id="id_onefive_onetwoo" className={` frame framethree ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FramethreeStyle , transitionDuration: transaction['framethree']?.duration, transitionTimingFunction: transaction['framethree']?.timingFunction } } onClick={ props.FramethreeonClick } onMouseEnter={ props.FramethreeonMouseEnter } onMouseOver={ props.FramethreeonMouseOver } onKeyPress={ props.FramethreeonKeyPress } onDrag={ props.FramethreeonDrag } onMouseLeave={ props.FramethreeonMouseLeave } onMouseUp={ props.FramethreeonMouseUp } onMouseDown={ props.FramethreeonMouseDown } onKeyDown={ props.FramethreeonKeyDown } onChange={ props.FramethreeonChange } ondelay={ props.Framethreeondelay }>
                    <CSSTransition in={_in} appear={true} classNames={transaction['twemojijapanesebargainbutton']?.animationClass || {}}>

                      <div id="id_onefive_eight" className={` frame twemojijapanesebargainbutton ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.TwemojijapanesebargainbuttonStyle , transitionDuration: transaction['twemojijapanesebargainbutton']?.duration, transitionTimingFunction: transaction['twemojijapanesebargainbutton']?.timingFunction } } onClick={ props.TwemojijapanesebargainbuttononClick } onMouseEnter={ props.TwemojijapanesebargainbuttononMouseEnter } onMouseOver={ props.TwemojijapanesebargainbuttononMouseOver } onKeyPress={ props.TwemojijapanesebargainbuttononKeyPress } onDrag={ props.TwemojijapanesebargainbuttononDrag } onMouseLeave={ props.TwemojijapanesebargainbuttononMouseLeave } onMouseUp={ props.TwemojijapanesebargainbuttononMouseUp } onMouseDown={ props.TwemojijapanesebargainbuttononMouseDown } onKeyDown={ props.TwemojijapanesebargainbuttononKeyDown } onChange={ props.TwemojijapanesebargainbuttononChange } ondelay={ props.Twemojijapanesebargainbuttonondelay }>
                        <CSSTransition in={_in} appear={true} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_onefive_nigth" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="120" height="120">

                            <path d="M60 0C26.8633 0 0 26.8667 0 60C0 93.1367 26.8633 120 60 120C93.1367 120 120 93.1367 120 60C120 26.8667 93.1367 0 60 0ZM60 113.333C30.5467 113.333 6.66667 89.4567 6.66667 60C6.66667 30.5467 30.5467 6.66667 60 6.66667C89.4567 6.66667 113.333 30.5467 113.333 60C113.333 89.4567 89.4567 113.333 60 113.333Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_onefive_onezero" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="79.8250732421875" height="80.433349609375">

                            <path d="M10.9217 51.66C10.9217 48.1067 11.1817 44.3767 11.8751 40.3033C9.7084 42.99 7.36841 45.3333 4.94174 47.1533C4.46235 47.5693 3.84267 47.7874 3.20841 47.7633C2.34174 47.7633 1.3884 47.3267 0.608404 46.46C0.210371 45.8394 -0.000267051 45.1172 0.00173714 44.38C-0.0141825 43.915 0.0795008 43.4527 0.275219 43.0306C0.470937 42.6085 0.763204 42.2383 1.1284 41.95C8.58174 36.1433 14.2151 28.95 18.4617 21.2367C19.0684 20.1967 19.9351 19.6767 20.8884 19.6767C21.4951 19.6767 22.1017 19.85 22.6217 20.1967C23.8351 20.8033 24.3551 21.67 24.3551 22.71C24.3551 23.23 24.1817 23.7467 23.9217 24.27C22.017 27.6261 19.8737 30.841 17.5084 33.89L17.5084 77.92C17.5084 79.5667 16.2084 80.4333 14.2151 80.4333C12.3084 80.4333 10.9217 79.5667 10.9217 77.92L10.9217 51.66ZM24.2684 2.94667C24.2566 3.53407 24.0761 4.10567 23.7484 4.59333C19.6751 11.18 13.2617 17.94 6.76174 22.7933C6.16851 23.3354 5.3985 23.6434 4.59507 23.66C3.7284 23.66 2.9484 23.2267 2.1684 22.2733C1.67639 21.7248 1.39936 21.0168 1.38841 20.28C1.38841 19.3267 1.9084 18.46 2.77507 17.7667C8.9284 13.52 14.4751 7.45333 18.1151 1.3C18.4216 0.900485 18.815 0.575862 19.2654 0.350653C19.7158 0.125445 20.2115 0.00554082 20.7151 0C21.4084 -2.96059e-15 22.0151 0.173334 22.6217 0.52C23.6584 1.12667 24.2684 1.99667 24.2684 2.94667ZM68.0351 49.1467L77.4817 49.1467C79.0451 49.1467 79.8251 50.1867 79.8251 51.92C79.8251 53.5633 79.0451 54.69 77.4817 54.69L68.0351 54.69L68.0351 71.2467C68.0351 77.2267 64.9184 79.22 58.4117 79.22C55.2917 79.22 52.3517 79.0467 50.0917 78.5267C48.8817 78.2667 47.8384 77.14 47.8384 75.58C47.8384 75.32 47.8384 75.06 47.9284 74.8C48.2751 73.0667 49.4851 72.2033 50.6117 72.2033C50.7884 72.2033 50.8717 72.2033 51.0484 72.2867C53.3017 72.8967 55.9917 73.0667 57.6384 73.0667C60.9317 73.0667 61.7084 72.46 61.7084 69.6867L61.7084 54.69L24.2684 54.69C22.7084 54.69 21.9284 53.65 21.9284 52.0033C21.9284 50.1833 22.7084 49.1467 24.2684 49.1467L61.7084 49.1467L61.7084 42.47L25.4817 42.47C23.9217 42.47 23.1417 41.2567 23.1417 39.6967C23.1417 38.05 23.9217 36.9233 25.4817 36.9233L76.8784 36.9233C78.3484 36.9233 79.1317 38.05 79.1317 39.6967C79.1317 41.2567 78.3517 42.47 76.8784 42.47L68.0351 42.47L68.0351 49.1467ZM66.7317 2.51333C71.6717 2.51333 73.8417 4.42 73.8417 9.01333L73.8417 24.96C73.8417 29.5533 71.6751 31.46 66.7317 31.46L37.6117 31.46C32.5851 31.46 30.4184 29.5533 30.4184 24.96L30.4184 9.01667C30.4184 4.42333 32.5851 2.51667 37.6117 2.51667L66.7317 2.51333ZM37.0084 56.6867C37.8751 56.6867 38.7417 57.0333 39.2617 57.5533C42.2951 60.5 45.2451 63.7933 47.4117 66.74C47.7528 67.2548 47.9337 67.8591 47.9317 68.4767C47.9317 69.43 47.4951 70.38 46.5451 71.1633C45.883 71.772 45.0178 72.112 44.1184 72.1167C43.2517 72.1167 42.3851 71.7667 41.9517 71.0767C39.6164 67.7414 37.0383 64.5828 34.2384 61.6267C33.7515 61.1446 33.4717 60.4917 33.4584 59.8067C33.4584 58.94 33.8917 58.16 34.6717 57.4667C35.3481 56.9653 36.1665 56.6921 37.0084 56.6867ZM67.7751 14.3033L67.7751 9.71C67.7751 8.15 66.9951 7.71667 65.4384 7.71667L38.8284 7.71667C37.2684 7.71667 36.4884 8.15 36.4884 9.71L36.4884 14.3033L67.7751 14.3033ZM36.4851 24.1833C36.4851 25.83 37.2651 26.2633 38.8251 26.2633L65.4351 26.2633C66.9917 26.2633 67.7717 25.83 67.7717 24.1833L67.7717 19.4167L36.4851 19.4167L36.4851 24.1833Z" />
                          </svg>
                        </CSSTransition>
                      </div>

                    </CSSTransition>
                  </div>

                </CSSTransition>
          </div>

        </CSSTransition>
    </div>

  </CSSTransition>

</>
}
</div>

</CSSTransition>
</>
    ) 
}

Desktopone.propTypes = {
    style: PropTypes.any,
DesktoponeonClick: PropTypes.any,
DesktoponeonMouseEnter: PropTypes.any,
DesktoponeonMouseOver: PropTypes.any,
DesktoponeonKeyPress: PropTypes.any,
DesktoponeonDrag: PropTypes.any,
DesktoponeonMouseLeave: PropTypes.any,
DesktoponeonMouseUp: PropTypes.any,
DesktoponeonMouseDown: PropTypes.any,
DesktoponeonKeyDown: PropTypes.any,
DesktoponeonChange: PropTypes.any,
Desktoponeondelay: PropTypes.any,
GrouponeonClick: PropTypes.any,
GrouponeonMouseEnter: PropTypes.any,
GrouponeonMouseOver: PropTypes.any,
GrouponeonKeyPress: PropTypes.any,
GrouponeonDrag: PropTypes.any,
GrouponeonMouseLeave: PropTypes.any,
GrouponeonMouseUp: PropTypes.any,
GrouponeonMouseDown: PropTypes.any,
GrouponeonKeyDown: PropTypes.any,
GrouponeonChange: PropTypes.any,
Grouponeondelay: PropTypes.any,
FrameoneonClick: PropTypes.any,
FrameoneonMouseEnter: PropTypes.any,
FrameoneonMouseOver: PropTypes.any,
FrameoneonKeyPress: PropTypes.any,
FrameoneonDrag: PropTypes.any,
FrameoneonMouseLeave: PropTypes.any,
FrameoneonMouseUp: PropTypes.any,
FrameoneonMouseDown: PropTypes.any,
FrameoneonKeyDown: PropTypes.any,
FrameoneonChange: PropTypes.any,
Frameoneondelay: PropTypes.any,
FramethreeonClick: PropTypes.any,
FramethreeonMouseEnter: PropTypes.any,
FramethreeonMouseOver: PropTypes.any,
FramethreeonKeyPress: PropTypes.any,
FramethreeonDrag: PropTypes.any,
FramethreeonMouseLeave: PropTypes.any,
FramethreeonMouseUp: PropTypes.any,
FramethreeonMouseDown: PropTypes.any,
FramethreeonKeyDown: PropTypes.any,
FramethreeonChange: PropTypes.any,
Framethreeondelay: PropTypes.any,
TwemojijapanesebargainbuttononClick: PropTypes.any,
TwemojijapanesebargainbuttononMouseEnter: PropTypes.any,
TwemojijapanesebargainbuttononMouseOver: PropTypes.any,
TwemojijapanesebargainbuttononKeyPress: PropTypes.any,
TwemojijapanesebargainbuttononDrag: PropTypes.any,
TwemojijapanesebargainbuttononMouseLeave: PropTypes.any,
TwemojijapanesebargainbuttononMouseUp: PropTypes.any,
TwemojijapanesebargainbuttononMouseDown: PropTypes.any,
TwemojijapanesebargainbuttononKeyDown: PropTypes.any,
TwemojijapanesebargainbuttononChange: PropTypes.any,
Twemojijapanesebargainbuttonondelay: PropTypes.any,
VectoronClick: PropTypes.any,
VectoronMouseEnter: PropTypes.any,
VectoronMouseOver: PropTypes.any,
VectoronKeyPress: PropTypes.any,
VectoronDrag: PropTypes.any,
VectoronMouseLeave: PropTypes.any,
VectoronMouseUp: PropTypes.any,
VectoronMouseDown: PropTypes.any,
VectoronKeyDown: PropTypes.any,
VectoronChange: PropTypes.any,
Vectorondelay: PropTypes.any
}
export default Desktopone;