import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Desktopthree.css'





const Desktopthree = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        

        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition nodeRef={nodeRef} in={true} appear={true} timeout={10} classNames={SingletoneNavigation.getTransitionInstance()?.Desktopthree?.cssClass || '' }>

    <div id="id_nigthzero_oneone" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } desktopthree ${ props.cssClass } `} style={ { ...{ ...{}, transitionDuration: `${((SingletoneNavigation.getTransitionInstance()?.Desktopthree?.duration || 0) * 1000).toFixed(0)}ms` }, ...props.style }} onClick={ props.DesktopthreeonClick } onMouseEnter={ props.DesktopthreeonMouseEnter } onMouseOver={ props.DesktopthreeonMouseOver } onKeyPress={ props.DesktopthreeonKeyPress } onDrag={ props.DesktopthreeonDrag } onMouseLeave={ props.DesktopthreeonMouseLeave } onMouseUp={ props.DesktopthreeonMouseUp } onMouseDown={ props.DesktopthreeonMouseDown } onKeyDown={ props.DesktopthreeonKeyDown } onChange={ props.DesktopthreeonChange } ondelay={ props.Desktopthreeondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} classNames={transaction['frameeight']?.animationClass || {}}>

          <div id="id_nigthzero_onetwoo" className={` frame frameeight ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameeightStyle , transitionDuration: transaction['frameeight']?.duration, transitionTimingFunction: transaction['frameeight']?.timingFunction } } onClick={ props.FrameeightonClick } onMouseEnter={ props.FrameeightonMouseEnter } onMouseOver={ props.FrameeightonMouseOver } onKeyPress={ props.FrameeightonKeyPress } onDrag={ props.FrameeightonDrag } onMouseLeave={ props.FrameeightonMouseLeave } onMouseUp={ props.FrameeightonMouseUp } onMouseDown={ props.FrameeightonMouseDown } onKeyDown={ props.FrameeightonKeyDown } onChange={ props.FrameeightonChange } ondelay={ props.Frameeightondelay }>
            <CSSTransition in={_in} appear={true} classNames={transaction['colegiojaponesdemorelos']?.animationClass || {}}>

              <span id="id_nigthzero_onethree"  className={` text colegiojaponesdemorelos    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.COLEGIOJAPONESDEMORELOSStyle , transitionDuration: transaction['colegiojaponesdemorelos']?.duration, transitionTimingFunction: transaction['colegiojaponesdemorelos']?.timingFunction }} onClick={ props.COLEGIOJAPONESDEMORELOSonClick } onMouseEnter={ props.COLEGIOJAPONESDEMORELOSonMouseEnter } onMouseOver={ props.COLEGIOJAPONESDEMORELOSonMouseOver } onKeyPress={ props.COLEGIOJAPONESDEMORELOSonKeyPress } onDrag={ props.COLEGIOJAPONESDEMORELOSonDrag } onMouseLeave={ props.COLEGIOJAPONESDEMORELOSonMouseLeave } onMouseUp={ props.COLEGIOJAPONESDEMORELOSonMouseUp } onMouseDown={ props.COLEGIOJAPONESDEMORELOSonMouseDown } onKeyDown={ props.COLEGIOJAPONESDEMORELOSonKeyDown } onChange={ props.COLEGIOJAPONESDEMORELOSonChange } ondelay={ props.COLEGIOJAPONESDEMORELOSondelay } >{props.COLEGIOJAPONESDEMORELOS0 || `COLEGIO JAPONES DE MORELOS`}</span>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} classNames={transaction['framenigth']?.animationClass || {}}>

          <div id="id_nigthzero_onefour" className={` frame framenigth ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FramenigthStyle , transitionDuration: transaction['framenigth']?.duration, transitionTimingFunction: transaction['framenigth']?.timingFunction } } onClick={ props.FramenigthonClick } onMouseEnter={ props.FramenigthonMouseEnter } onMouseOver={ props.FramenigthonMouseOver } onKeyPress={ props.FramenigthonKeyPress } onDrag={ props.FramenigthonDrag } onMouseLeave={ props.FramenigthonMouseLeave } onMouseUp={ props.FramenigthonMouseUp } onMouseDown={ props.FramenigthonMouseDown } onKeyDown={ props.FramenigthonKeyDown } onChange={ props.FramenigthonChange } ondelay={ props.Framenigthondelay }>
            <CSSTransition in={_in} appear={true} classNames={transaction['notojapanesebargainbutton']?.animationClass || {}}>

              <div id="id_nigthzero_onefive" className={` frame notojapanesebargainbutton ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.NotojapanesebargainbuttonStyle , transitionDuration: transaction['notojapanesebargainbutton']?.duration, transitionTimingFunction: transaction['notojapanesebargainbutton']?.timingFunction } } onClick={ props.NotojapanesebargainbuttononClick } onMouseEnter={ props.NotojapanesebargainbuttononMouseEnter } onMouseOver={ props.NotojapanesebargainbuttononMouseOver } onKeyPress={ props.NotojapanesebargainbuttononKeyPress } onDrag={ props.NotojapanesebargainbuttononDrag } onMouseLeave={ props.NotojapanesebargainbuttononMouseLeave } onMouseUp={ props.NotojapanesebargainbuttononMouseUp } onMouseDown={ props.NotojapanesebargainbuttononMouseDown } onKeyDown={ props.NotojapanesebargainbuttononKeyDown } onChange={ props.NotojapanesebargainbuttononChange } ondelay={ props.Notojapanesebargainbuttonondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_nigthzero_onesix" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="84.375" height="84.375">

                    <path d="M42.1875 84.375C65.487 84.375 84.375 65.487 84.375 42.1875C84.375 18.888 65.487 0 42.1875 0C18.888 0 0 18.888 0 42.1875C0 65.487 18.888 84.375 42.1875 84.375Z" />
                  </svg>
                </CSSTransition>
                <CSSTransition in={_in} appear={true} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_nigthzero_oneseven" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="78.890625" height="78.890625">

                    <path d="M39.4453 78.8906C61.2304 78.8906 78.8906 61.2304 78.8906 39.4453C78.8906 17.6603 61.2304 0 39.4453 0C17.6603 0 0 17.6603 0 39.4453C0 61.2304 17.6603 78.8906 39.4453 78.8906Z" />
                  </svg>
                </CSSTransition>
                <CSSTransition in={_in} appear={true} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_nigthzero_oneeight" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="27.076171875" height="23.75604248046875">

                    <path d="M3.52479 10.6523C6.68885 5.66016 13.4388 1.51172 20.4701 0.246093C22.2279 -0.0351565 23.9857 -0.175781 25.4623 0.386719C26.5873 0.808594 27.5014 1.86328 26.8685 3.05859C26.3764 4.04297 25.0404 4.46484 23.9857 4.81641C17.3899 6.9929 11.7023 11.2957 7.81385 17.0508C6.4076 19.1602 4.29822 24.9961 1.69666 23.5195C-1.04553 21.9023 -0.483026 16.8398 3.52479 10.6523L3.52479 10.6523Z" />
                  </svg>
                </CSSTransition>
                <CSSTransition in={_in} appear={true} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_nigthzero_onenigth" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="57.9921875" height="57.3336181640625">

                    <path d="M19.3198 16.9039C18.0541 18.8727 16.6479 20.9118 15.1713 22.8805C14.9745 23.1196 14.876 23.4149 14.8901 23.7243L14.8901 55.9274C14.8901 56.7008 14.2573 57.3336 13.4838 57.3336L10.601 57.3336C9.82758 57.3336 9.19477 56.7008 9.19477 55.9274L9.19477 33.2164C9.19202 33.0318 9.15292 32.8494 9.0797 32.6799C9.00647 32.5103 8.90057 32.3568 8.76802 32.2282C8.63548 32.0996 8.47889 31.9983 8.30721 31.9302C8.13552 31.8621 7.95211 31.8285 7.76743 31.8313C7.40883 31.8383 7.05727 31.9789 6.80415 32.2321C5.89008 33.1461 4.90571 33.9196 3.99165 34.7633C3.84998 34.8821 3.68628 34.9718 3.50991 35.0273C3.33354 35.0828 3.14796 35.1029 2.96379 35.0866C2.77962 35.0703 2.60048 35.0178 2.43661 34.9322C2.27275 34.8465 2.12738 34.7294 2.00883 34.5875C1.93149 34.4891 1.86118 34.3836 1.81196 34.2711C1.31977 33.2868 0.757272 32.0914 0.194772 31.1071C-0.163822 30.4953 -0.0161657 29.7149 0.546334 29.2789C5.95894 25.1628 10.621 20.1439 14.3276 14.443C14.6932 13.8453 15.4385 13.6063 16.0854 13.8805L18.6166 14.9352C19.3127 15.1672 19.6854 15.9196 19.4534 16.6157C19.4182 16.7211 19.376 16.8125 19.3198 16.9039ZM18.1948 3.40394C14.3658 8.2768 9.86837 12.5849 4.8354 16.2008C4.20962 16.6578 3.33071 16.5172 2.87368 15.8914C2.84555 15.8563 2.82446 15.8141 2.79633 15.7789C2.37446 15.0055 1.81196 14.1618 1.31977 13.3883C0.897897 12.7414 1.05258 11.8766 1.67133 11.4196C5.89008 8.46644 10.3901 4.318 12.9916 0.591438C13.3432 0.0640947 14.0182 -0.139812 14.6088 0.0992503L17.4916 1.22425C18.2299 1.45628 18.6448 2.25081 18.4057 2.98909C18.3565 3.13675 18.2862 3.27738 18.1948 3.40394ZM56.5854 40.5993L50.3979 40.5993C49.6245 40.5993 48.9916 41.2321 48.9916 42.0055L48.9916 51.2868C48.9916 54.1696 48.4291 55.6461 46.3198 56.4196C44.4213 57.2633 41.7495 57.3336 37.812 57.3336C37.2002 57.3336 36.6588 56.9328 36.476 56.3493C36.2651 55.6461 35.9838 54.8024 35.7026 54.0289C35.4284 53.3047 35.801 52.4891 36.5252 52.2219C36.687 52.1586 36.8627 52.1305 37.0385 52.1305C39.2885 52.2008 41.187 52.2008 41.8901 52.1305C42.8041 52.0602 43.0854 51.8493 43.0854 51.1461L43.0854 42.0055C43.0854 41.2321 42.4526 40.5993 41.6791 40.5993L20.2338 40.5993C19.4604 40.5993 18.8276 39.9664 18.8276 39.193L18.8276 36.943C18.8276 36.1696 19.4604 35.5367 20.2338 35.5367L41.6791 35.5367C42.4526 35.5367 43.0854 34.9039 43.0854 34.1305L43.0854 32.3024C43.0854 31.5289 42.4526 30.8961 41.6791 30.8961L21.7807 30.8961C21.0073 30.8961 20.3745 30.2633 20.3745 29.4899L20.3745 27.4508C20.3745 26.6774 21.0073 26.0446 21.7807 26.0446L55.3901 26.0446C56.1635 26.0446 56.7963 26.6774 56.7963 27.4508L56.7963 29.4899C56.7963 30.2633 56.1635 30.8961 55.3901 30.8961L50.3979 30.8961C49.6245 30.8961 48.9916 31.5289 48.9916 32.3024L48.9916 34.1305C48.9916 34.9039 49.6245 35.5367 50.3979 35.5367L56.5854 35.5367C57.3588 35.5367 57.9916 36.1696 57.9916 36.943L57.9916 39.193C58.0127 39.9453 57.4221 40.5782 56.6698 40.5993L56.5854 40.5993ZM28.6713 41.7946C30.8581 43.8125 32.876 45.9993 34.7182 48.3336C35.1893 48.9243 35.0909 49.7821 34.5073 50.2532C34.4868 50.2734 34.463 50.2901 34.437 50.3024L32.3276 51.7789C31.6948 52.2289 30.8159 52.0742 30.3659 51.4414C30.3659 51.4344 30.3588 51.4344 30.3588 51.4274C28.6576 49.1243 26.8018 46.9396 24.8041 44.8883C24.2487 44.3117 24.2698 43.4047 24.8463 42.8493C24.9026 42.8 24.9588 42.7508 25.0151 42.7086L26.8432 41.5133C27.4479 41.211 28.1791 41.3235 28.6713 41.7946ZM52.8588 22.5289L25.2963 22.5289C24.5229 22.5289 23.8901 21.8961 23.8901 21.1227L23.8901 2.84144C23.8901 2.068 24.5229 1.43519 25.2963 1.43519L52.8588 1.43519C53.6323 1.43519 54.2651 2.068 54.2651 2.84144L54.2651 21.1227C54.2651 21.4956 54.1169 21.8533 53.8532 22.1171C53.5895 22.3808 53.2318 22.5289 52.8588 22.5289ZM46.9526 5.72425L30.9916 5.72425C30.2182 5.72425 29.5854 6.35706 29.5854 7.1305L29.5854 8.53675C29.5854 9.31019 30.2182 9.943 30.9916 9.943L46.9526 9.943C47.726 9.943 48.3588 9.31019 48.3588 8.53675L48.3588 7.1305C48.3588 6.35706 47.726 5.72425 46.9526 5.72425ZM46.9526 13.9508L30.9916 13.9508C30.2182 13.9508 29.5854 14.5836 29.5854 15.3571L29.5854 16.7633C29.5854 17.5368 30.2182 18.1696 30.9916 18.1696L46.9526 18.1696C47.726 18.1696 48.3588 17.5368 48.3588 16.7633L48.3588 15.3571C48.3588 14.5836 47.726 13.9508 46.9526 13.9508Z" />
                  </svg>
                </CSSTransition>
                <CSSTransition in={_in} appear={true} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_nigthzero_twoozero" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="27.076171875" height="23.75604248046875">

                    <path d="M3.52479 10.6523C6.68885 5.66016 13.4388 1.51172 20.4701 0.246093C22.2279 -0.0351565 23.9857 -0.175781 25.4623 0.386719C26.5873 0.808594 27.5014 1.86328 26.8685 3.05859C26.3764 4.04297 25.0404 4.46484 23.9857 4.81641C17.3899 6.9929 11.7023 11.2957 7.81385 17.0508C6.4076 19.1602 4.29822 24.9961 1.69666 23.5195C-1.04553 21.9023 -0.483026 16.8398 3.52479 10.6523L3.52479 10.6523Z" />
                  </svg>
                </CSSTransition>
              </div>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} classNames={transaction['frameonezero']?.animationClass || {}}>

          <div id="id_nigthzero_twooone" className={` frame frameonezero cursor ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonezeroStyle , transitionDuration: transaction['frameonezero']?.duration, transitionTimingFunction: transaction['frameonezero']?.timingFunction } } onClick={ props.FrameonezeroonClick || function(e){ SingletoneNavigation.setTransitionInstance(null, 'Desktopfive' ); history.push('/4'); }} onMouseEnter={ props.FrameonezeroonMouseEnter } onMouseOver={ props.FrameonezeroonMouseOver } onKeyPress={ props.FrameonezeroonKeyPress } onDrag={ props.FrameonezeroonDrag } onMouseLeave={ props.FrameonezeroonMouseLeave } onMouseUp={ props.FrameonezeroonMouseUp } onMouseDown={ props.FrameonezeroonMouseDown } onKeyDown={ props.FrameonezeroonKeyDown } onChange={ props.FrameonezeroonChange } ondelay={ props.Frameonezeroondelay }>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameonetwoo']?.animationClass || {}}>

              <div id="id_nigthzero_twootwoo" className={` frame frameonetwoo cursor ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonetwooStyle , transitionDuration: transaction['frameonetwoo']?.duration, transitionTimingFunction: transaction['frameonetwoo']?.timingFunction } } onClick={ props.FrameonetwooonClick || function(e){ SingletoneNavigation.setTransitionInstance(null, 'Desktoptwoo' ); history.push('/1'); }} onMouseEnter={ props.FrameonetwooonMouseEnter } onMouseOver={ props.FrameonetwooonMouseOver } onKeyPress={ props.FrameonetwooonKeyPress } onDrag={ props.FrameonetwooonDrag } onMouseLeave={ props.FrameonetwooonMouseLeave } onMouseUp={ props.FrameonetwooonMouseUp } onMouseDown={ props.FrameonetwooonMouseDown } onKeyDown={ props.FrameonetwooonKeyDown } onChange={ props.FrameonetwooonChange } ondelay={ props.Frameonetwooondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['eliminar']?.animationClass || {}}>

                  <span id="id_nigthzero_twoothree"  className={` text eliminar    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.ELIMINARStyle , transitionDuration: transaction['eliminar']?.duration, transitionTimingFunction: transaction['eliminar']?.timingFunction }} onClick={ props.ELIMINARonClick } onMouseEnter={ props.ELIMINARonMouseEnter } onMouseOver={ props.ELIMINARonMouseOver } onKeyPress={ props.ELIMINARonKeyPress } onDrag={ props.ELIMINARonDrag } onMouseLeave={ props.ELIMINARonMouseLeave } onMouseUp={ props.ELIMINARonMouseUp } onMouseDown={ props.ELIMINARonMouseDown } onKeyDown={ props.ELIMINARonKeyDown } onChange={ props.ELIMINARonChange } ondelay={ props.ELIMINARondelay } >{props.ELIMINAR0 || `ELIMINAR`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameonetwoo']?.animationClass || {}}>

              <div id="id_nigthzero_twoofour" className={` frame frameonetwoo ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonetwooStyle , transitionDuration: transaction['frameonetwoo']?.duration, transitionTimingFunction: transaction['frameonetwoo']?.timingFunction } } onClick={ props.FrameonetwooonClick } onMouseEnter={ props.FrameonetwooonMouseEnter } onMouseOver={ props.FrameonetwooonMouseOver } onKeyPress={ props.FrameonetwooonKeyPress } onDrag={ props.FrameonetwooonDrag } onMouseLeave={ props.FrameonetwooonMouseLeave } onMouseUp={ props.FrameonetwooonMouseUp } onMouseDown={ props.FrameonetwooonMouseDown } onKeyDown={ props.FrameonetwooonKeyDown } onChange={ props.FrameonetwooonChange } ondelay={ props.Frameonetwooondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['actualizar']?.animationClass || {}}>

                  <span id="id_nigthzero_twoofive"  className={` text actualizar    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.ACTUALIZARStyle , transitionDuration: transaction['actualizar']?.duration, transitionTimingFunction: transaction['actualizar']?.timingFunction }} onClick={ props.ACTUALIZARonClick } onMouseEnter={ props.ACTUALIZARonMouseEnter } onMouseOver={ props.ACTUALIZARonMouseOver } onKeyPress={ props.ACTUALIZARonKeyPress } onDrag={ props.ACTUALIZARonDrag } onMouseLeave={ props.ACTUALIZARonMouseLeave } onMouseUp={ props.ACTUALIZARonMouseUp } onMouseDown={ props.ACTUALIZARonMouseDown } onKeyDown={ props.ACTUALIZARonKeyDown } onChange={ props.ACTUALIZARonChange } ondelay={ props.ACTUALIZARondelay } >{props.ACTUALIZAR0 || `ACTUALIZAR`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameonetwoo']?.animationClass || {}}>

              <div id="id_nigthzero_twoosix" className={` frame frameonetwoo ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonetwooStyle , transitionDuration: transaction['frameonetwoo']?.duration, transitionTimingFunction: transaction['frameonetwoo']?.timingFunction } } onClick={ props.FrameonetwooonClick } onMouseEnter={ props.FrameonetwooonMouseEnter } onMouseOver={ props.FrameonetwooonMouseOver } onKeyPress={ props.FrameonetwooonKeyPress } onDrag={ props.FrameonetwooonDrag } onMouseLeave={ props.FrameonetwooonMouseLeave } onMouseUp={ props.FrameonetwooonMouseUp } onMouseDown={ props.FrameonetwooonMouseDown } onKeyDown={ props.FrameonetwooonKeyDown } onChange={ props.FrameonetwooonChange } ondelay={ props.Frameonetwooondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['reportes']?.animationClass || {}}>

                  <span id="id_nigthzero_twooseven"  className={` text reportes    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.REPORTESStyle , transitionDuration: transaction['reportes']?.duration, transitionTimingFunction: transaction['reportes']?.timingFunction }} onClick={ props.REPORTESonClick } onMouseEnter={ props.REPORTESonMouseEnter } onMouseOver={ props.REPORTESonMouseOver } onKeyPress={ props.REPORTESonKeyPress } onDrag={ props.REPORTESonDrag } onMouseLeave={ props.REPORTESonMouseLeave } onMouseUp={ props.REPORTESonMouseUp } onMouseDown={ props.REPORTESonMouseDown } onKeyDown={ props.REPORTESonKeyDown } onChange={ props.REPORTESonChange } ondelay={ props.REPORTESondelay } >{props.REPORTES0 || `REPORTES`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameonetwoo']?.animationClass || {}}>

              <div id="id_nigthzero_threezero" className={` frame frameonetwoo cursor ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonetwooStyle , transitionDuration: transaction['frameonetwoo']?.duration, transitionTimingFunction: transaction['frameonetwoo']?.timingFunction } } onClick={ props.FrameonetwooonClick || function(e){ SingletoneNavigation.setTransitionInstance(null, 'Desktopone' ); history.push('/'); }} onMouseEnter={ props.FrameonetwooonMouseEnter } onMouseOver={ props.FrameonetwooonMouseOver } onKeyPress={ props.FrameonetwooonKeyPress } onDrag={ props.FrameonetwooonDrag } onMouseLeave={ props.FrameonetwooonMouseLeave } onMouseUp={ props.FrameonetwooonMouseUp } onMouseDown={ props.FrameonetwooonMouseDown } onKeyDown={ props.FrameonetwooonKeyDown } onChange={ props.FrameonetwooonChange } ondelay={ props.Frameonetwooondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['salir']?.animationClass || {}}>

                  <span id="id_nigthzero_threeone"  className={` text salir    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.SALIRStyle , transitionDuration: transaction['salir']?.duration, transitionTimingFunction: transaction['salir']?.timingFunction }} onClick={ props.SALIRonClick } onMouseEnter={ props.SALIRonMouseEnter } onMouseOver={ props.SALIRonMouseOver } onKeyPress={ props.SALIRonKeyPress } onDrag={ props.SALIRonDrag } onMouseLeave={ props.SALIRonMouseLeave } onMouseUp={ props.SALIRonMouseUp } onMouseDown={ props.SALIRonMouseDown } onKeyDown={ props.SALIRonKeyDown } onChange={ props.SALIRonChange } ondelay={ props.SALIRondelay } >{props.SALIR0 || `SALIR`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['menu']?.animationClass || {}}>

              <span id="id_nigthzero_threetwoo"  className={` text menu   cursor ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.MENUStyle , transitionDuration: transaction['menu']?.duration, transitionTimingFunction: transaction['menu']?.timingFunction }} onClick={ props.MENUonClick || function(e){ SingletoneNavigation.setTransitionInstance(null, 'Desktopfive');
history.push('/4'); }} onMouseEnter={ props.MENUonMouseEnter } onMouseOver={ props.MENUonMouseOver } onKeyPress={ props.MENUonKeyPress } onDrag={ props.MENUonDrag } onMouseLeave={ props.MENUonMouseLeave } onMouseUp={ props.MENUonMouseUp } onMouseDown={ props.MENUonMouseDown } onKeyDown={ props.MENUonKeyDown } onChange={ props.MENUonChange } ondelay={ props.MENUondelay } >{props.MENU0 || `MENU`}</span>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameoneone']?.animationClass || {}}>

              <div id="id_nigthzero_threethree" className={` frame frameoneone cursor ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameoneoneStyle , transitionDuration: transaction['frameoneone']?.duration, transitionTimingFunction: transaction['frameoneone']?.timingFunction } } onClick={ props.FrameoneoneonClick || function(e){ SingletoneNavigation.setTransitionInstance(null, 'Desktopfour' ); history.push('/3'); }} onMouseEnter={ props.FrameoneoneonMouseEnter } onMouseOver={ props.FrameoneoneonMouseOver } onKeyPress={ props.FrameoneoneonKeyPress } onDrag={ props.FrameoneoneonDrag } onMouseLeave={ props.FrameoneoneonMouseLeave } onMouseUp={ props.FrameoneoneonMouseUp } onMouseDown={ props.FrameoneoneonMouseDown } onKeyDown={ props.FrameoneoneonKeyDown } onChange={ props.FrameoneoneonChange } ondelay={ props.Frameoneoneondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['crear']?.animationClass || {}}>

                  <span id="id_nigthzero_threefour"  className={` text crear    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.CREARStyle , transitionDuration: transaction['crear']?.duration, transitionTimingFunction: transaction['crear']?.timingFunction }} onClick={ props.CREARonClick } onMouseEnter={ props.CREARonMouseEnter } onMouseOver={ props.CREARonMouseOver } onKeyPress={ props.CREARonKeyPress } onDrag={ props.CREARonDrag } onMouseLeave={ props.CREARonMouseLeave } onMouseUp={ props.CREARonMouseUp } onMouseDown={ props.CREARonMouseDown } onKeyDown={ props.CREARonKeyDown } onChange={ props.CREARonChange } ondelay={ props.CREARondelay } >{props.CREAR0 || `CREAR`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} classNames={transaction['frameonefour']?.animationClass || {}}>

          <div id="id_nigthzero_threefive" className={` frame frameonefour ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonefourStyle , transitionDuration: transaction['frameonefour']?.duration, transitionTimingFunction: transaction['frameonefour']?.timingFunction } } onClick={ props.FrameonefouronClick } onMouseEnter={ props.FrameonefouronMouseEnter } onMouseOver={ props.FrameonefouronMouseOver } onKeyPress={ props.FrameonefouronKeyPress } onDrag={ props.FrameonefouronDrag } onMouseLeave={ props.FrameonefouronMouseLeave } onMouseUp={ props.FrameonefouronMouseUp } onMouseDown={ props.FrameonefouronMouseDown } onKeyDown={ props.FrameonefouronKeyDown } onChange={ props.FrameonefouronChange } ondelay={ props.Frameonefourondelay }>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameoneseven']?.animationClass || {}}>

              <div id="id_nigthfour_foursix" className={` frame frameoneseven ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonesevenStyle , transitionDuration: transaction['frameoneseven']?.duration, transitionTimingFunction: transaction['frameoneseven']?.timingFunction } } onClick={ props.FrameonesevenonClick } onMouseEnter={ props.FrameonesevenonMouseEnter } onMouseOver={ props.FrameonesevenonMouseOver } onKeyPress={ props.FrameonesevenonKeyPress } onDrag={ props.FrameonesevenonDrag } onMouseLeave={ props.FrameonesevenonMouseLeave } onMouseUp={ props.FrameonesevenonMouseUp } onMouseDown={ props.FrameonesevenonMouseDown } onKeyDown={ props.FrameonesevenonKeyDown } onChange={ props.FrameonesevenonChange } ondelay={ props.Frameonesevenondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['edad']?.animationClass || {}}>

                  <span id="id_nigthfour_fourseven"  className={` text edad    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.EDADStyle , transitionDuration: transaction['edad']?.duration, transitionTimingFunction: transaction['edad']?.timingFunction }} onClick={ props.EDADonClick } onMouseEnter={ props.EDADonMouseEnter } onMouseOver={ props.EDADonMouseOver } onKeyPress={ props.EDADonKeyPress } onDrag={ props.EDADonDrag } onMouseLeave={ props.EDADonMouseLeave } onMouseUp={ props.EDADonMouseUp } onMouseDown={ props.EDADonMouseDown } onKeyDown={ props.EDADonKeyDown } onChange={ props.EDADonChange } ondelay={ props.EDADondelay } >{props.EDAD0 || `EDAD`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameoneeight']?.animationClass || {}}>

              <div id="id_nigthfour_foureight" className={` frame frameoneeight ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameoneeightStyle , transitionDuration: transaction['frameoneeight']?.duration, transitionTimingFunction: transaction['frameoneeight']?.timingFunction } } onClick={ props.FrameoneeightonClick } onMouseEnter={ props.FrameoneeightonMouseEnter } onMouseOver={ props.FrameoneeightonMouseOver } onKeyPress={ props.FrameoneeightonKeyPress } onDrag={ props.FrameoneeightonDrag } onMouseLeave={ props.FrameoneeightonMouseLeave } onMouseUp={ props.FrameoneeightonMouseUp } onMouseDown={ props.FrameoneeightonMouseDown } onKeyDown={ props.FrameoneeightonKeyDown } onChange={ props.FrameoneeightonChange } ondelay={ props.Frameoneeightondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['puesto']?.animationClass || {}}>

                  <span id="id_nigthfour_fournigth"  className={` text puesto    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.PUESTOStyle , transitionDuration: transaction['puesto']?.duration, transitionTimingFunction: transaction['puesto']?.timingFunction }} onClick={ props.PUESTOonClick } onMouseEnter={ props.PUESTOonMouseEnter } onMouseOver={ props.PUESTOonMouseOver } onKeyPress={ props.PUESTOonKeyPress } onDrag={ props.PUESTOonDrag } onMouseLeave={ props.PUESTOonMouseLeave } onMouseUp={ props.PUESTOonMouseUp } onMouseDown={ props.PUESTOonMouseDown } onKeyDown={ props.PUESTOonKeyDown } onChange={ props.PUESTOonChange } ondelay={ props.PUESTOondelay } >{props.PUESTO0 || `PUESTO`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameonenigth']?.animationClass || {}}>

              <div id="id_nigthfour_fivezero" className={` frame frameonenigth ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonenigthStyle , transitionDuration: transaction['frameonenigth']?.duration, transitionTimingFunction: transaction['frameonenigth']?.timingFunction } } onClick={ props.FrameonenigthonClick } onMouseEnter={ props.FrameonenigthonMouseEnter } onMouseOver={ props.FrameonenigthonMouseOver } onKeyPress={ props.FrameonenigthonKeyPress } onDrag={ props.FrameonenigthonDrag } onMouseLeave={ props.FrameonenigthonMouseLeave } onMouseUp={ props.FrameonenigthonMouseUp } onMouseDown={ props.FrameonenigthonMouseDown } onKeyDown={ props.FrameonenigthonKeyDown } onChange={ props.FrameonenigthonChange } ondelay={ props.Frameonenigthondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['puesto']?.animationClass || {}}>

                  <span id="id_nigthfour_fiveone"  className={` text puesto    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.PUESTOStyle , transitionDuration: transaction['puesto']?.duration, transitionTimingFunction: transaction['puesto']?.timingFunction }} onClick={ props.PUESTOonClick } onMouseEnter={ props.PUESTOonMouseEnter } onMouseOver={ props.PUESTOonMouseOver } onKeyPress={ props.PUESTOonKeyPress } onDrag={ props.PUESTOonDrag } onMouseLeave={ props.PUESTOonMouseLeave } onMouseUp={ props.PUESTOonMouseUp } onMouseDown={ props.PUESTOonMouseDown } onKeyDown={ props.PUESTOonKeyDown } onChange={ props.PUESTOonChange } ondelay={ props.PUESTOondelay } >{props.PUESTO1 || `PUESTO`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frametwoozero']?.animationClass || {}}>

              <div id="id_oneoneone_twoonigth" className={` frame frametwoozero ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrametwoozeroStyle , transitionDuration: transaction['frametwoozero']?.duration, transitionTimingFunction: transaction['frametwoozero']?.timingFunction } } onClick={ props.FrametwoozeroonClick } onMouseEnter={ props.FrametwoozeroonMouseEnter } onMouseOver={ props.FrametwoozeroonMouseOver } onKeyPress={ props.FrametwoozeroonKeyPress } onDrag={ props.FrametwoozeroonDrag } onMouseLeave={ props.FrametwoozeroonMouseLeave } onMouseUp={ props.FrametwoozeroonMouseUp } onMouseDown={ props.FrametwoozeroonMouseDown } onKeyDown={ props.FrametwoozeroonKeyDown } onChange={ props.FrametwoozeroonChange } ondelay={ props.Frametwoozeroondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['frameoneseven']?.animationClass || {}}>

                  <div id="id_oneoneone_threezero" className={` frame frameoneseven ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonesevenStyle , transitionDuration: transaction['frameoneseven']?.duration, transitionTimingFunction: transaction['frameoneseven']?.timingFunction } } onClick={ props.FrameonesevenonClick } onMouseEnter={ props.FrameonesevenonMouseEnter } onMouseOver={ props.FrameonesevenonMouseOver } onKeyPress={ props.FrameonesevenonKeyPress } onDrag={ props.FrameonesevenonDrag } onMouseLeave={ props.FrameonesevenonMouseLeave } onMouseUp={ props.FrameonesevenonMouseUp } onMouseDown={ props.FrameonesevenonMouseDown } onKeyDown={ props.FrameonesevenonKeyDown } onChange={ props.FrameonesevenonChange } ondelay={ props.Frameonesevenondelay }>
                    <CSSTransition in={_in} appear={true} classNames={transaction['frameoneseven']?.animationClass || {}}>

                      <div id="id_oneoneone_threeone" className={` frame frameoneseven ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonesevenStyle , transitionDuration: transaction['frameoneseven']?.duration, transitionTimingFunction: transaction['frameoneseven']?.timingFunction } } onClick={ props.FrameonesevenonClick } onMouseEnter={ props.FrameonesevenonMouseEnter } onMouseOver={ props.FrameonesevenonMouseOver } onKeyPress={ props.FrameonesevenonKeyPress } onDrag={ props.FrameonesevenonDrag } onMouseLeave={ props.FrameonesevenonMouseLeave } onMouseUp={ props.FrameonesevenonMouseUp } onMouseDown={ props.FrameonesevenonMouseDown } onKeyDown={ props.FrameonesevenonKeyDown } onChange={ props.FrameonesevenonChange } ondelay={ props.Frameonesevenondelay }>
                        <CSSTransition in={_in} appear={true} classNames={transaction['frameoneseven']?.animationClass || {}}>

                          <div id="id_oneoneone_threetwoo" className={` frame frameoneseven ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonesevenStyle , transitionDuration: transaction['frameoneseven']?.duration, transitionTimingFunction: transaction['frameoneseven']?.timingFunction } } onClick={ props.FrameonesevenonClick } onMouseEnter={ props.FrameonesevenonMouseEnter } onMouseOver={ props.FrameonesevenonMouseOver } onKeyPress={ props.FrameonesevenonKeyPress } onDrag={ props.FrameonesevenonDrag } onMouseLeave={ props.FrameonesevenonMouseLeave } onMouseUp={ props.FrameonesevenonMouseUp } onMouseDown={ props.FrameonesevenonMouseDown } onKeyDown={ props.FrameonesevenonKeyDown } onChange={ props.FrameonesevenonChange } ondelay={ props.Frameonesevenondelay }>

                          </div>

                        </CSSTransition>
                      </div>

                    </CSSTransition>
                  </div>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frametwooone']?.animationClass || {}}>

              <div id="id_oneoneone_threethree" className={` frame frametwooone ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrametwoooneStyle , transitionDuration: transaction['frametwooone']?.duration, transitionTimingFunction: transaction['frametwooone']?.timingFunction } } onClick={ props.FrametwoooneonClick } onMouseEnter={ props.FrametwoooneonMouseEnter } onMouseOver={ props.FrametwoooneonMouseOver } onKeyPress={ props.FrametwoooneonKeyPress } onDrag={ props.FrametwoooneonDrag } onMouseLeave={ props.FrametwoooneonMouseLeave } onMouseUp={ props.FrametwoooneonMouseUp } onMouseDown={ props.FrametwoooneonMouseDown } onKeyDown={ props.FrametwoooneonKeyDown } onChange={ props.FrametwoooneonChange } ondelay={ props.Frametwoooneondelay }>

              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frametwootwoo']?.animationClass || {}}>

              <div id="id_oneoneone_threefour" className={` frame frametwootwoo ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrametwootwooStyle , transitionDuration: transaction['frametwootwoo']?.duration, transitionTimingFunction: transaction['frametwootwoo']?.timingFunction } } onClick={ props.FrametwootwooonClick } onMouseEnter={ props.FrametwootwooonMouseEnter } onMouseOver={ props.FrametwootwooonMouseOver } onKeyPress={ props.FrametwootwooonKeyPress } onDrag={ props.FrametwootwooonDrag } onMouseLeave={ props.FrametwootwooonMouseLeave } onMouseUp={ props.FrametwootwooonMouseUp } onMouseDown={ props.FrametwootwooonMouseDown } onKeyDown={ props.FrametwootwooonKeyDown } onChange={ props.FrametwootwooonChange } ondelay={ props.Frametwootwooondelay }>

              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frametwoothree']?.animationClass || {}}>

              <div id="id_oneoneone_threefive" className={` frame frametwoothree ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrametwoothreeStyle , transitionDuration: transaction['frametwoothree']?.duration, transitionTimingFunction: transaction['frametwoothree']?.timingFunction } } onClick={ props.FrametwoothreeonClick } onMouseEnter={ props.FrametwoothreeonMouseEnter } onMouseOver={ props.FrametwoothreeonMouseOver } onKeyPress={ props.FrametwoothreeonKeyPress } onDrag={ props.FrametwoothreeonDrag } onMouseLeave={ props.FrametwoothreeonMouseLeave } onMouseUp={ props.FrametwoothreeonMouseUp } onMouseDown={ props.FrametwoothreeonMouseDown } onKeyDown={ props.FrametwoothreeonKeyDown } onChange={ props.FrametwoothreeonChange } ondelay={ props.Frametwoothreeondelay }>

              </div>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} classNames={transaction['frameonesix']?.animationClass || {}}>

          <div id="id_nigthfour_fourtwoo" className={` frame frameonesix ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonesixStyle , transitionDuration: transaction['frameonesix']?.duration, transitionTimingFunction: transaction['frameonesix']?.timingFunction } } onClick={ props.FrameonesixonClick } onMouseEnter={ props.FrameonesixonMouseEnter } onMouseOver={ props.FrameonesixonMouseOver } onKeyPress={ props.FrameonesixonKeyPress } onDrag={ props.FrameonesixonDrag } onMouseLeave={ props.FrameonesixonMouseLeave } onMouseUp={ props.FrameonesixonMouseUp } onMouseDown={ props.FrameonesixonMouseDown } onKeyDown={ props.FrameonesixonKeyDown } onChange={ props.FrameonesixonChange } ondelay={ props.Frameonesixondelay }>
            <CSSTransition in={_in} appear={true} classNames={transaction['nombre']?.animationClass || {}}>

              <span id="id_nigthfour_fourthree"  className={` text nombre    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.NOMBREStyle , transitionDuration: transaction['nombre']?.duration, transitionTimingFunction: transaction['nombre']?.timingFunction }} onClick={ props.NOMBREonClick } onMouseEnter={ props.NOMBREonMouseEnter } onMouseOver={ props.NOMBREonMouseOver } onKeyPress={ props.NOMBREonKeyPress } onDrag={ props.NOMBREonDrag } onMouseLeave={ props.NOMBREonMouseLeave } onMouseUp={ props.NOMBREonMouseUp } onMouseDown={ props.NOMBREonMouseDown } onKeyDown={ props.NOMBREonKeyDown } onChange={ props.NOMBREonChange } ondelay={ props.NOMBREondelay } >{props.NOMBRE0 || `NOMBRE`}</span>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} classNames={transaction['frameonefive']?.animationClass || {}}>

          <div id="id_nigthzero_threesix" className={` frame frameonefive ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonefiveStyle , transitionDuration: transaction['frameonefive']?.duration, transitionTimingFunction: transaction['frameonefive']?.timingFunction } } onClick={ props.FrameonefiveonClick } onMouseEnter={ props.FrameonefiveonMouseEnter } onMouseOver={ props.FrameonefiveonMouseOver } onKeyPress={ props.FrameonefiveonKeyPress } onDrag={ props.FrameonefiveonDrag } onMouseLeave={ props.FrameonefiveonMouseLeave } onMouseUp={ props.FrameonefiveonMouseUp } onMouseDown={ props.FrameonefiveonMouseDown } onKeyDown={ props.FrameonefiveonKeyDown } onChange={ props.FrameonefiveonChange } ondelay={ props.Frameonefiveondelay }>
            <CSSTransition in={_in} appear={true} classNames={transaction['formulariodealtas']?.animationClass || {}}>

              <span id="id_nigthzero_threenigth"  className={` text formulariodealtas    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.FORMULARIODEALTASStyle , transitionDuration: transaction['formulariodealtas']?.duration, transitionTimingFunction: transaction['formulariodealtas']?.timingFunction }} onClick={ props.FORMULARIODEALTASonClick } onMouseEnter={ props.FORMULARIODEALTASonMouseEnter } onMouseOver={ props.FORMULARIODEALTASonMouseOver } onKeyPress={ props.FORMULARIODEALTASonKeyPress } onDrag={ props.FORMULARIODEALTASonDrag } onMouseLeave={ props.FORMULARIODEALTASonMouseLeave } onMouseUp={ props.FORMULARIODEALTASonMouseUp } onMouseDown={ props.FORMULARIODEALTASonMouseDown } onKeyDown={ props.FORMULARIODEALTASonKeyDown } onChange={ props.FORMULARIODEALTASonChange } ondelay={ props.FORMULARIODEALTASondelay } >{props.FORMULARIODEALTAS0 || `FORMULARIO DE ALTAS`}</span>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} classNames={transaction['desktopone']?.animationClass || {}}>

          <div id="id_oneonethree_twoo" className={` frame desktopone ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.DesktoponeStyle , transitionDuration: transaction['desktopone']?.duration, transitionTimingFunction: transaction['desktopone']?.timingFunction } } onClick={ props.DesktoponeonClick } onMouseEnter={ props.DesktoponeonMouseEnter } onMouseOver={ props.DesktoponeonMouseOver } onKeyPress={ props.DesktoponeonKeyPress } onDrag={ props.DesktoponeonDrag } onMouseLeave={ props.DesktoponeonMouseLeave } onMouseUp={ props.DesktoponeonMouseUp } onMouseDown={ props.DesktoponeonMouseDown } onKeyDown={ props.DesktoponeonKeyDown } onChange={ props.DesktoponeonChange } ondelay={ props.Desktoponeondelay }>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameone']?.animationClass || {}}>

              <div id="id_oneonethree_three" className={` frame frameone ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameoneStyle , transitionDuration: transaction['frameone']?.duration, transitionTimingFunction: transaction['frameone']?.timingFunction } } onClick={ props.FrameoneonClick } onMouseEnter={ props.FrameoneonMouseEnter } onMouseOver={ props.FrameoneonMouseOver } onKeyPress={ props.FrameoneonKeyPress } onDrag={ props.FrameoneonDrag } onMouseLeave={ props.FrameoneonMouseLeave } onMouseUp={ props.FrameoneonMouseUp } onMouseDown={ props.FrameoneonMouseDown } onKeyDown={ props.FrameoneonKeyDown } onChange={ props.FrameoneonChange } ondelay={ props.Frameoneondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['framethree']?.animationClass || {}}>

                  <div id="id_oneonethree_four" className={` frame framethree ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FramethreeStyle , transitionDuration: transaction['framethree']?.duration, transitionTimingFunction: transaction['framethree']?.timingFunction } } onClick={ props.FramethreeonClick } onMouseEnter={ props.FramethreeonMouseEnter } onMouseOver={ props.FramethreeonMouseOver } onKeyPress={ props.FramethreeonKeyPress } onDrag={ props.FramethreeonDrag } onMouseLeave={ props.FramethreeonMouseLeave } onMouseUp={ props.FramethreeonMouseUp } onMouseDown={ props.FramethreeonMouseDown } onKeyDown={ props.FramethreeonKeyDown } onChange={ props.FramethreeonChange } ondelay={ props.Framethreeondelay }>
                    <CSSTransition in={_in} appear={true} classNames={transaction['twemojijapanesebargainbutton']?.animationClass || {}}>

                      <div id="id_oneonethree_five" className={` frame twemojijapanesebargainbutton ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.TwemojijapanesebargainbuttonStyle , transitionDuration: transaction['twemojijapanesebargainbutton']?.duration, transitionTimingFunction: transaction['twemojijapanesebargainbutton']?.timingFunction } } onClick={ props.TwemojijapanesebargainbuttononClick } onMouseEnter={ props.TwemojijapanesebargainbuttononMouseEnter } onMouseOver={ props.TwemojijapanesebargainbuttononMouseOver } onKeyPress={ props.TwemojijapanesebargainbuttononKeyPress } onDrag={ props.TwemojijapanesebargainbuttononDrag } onMouseLeave={ props.TwemojijapanesebargainbuttononMouseLeave } onMouseUp={ props.TwemojijapanesebargainbuttononMouseUp } onMouseDown={ props.TwemojijapanesebargainbuttononMouseDown } onKeyDown={ props.TwemojijapanesebargainbuttononKeyDown } onChange={ props.TwemojijapanesebargainbuttononChange } ondelay={ props.Twemojijapanesebargainbuttonondelay }>
                        <CSSTransition in={_in} appear={true} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_oneonethree_six" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="120" height="120">

                            <path d="M60 0C26.8633 0 0 26.8667 0 60C0 93.1367 26.8633 120 60 120C93.1367 120 120 93.1367 120 60C120 26.8667 93.1367 0 60 0ZM60 113.333C30.5467 113.333 6.66667 89.4567 6.66667 60C6.66667 30.5467 30.5467 6.66667 60 6.66667C89.4567 6.66667 113.333 30.5467 113.333 60C113.333 89.4567 89.4567 113.333 60 113.333Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_oneonethree_seven" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="79.8251953125" height="80.433349609375">

                            <path d="M10.9217 51.66C10.9217 48.1067 11.1817 44.3767 11.8751 40.3033C9.7084 42.99 7.36841 45.3333 4.94174 47.1533C4.46235 47.5693 3.84267 47.7874 3.20841 47.7633C2.34174 47.7633 1.3884 47.3267 0.608404 46.46C0.210371 45.8394 -0.000267051 45.1172 0.00173714 44.38C-0.0141825 43.915 0.0795008 43.4527 0.275219 43.0306C0.470937 42.6085 0.763204 42.2383 1.1284 41.95C8.58174 36.1433 14.2151 28.95 18.4617 21.2367C19.0684 20.1967 19.9351 19.6767 20.8884 19.6767C21.4951 19.6767 22.1017 19.85 22.6217 20.1967C23.8351 20.8033 24.3551 21.67 24.3551 22.71C24.3551 23.23 24.1817 23.7467 23.9217 24.27C22.017 27.6261 19.8737 30.841 17.5084 33.89L17.5084 77.92C17.5084 79.5667 16.2084 80.4333 14.2151 80.4333C12.3084 80.4333 10.9217 79.5667 10.9217 77.92L10.9217 51.66ZM24.2684 2.94667C24.2566 3.53407 24.0761 4.10567 23.7484 4.59333C19.6751 11.18 13.2617 17.94 6.76174 22.7933C6.16851 23.3354 5.3985 23.6434 4.59507 23.66C3.7284 23.66 2.9484 23.2267 2.1684 22.2733C1.67639 21.7248 1.39936 21.0168 1.38841 20.28C1.38841 19.3267 1.9084 18.46 2.77507 17.7667C8.9284 13.52 14.4751 7.45333 18.1151 1.3C18.4216 0.900485 18.815 0.575862 19.2654 0.350653C19.7158 0.125445 20.2115 0.00554082 20.7151 0C21.4084 -2.96059e-15 22.0151 0.173334 22.6217 0.52C23.6584 1.12667 24.2684 1.99667 24.2684 2.94667ZM68.0351 49.1467L77.4817 49.1467C79.0451 49.1467 79.8251 50.1867 79.8251 51.92C79.8251 53.5633 79.0451 54.69 77.4817 54.69L68.0351 54.69L68.0351 71.2467C68.0351 77.2267 64.9184 79.22 58.4117 79.22C55.2917 79.22 52.3517 79.0467 50.0917 78.5267C48.8817 78.2667 47.8384 77.14 47.8384 75.58C47.8384 75.32 47.8384 75.06 47.9284 74.8C48.2751 73.0667 49.4851 72.2033 50.6117 72.2033C50.7884 72.2033 50.8717 72.2033 51.0484 72.2867C53.3017 72.8967 55.9917 73.0667 57.6384 73.0667C60.9317 73.0667 61.7084 72.46 61.7084 69.6867L61.7084 54.69L24.2684 54.69C22.7084 54.69 21.9284 53.65 21.9284 52.0033C21.9284 50.1833 22.7084 49.1467 24.2684 49.1467L61.7084 49.1467L61.7084 42.47L25.4817 42.47C23.9217 42.47 23.1417 41.2567 23.1417 39.6967C23.1417 38.05 23.9217 36.9233 25.4817 36.9233L76.8784 36.9233C78.3484 36.9233 79.1317 38.05 79.1317 39.6967C79.1317 41.2567 78.3517 42.47 76.8784 42.47L68.0351 42.47L68.0351 49.1467ZM66.7317 2.51333C71.6717 2.51333 73.8417 4.42 73.8417 9.01333L73.8417 24.96C73.8417 29.5533 71.6751 31.46 66.7317 31.46L37.6117 31.46C32.5851 31.46 30.4184 29.5533 30.4184 24.96L30.4184 9.01667C30.4184 4.42333 32.5851 2.51667 37.6117 2.51667L66.7317 2.51333ZM37.0084 56.6867C37.8751 56.6867 38.7417 57.0333 39.2617 57.5533C42.2951 60.5 45.2451 63.7933 47.4117 66.74C47.7528 67.2548 47.9337 67.8591 47.9317 68.4767C47.9317 69.43 47.4951 70.38 46.5451 71.1633C45.883 71.772 45.0178 72.112 44.1184 72.1167C43.2517 72.1167 42.3851 71.7667 41.9517 71.0767C39.6164 67.7414 37.0383 64.5828 34.2384 61.6267C33.7515 61.1446 33.4717 60.4917 33.4584 59.8067C33.4584 58.94 33.8917 58.16 34.6717 57.4667C35.3481 56.9653 36.1665 56.6921 37.0084 56.6867ZM67.7751 14.3033L67.7751 9.71C67.7751 8.15 66.9951 7.71667 65.4384 7.71667L38.8284 7.71667C37.2684 7.71667 36.4884 8.15 36.4884 9.71L36.4884 14.3033L67.7751 14.3033ZM36.4851 24.1833C36.4851 25.83 37.2651 26.2633 38.8251 26.2633L65.4351 26.2633C66.9917 26.2633 67.7717 25.83 67.7717 24.1833L67.7717 19.4167L36.4851 19.4167L36.4851 24.1833Z" />
                          </svg>
                        </CSSTransition>
                      </div>

                    </CSSTransition>
                  </div>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frametwoo']?.animationClass || {}}>

              <div id="id_oneonethree_eight" className={` frame frametwoo ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrametwooStyle , transitionDuration: transaction['frametwoo']?.duration, transitionTimingFunction: transaction['frametwoo']?.timingFunction } } onClick={ props.FrametwooonClick } onMouseEnter={ props.FrametwooonMouseEnter } onMouseOver={ props.FrametwooonMouseOver } onKeyPress={ props.FrametwooonKeyPress } onDrag={ props.FrametwooonDrag } onMouseLeave={ props.FrametwooonMouseLeave } onMouseUp={ props.FrametwooonMouseUp } onMouseDown={ props.FrametwooonMouseDown } onKeyDown={ props.FrametwooonKeyDown } onChange={ props.FrametwooonChange } ondelay={ props.Frametwooondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['framefour']?.animationClass || {}}>

                  <div id="id_oneonethree_nigth" className={` frame framefour ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FramefourStyle , transitionDuration: transaction['framefour']?.duration, transitionTimingFunction: transaction['framefour']?.timingFunction } } onClick={ props.FramefouronClick } onMouseEnter={ props.FramefouronMouseEnter } onMouseOver={ props.FramefouronMouseOver } onKeyPress={ props.FramefouronKeyPress } onDrag={ props.FramefouronDrag } onMouseLeave={ props.FramefouronMouseLeave } onMouseUp={ props.FramefouronMouseUp } onMouseDown={ props.FramefouronMouseDown } onKeyDown={ props.FramefouronKeyDown } onChange={ props.FramefouronChange } ondelay={ props.Framefourondelay }>
                    <CSSTransition in={_in} appear={true} classNames={transaction['mingcuteuserfourfill']?.animationClass || {}}>

                      <div id="id_oneonethree_onezero" className={` frame mingcuteuserfourfill ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.MingcuteuserfourfillStyle , transitionDuration: transaction['mingcuteuserfourfill']?.duration, transitionTimingFunction: transaction['mingcuteuserfourfill']?.timingFunction } } onClick={ props.MingcuteuserfourfillonClick } onMouseEnter={ props.MingcuteuserfourfillonMouseEnter } onMouseOver={ props.MingcuteuserfourfillonMouseOver } onKeyPress={ props.MingcuteuserfourfillonKeyPress } onDrag={ props.MingcuteuserfourfillonDrag } onMouseLeave={ props.MingcuteuserfourfillonMouseLeave } onMouseUp={ props.MingcuteuserfourfillonMouseUp } onMouseDown={ props.MingcuteuserfourfillonMouseDown } onKeyDown={ props.MingcuteuserfourfillonKeyDown } onChange={ props.MingcuteuserfourfillonChange } ondelay={ props.Mingcuteuserfourfillondelay }>
                        <CSSTransition in={_in} appear={true} classNames={transaction['group']?.animationClass || {}}>

                          <div id="id_oneonethree_oneone" className={` group group ${ props.onClick ? 'cursor' : '' } `} style={{ ...{},  ...props.GroupStyle , transitionDuration: transaction['group']?.duration, transitionTimingFunction: transaction['group']?.timingFunction }} onClick={ props.GrouponClick } onMouseEnter={ props.GrouponMouseEnter } onMouseOver={ props.GrouponMouseOver } onKeyPress={ props.GrouponKeyPress } onDrag={ props.GrouponDrag } onMouseLeave={ props.GrouponMouseLeave } onMouseUp={ props.GrouponMouseUp } onMouseDown={ props.GrouponMouseDown } onKeyDown={ props.GrouponKeyDown } onChange={ props.GrouponChange } ondelay={ props.Groupondelay }>
                            <CSSTransition in={_in} appear={true} classNames={transaction['vector']?.animationClass || {}}>
                              <svg id="id_oneonethree_onetwoo" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="200" height="200">


                              </svg>
                            </CSSTransition>
                            <CSSTransition in={_in} appear={true} classNames={transaction['vector']?.animationClass || {}}>
                              <svg id="id_oneonethree_onethree" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="166.6669921875" height="166.6666259765625">

                                <path d="M83.3333 0C37.3083 0 0 37.3083 0 83.3333C0 129.358 37.3083 166.667 83.3333 166.667C129.358 166.667 166.667 129.358 166.667 83.3333C166.667 37.3083 129.358 0 83.3333 0ZM54.1667 62.5C54.1667 58.6698 54.9211 54.8771 56.3868 51.3384C57.8526 47.7997 60.001 44.5844 62.7094 41.8761C65.4178 39.1677 68.6331 37.0193 72.1717 35.5535C75.7104 34.0878 79.5031 33.3333 83.3333 33.3333C87.1636 33.3333 90.9563 34.0878 94.4949 35.5535C98.0336 37.0193 101.249 39.1677 103.957 41.8761C106.666 44.5844 108.814 47.7997 110.28 51.3384C111.746 54.8771 112.5 58.6698 112.5 62.5C112.5 70.2355 109.427 77.6541 103.957 83.124C98.4875 88.5938 91.0688 91.6667 83.3333 91.6667C75.5979 91.6667 68.1792 88.5938 62.7094 83.124C57.2396 77.6541 54.1667 70.2355 54.1667 62.5ZM135.483 124.867C129.245 132.714 121.315 139.05 112.285 143.402C103.254 147.754 93.3576 150.009 83.3333 150C73.3091 150.009 63.4122 147.754 54.3819 143.402C45.3517 139.05 37.4214 132.714 31.1833 124.867C44.6917 115.175 63.125 108.333 83.3333 108.333C103.542 108.333 121.975 115.175 135.483 124.867Z" />
                              </svg>
                            </CSSTransition>
                          </div>

                        </CSSTransition>
                      </div>

                    </CSSTransition>
                  </div>

                </CSSTransition>
                <CSSTransition in={_in} appear={true} classNames={transaction['framefive']?.animationClass || {}}>

                  <div id="id_oneonethree_onefour" className={` frame framefive ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FramefiveStyle , transitionDuration: transaction['framefive']?.duration, transitionTimingFunction: transaction['framefive']?.timingFunction } } onClick={ props.FramefiveonClick } onMouseEnter={ props.FramefiveonMouseEnter } onMouseOver={ props.FramefiveonMouseOver } onKeyPress={ props.FramefiveonKeyPress } onDrag={ props.FramefiveonDrag } onMouseLeave={ props.FramefiveonMouseLeave } onMouseUp={ props.FramefiveonMouseUp } onMouseDown={ props.FramefiveonMouseDown } onKeyDown={ props.FramefiveonKeyDown } onChange={ props.FramefiveonChange } ondelay={ props.Framefiveondelay }>
                    <CSSTransition in={_in} appear={true} classNames={transaction['usuario']?.animationClass || {}}>

                      <span id="id_oneonethree_onefive"  className={` text usuario    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.USUARIOStyle , transitionDuration: transaction['usuario']?.duration, transitionTimingFunction: transaction['usuario']?.timingFunction }} onClick={ props.USUARIOonClick } onMouseEnter={ props.USUARIOonMouseEnter } onMouseOver={ props.USUARIOonMouseOver } onKeyPress={ props.USUARIOonKeyPress } onDrag={ props.USUARIOonDrag } onMouseLeave={ props.USUARIOonMouseLeave } onMouseUp={ props.USUARIOonMouseUp } onMouseDown={ props.USUARIOonMouseDown } onKeyDown={ props.USUARIOonKeyDown } onChange={ props.USUARIOonChange } ondelay={ props.USUARIOondelay } >{props.USUARIO0 || `USUARIO`}</span>

                    </CSSTransition>
                  </div>

                </CSSTransition>
                <CSSTransition in={_in} appear={true} classNames={transaction['framesix']?.animationClass || {}}>

                  <div id="id_oneonethree_onesix" className={` frame framesix ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FramesixStyle , transitionDuration: transaction['framesix']?.duration, transitionTimingFunction: transaction['framesix']?.timingFunction } } onClick={ props.FramesixonClick } onMouseEnter={ props.FramesixonMouseEnter } onMouseOver={ props.FramesixonMouseOver } onKeyPress={ props.FramesixonKeyPress } onDrag={ props.FramesixonDrag } onMouseLeave={ props.FramesixonMouseLeave } onMouseUp={ props.FramesixonMouseUp } onMouseDown={ props.FramesixonMouseDown } onKeyDown={ props.FramesixonKeyDown } onChange={ props.FramesixonChange } ondelay={ props.Framesixondelay }>
                    <CSSTransition in={_in} appear={true} classNames={transaction['contrasea']?.animationClass || {}}>

                      <span id="id_oneonethree_oneseven"  className={` text contrasea    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.CONTRASEAStyle , transitionDuration: transaction['contrasea']?.duration, transitionTimingFunction: transaction['contrasea']?.timingFunction }} onClick={ props.CONTRASEAonClick } onMouseEnter={ props.CONTRASEAonMouseEnter } onMouseOver={ props.CONTRASEAonMouseOver } onKeyPress={ props.CONTRASEAonKeyPress } onDrag={ props.CONTRASEAonDrag } onMouseLeave={ props.CONTRASEAonMouseLeave } onMouseUp={ props.CONTRASEAonMouseUp } onMouseDown={ props.CONTRASEAonMouseDown } onKeyDown={ props.CONTRASEAonKeyDown } onChange={ props.CONTRASEAonChange } ondelay={ props.CONTRASEAondelay } >{props.CONTRASEA0 || `CONTRASEÑA`}</span>

                    </CSSTransition>
                  </div>

                </CSSTransition>
                <CSSTransition in={_in} appear={true} classNames={transaction['frameseven']?.animationClass || {}}>

                  <div id="id_oneonethree_oneeight" className={` frame frameseven ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FramesevenStyle , transitionDuration: transaction['frameseven']?.duration, transitionTimingFunction: transaction['frameseven']?.timingFunction } } onClick={ props.FramesevenonClick } onMouseEnter={ props.FramesevenonMouseEnter } onMouseOver={ props.FramesevenonMouseOver } onKeyPress={ props.FramesevenonKeyPress } onDrag={ props.FramesevenonDrag } onMouseLeave={ props.FramesevenonMouseLeave } onMouseUp={ props.FramesevenonMouseUp } onMouseDown={ props.FramesevenonMouseDown } onKeyDown={ props.FramesevenonKeyDown } onChange={ props.FramesevenonChange } ondelay={ props.Framesevenondelay }>
                    <CSSTransition in={_in} appear={true} classNames={transaction['login']?.animationClass || {}}>

                      <span id="id_oneonethree_onenigth"  className={` text login    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.LOGINStyle , transitionDuration: transaction['login']?.duration, transitionTimingFunction: transaction['login']?.timingFunction }} onClick={ props.LOGINonClick } onMouseEnter={ props.LOGINonMouseEnter } onMouseOver={ props.LOGINonMouseOver } onKeyPress={ props.LOGINonKeyPress } onDrag={ props.LOGINonDrag } onMouseLeave={ props.LOGINonMouseLeave } onMouseUp={ props.LOGINonMouseUp } onMouseDown={ props.LOGINonMouseDown } onKeyDown={ props.LOGINonKeyDown } onChange={ props.LOGINonChange } ondelay={ props.LOGINondelay } >{props.LOGIN0 || `LOGIN`}</span>

                    </CSSTransition>
                  </div>

                </CSSTransition>
              </div>

            </CSSTransition>
          </div>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Desktopthree.propTypes = {
    style: PropTypes.any,
COLEGIOJAPONESDEMORELOS0: PropTypes.any,
ELIMINAR0: PropTypes.any,
ACTUALIZAR0: PropTypes.any,
REPORTES0: PropTypes.any,
SALIR0: PropTypes.any,
MENU0: PropTypes.any,
CREAR0: PropTypes.any,
EDAD0: PropTypes.any,
PUESTO0: PropTypes.any,
PUESTO1: PropTypes.any,
NOMBRE0: PropTypes.any,
FORMULARIODEALTAS0: PropTypes.any,
USUARIO0: PropTypes.any,
CONTRASEA0: PropTypes.any,
LOGIN0: PropTypes.any,
DesktopthreeonClick: PropTypes.any,
DesktopthreeonMouseEnter: PropTypes.any,
DesktopthreeonMouseOver: PropTypes.any,
DesktopthreeonKeyPress: PropTypes.any,
DesktopthreeonDrag: PropTypes.any,
DesktopthreeonMouseLeave: PropTypes.any,
DesktopthreeonMouseUp: PropTypes.any,
DesktopthreeonMouseDown: PropTypes.any,
DesktopthreeonKeyDown: PropTypes.any,
DesktopthreeonChange: PropTypes.any,
Desktopthreeondelay: PropTypes.any,
FrameeightonClick: PropTypes.any,
FrameeightonMouseEnter: PropTypes.any,
FrameeightonMouseOver: PropTypes.any,
FrameeightonKeyPress: PropTypes.any,
FrameeightonDrag: PropTypes.any,
FrameeightonMouseLeave: PropTypes.any,
FrameeightonMouseUp: PropTypes.any,
FrameeightonMouseDown: PropTypes.any,
FrameeightonKeyDown: PropTypes.any,
FrameeightonChange: PropTypes.any,
Frameeightondelay: PropTypes.any,
COLEGIOJAPONESDEMORELOSonClick: PropTypes.any,
COLEGIOJAPONESDEMORELOSonMouseEnter: PropTypes.any,
COLEGIOJAPONESDEMORELOSonMouseOver: PropTypes.any,
COLEGIOJAPONESDEMORELOSonKeyPress: PropTypes.any,
COLEGIOJAPONESDEMORELOSonDrag: PropTypes.any,
COLEGIOJAPONESDEMORELOSonMouseLeave: PropTypes.any,
COLEGIOJAPONESDEMORELOSonMouseUp: PropTypes.any,
COLEGIOJAPONESDEMORELOSonMouseDown: PropTypes.any,
COLEGIOJAPONESDEMORELOSonKeyDown: PropTypes.any,
COLEGIOJAPONESDEMORELOSonChange: PropTypes.any,
COLEGIOJAPONESDEMORELOSondelay: PropTypes.any,
FramenigthonClick: PropTypes.any,
FramenigthonMouseEnter: PropTypes.any,
FramenigthonMouseOver: PropTypes.any,
FramenigthonKeyPress: PropTypes.any,
FramenigthonDrag: PropTypes.any,
FramenigthonMouseLeave: PropTypes.any,
FramenigthonMouseUp: PropTypes.any,
FramenigthonMouseDown: PropTypes.any,
FramenigthonKeyDown: PropTypes.any,
FramenigthonChange: PropTypes.any,
Framenigthondelay: PropTypes.any,
NotojapanesebargainbuttononClick: PropTypes.any,
NotojapanesebargainbuttononMouseEnter: PropTypes.any,
NotojapanesebargainbuttononMouseOver: PropTypes.any,
NotojapanesebargainbuttononKeyPress: PropTypes.any,
NotojapanesebargainbuttononDrag: PropTypes.any,
NotojapanesebargainbuttononMouseLeave: PropTypes.any,
NotojapanesebargainbuttononMouseUp: PropTypes.any,
NotojapanesebargainbuttononMouseDown: PropTypes.any,
NotojapanesebargainbuttononKeyDown: PropTypes.any,
NotojapanesebargainbuttononChange: PropTypes.any,
Notojapanesebargainbuttonondelay: PropTypes.any,
VectoronClick: PropTypes.any,
VectoronMouseEnter: PropTypes.any,
VectoronMouseOver: PropTypes.any,
VectoronKeyPress: PropTypes.any,
VectoronDrag: PropTypes.any,
VectoronMouseLeave: PropTypes.any,
VectoronMouseUp: PropTypes.any,
VectoronMouseDown: PropTypes.any,
VectoronKeyDown: PropTypes.any,
VectoronChange: PropTypes.any,
Vectorondelay: PropTypes.any,
FrameonezeroonClick: PropTypes.any,
FrameonezeroonMouseEnter: PropTypes.any,
FrameonezeroonMouseOver: PropTypes.any,
FrameonezeroonKeyPress: PropTypes.any,
FrameonezeroonDrag: PropTypes.any,
FrameonezeroonMouseLeave: PropTypes.any,
FrameonezeroonMouseUp: PropTypes.any,
FrameonezeroonMouseDown: PropTypes.any,
FrameonezeroonKeyDown: PropTypes.any,
FrameonezeroonChange: PropTypes.any,
Frameonezeroondelay: PropTypes.any,
FrameonetwooonClick: PropTypes.any,
FrameonetwooonMouseEnter: PropTypes.any,
FrameonetwooonMouseOver: PropTypes.any,
FrameonetwooonKeyPress: PropTypes.any,
FrameonetwooonDrag: PropTypes.any,
FrameonetwooonMouseLeave: PropTypes.any,
FrameonetwooonMouseUp: PropTypes.any,
FrameonetwooonMouseDown: PropTypes.any,
FrameonetwooonKeyDown: PropTypes.any,
FrameonetwooonChange: PropTypes.any,
Frameonetwooondelay: PropTypes.any,
ELIMINARonClick: PropTypes.any,
ELIMINARonMouseEnter: PropTypes.any,
ELIMINARonMouseOver: PropTypes.any,
ELIMINARonKeyPress: PropTypes.any,
ELIMINARonDrag: PropTypes.any,
ELIMINARonMouseLeave: PropTypes.any,
ELIMINARonMouseUp: PropTypes.any,
ELIMINARonMouseDown: PropTypes.any,
ELIMINARonKeyDown: PropTypes.any,
ELIMINARonChange: PropTypes.any,
ELIMINARondelay: PropTypes.any,
ACTUALIZARonClick: PropTypes.any,
ACTUALIZARonMouseEnter: PropTypes.any,
ACTUALIZARonMouseOver: PropTypes.any,
ACTUALIZARonKeyPress: PropTypes.any,
ACTUALIZARonDrag: PropTypes.any,
ACTUALIZARonMouseLeave: PropTypes.any,
ACTUALIZARonMouseUp: PropTypes.any,
ACTUALIZARonMouseDown: PropTypes.any,
ACTUALIZARonKeyDown: PropTypes.any,
ACTUALIZARonChange: PropTypes.any,
ACTUALIZARondelay: PropTypes.any,
REPORTESonClick: PropTypes.any,
REPORTESonMouseEnter: PropTypes.any,
REPORTESonMouseOver: PropTypes.any,
REPORTESonKeyPress: PropTypes.any,
REPORTESonDrag: PropTypes.any,
REPORTESonMouseLeave: PropTypes.any,
REPORTESonMouseUp: PropTypes.any,
REPORTESonMouseDown: PropTypes.any,
REPORTESonKeyDown: PropTypes.any,
REPORTESonChange: PropTypes.any,
REPORTESondelay: PropTypes.any,
SALIRonClick: PropTypes.any,
SALIRonMouseEnter: PropTypes.any,
SALIRonMouseOver: PropTypes.any,
SALIRonKeyPress: PropTypes.any,
SALIRonDrag: PropTypes.any,
SALIRonMouseLeave: PropTypes.any,
SALIRonMouseUp: PropTypes.any,
SALIRonMouseDown: PropTypes.any,
SALIRonKeyDown: PropTypes.any,
SALIRonChange: PropTypes.any,
SALIRondelay: PropTypes.any,
MENUonClick: PropTypes.any,
MENUonMouseEnter: PropTypes.any,
MENUonMouseOver: PropTypes.any,
MENUonKeyPress: PropTypes.any,
MENUonDrag: PropTypes.any,
MENUonMouseLeave: PropTypes.any,
MENUonMouseUp: PropTypes.any,
MENUonMouseDown: PropTypes.any,
MENUonKeyDown: PropTypes.any,
MENUonChange: PropTypes.any,
MENUondelay: PropTypes.any,
FrameoneoneonClick: PropTypes.any,
FrameoneoneonMouseEnter: PropTypes.any,
FrameoneoneonMouseOver: PropTypes.any,
FrameoneoneonKeyPress: PropTypes.any,
FrameoneoneonDrag: PropTypes.any,
FrameoneoneonMouseLeave: PropTypes.any,
FrameoneoneonMouseUp: PropTypes.any,
FrameoneoneonMouseDown: PropTypes.any,
FrameoneoneonKeyDown: PropTypes.any,
FrameoneoneonChange: PropTypes.any,
Frameoneoneondelay: PropTypes.any,
CREARonClick: PropTypes.any,
CREARonMouseEnter: PropTypes.any,
CREARonMouseOver: PropTypes.any,
CREARonKeyPress: PropTypes.any,
CREARonDrag: PropTypes.any,
CREARonMouseLeave: PropTypes.any,
CREARonMouseUp: PropTypes.any,
CREARonMouseDown: PropTypes.any,
CREARonKeyDown: PropTypes.any,
CREARonChange: PropTypes.any,
CREARondelay: PropTypes.any,
FrameonefouronClick: PropTypes.any,
FrameonefouronMouseEnter: PropTypes.any,
FrameonefouronMouseOver: PropTypes.any,
FrameonefouronKeyPress: PropTypes.any,
FrameonefouronDrag: PropTypes.any,
FrameonefouronMouseLeave: PropTypes.any,
FrameonefouronMouseUp: PropTypes.any,
FrameonefouronMouseDown: PropTypes.any,
FrameonefouronKeyDown: PropTypes.any,
FrameonefouronChange: PropTypes.any,
Frameonefourondelay: PropTypes.any,
FrameonesevenonClick: PropTypes.any,
FrameonesevenonMouseEnter: PropTypes.any,
FrameonesevenonMouseOver: PropTypes.any,
FrameonesevenonKeyPress: PropTypes.any,
FrameonesevenonDrag: PropTypes.any,
FrameonesevenonMouseLeave: PropTypes.any,
FrameonesevenonMouseUp: PropTypes.any,
FrameonesevenonMouseDown: PropTypes.any,
FrameonesevenonKeyDown: PropTypes.any,
FrameonesevenonChange: PropTypes.any,
Frameonesevenondelay: PropTypes.any,
EDADonClick: PropTypes.any,
EDADonMouseEnter: PropTypes.any,
EDADonMouseOver: PropTypes.any,
EDADonKeyPress: PropTypes.any,
EDADonDrag: PropTypes.any,
EDADonMouseLeave: PropTypes.any,
EDADonMouseUp: PropTypes.any,
EDADonMouseDown: PropTypes.any,
EDADonKeyDown: PropTypes.any,
EDADonChange: PropTypes.any,
EDADondelay: PropTypes.any,
FrameoneeightonClick: PropTypes.any,
FrameoneeightonMouseEnter: PropTypes.any,
FrameoneeightonMouseOver: PropTypes.any,
FrameoneeightonKeyPress: PropTypes.any,
FrameoneeightonDrag: PropTypes.any,
FrameoneeightonMouseLeave: PropTypes.any,
FrameoneeightonMouseUp: PropTypes.any,
FrameoneeightonMouseDown: PropTypes.any,
FrameoneeightonKeyDown: PropTypes.any,
FrameoneeightonChange: PropTypes.any,
Frameoneeightondelay: PropTypes.any,
PUESTOonClick: PropTypes.any,
PUESTOonMouseEnter: PropTypes.any,
PUESTOonMouseOver: PropTypes.any,
PUESTOonKeyPress: PropTypes.any,
PUESTOonDrag: PropTypes.any,
PUESTOonMouseLeave: PropTypes.any,
PUESTOonMouseUp: PropTypes.any,
PUESTOonMouseDown: PropTypes.any,
PUESTOonKeyDown: PropTypes.any,
PUESTOonChange: PropTypes.any,
PUESTOondelay: PropTypes.any,
FrameonenigthonClick: PropTypes.any,
FrameonenigthonMouseEnter: PropTypes.any,
FrameonenigthonMouseOver: PropTypes.any,
FrameonenigthonKeyPress: PropTypes.any,
FrameonenigthonDrag: PropTypes.any,
FrameonenigthonMouseLeave: PropTypes.any,
FrameonenigthonMouseUp: PropTypes.any,
FrameonenigthonMouseDown: PropTypes.any,
FrameonenigthonKeyDown: PropTypes.any,
FrameonenigthonChange: PropTypes.any,
Frameonenigthondelay: PropTypes.any,
FrametwoozeroonClick: PropTypes.any,
FrametwoozeroonMouseEnter: PropTypes.any,
FrametwoozeroonMouseOver: PropTypes.any,
FrametwoozeroonKeyPress: PropTypes.any,
FrametwoozeroonDrag: PropTypes.any,
FrametwoozeroonMouseLeave: PropTypes.any,
FrametwoozeroonMouseUp: PropTypes.any,
FrametwoozeroonMouseDown: PropTypes.any,
FrametwoozeroonKeyDown: PropTypes.any,
FrametwoozeroonChange: PropTypes.any,
Frametwoozeroondelay: PropTypes.any,
FrametwoooneonClick: PropTypes.any,
FrametwoooneonMouseEnter: PropTypes.any,
FrametwoooneonMouseOver: PropTypes.any,
FrametwoooneonKeyPress: PropTypes.any,
FrametwoooneonDrag: PropTypes.any,
FrametwoooneonMouseLeave: PropTypes.any,
FrametwoooneonMouseUp: PropTypes.any,
FrametwoooneonMouseDown: PropTypes.any,
FrametwoooneonKeyDown: PropTypes.any,
FrametwoooneonChange: PropTypes.any,
Frametwoooneondelay: PropTypes.any,
FrametwootwooonClick: PropTypes.any,
FrametwootwooonMouseEnter: PropTypes.any,
FrametwootwooonMouseOver: PropTypes.any,
FrametwootwooonKeyPress: PropTypes.any,
FrametwootwooonDrag: PropTypes.any,
FrametwootwooonMouseLeave: PropTypes.any,
FrametwootwooonMouseUp: PropTypes.any,
FrametwootwooonMouseDown: PropTypes.any,
FrametwootwooonKeyDown: PropTypes.any,
FrametwootwooonChange: PropTypes.any,
Frametwootwooondelay: PropTypes.any,
FrametwoothreeonClick: PropTypes.any,
FrametwoothreeonMouseEnter: PropTypes.any,
FrametwoothreeonMouseOver: PropTypes.any,
FrametwoothreeonKeyPress: PropTypes.any,
FrametwoothreeonDrag: PropTypes.any,
FrametwoothreeonMouseLeave: PropTypes.any,
FrametwoothreeonMouseUp: PropTypes.any,
FrametwoothreeonMouseDown: PropTypes.any,
FrametwoothreeonKeyDown: PropTypes.any,
FrametwoothreeonChange: PropTypes.any,
Frametwoothreeondelay: PropTypes.any,
FrameonesixonClick: PropTypes.any,
FrameonesixonMouseEnter: PropTypes.any,
FrameonesixonMouseOver: PropTypes.any,
FrameonesixonKeyPress: PropTypes.any,
FrameonesixonDrag: PropTypes.any,
FrameonesixonMouseLeave: PropTypes.any,
FrameonesixonMouseUp: PropTypes.any,
FrameonesixonMouseDown: PropTypes.any,
FrameonesixonKeyDown: PropTypes.any,
FrameonesixonChange: PropTypes.any,
Frameonesixondelay: PropTypes.any,
NOMBREonClick: PropTypes.any,
NOMBREonMouseEnter: PropTypes.any,
NOMBREonMouseOver: PropTypes.any,
NOMBREonKeyPress: PropTypes.any,
NOMBREonDrag: PropTypes.any,
NOMBREonMouseLeave: PropTypes.any,
NOMBREonMouseUp: PropTypes.any,
NOMBREonMouseDown: PropTypes.any,
NOMBREonKeyDown: PropTypes.any,
NOMBREonChange: PropTypes.any,
NOMBREondelay: PropTypes.any,
FrameonefiveonClick: PropTypes.any,
FrameonefiveonMouseEnter: PropTypes.any,
FrameonefiveonMouseOver: PropTypes.any,
FrameonefiveonKeyPress: PropTypes.any,
FrameonefiveonDrag: PropTypes.any,
FrameonefiveonMouseLeave: PropTypes.any,
FrameonefiveonMouseUp: PropTypes.any,
FrameonefiveonMouseDown: PropTypes.any,
FrameonefiveonKeyDown: PropTypes.any,
FrameonefiveonChange: PropTypes.any,
Frameonefiveondelay: PropTypes.any,
FORMULARIODEALTASonClick: PropTypes.any,
FORMULARIODEALTASonMouseEnter: PropTypes.any,
FORMULARIODEALTASonMouseOver: PropTypes.any,
FORMULARIODEALTASonKeyPress: PropTypes.any,
FORMULARIODEALTASonDrag: PropTypes.any,
FORMULARIODEALTASonMouseLeave: PropTypes.any,
FORMULARIODEALTASonMouseUp: PropTypes.any,
FORMULARIODEALTASonMouseDown: PropTypes.any,
FORMULARIODEALTASonKeyDown: PropTypes.any,
FORMULARIODEALTASonChange: PropTypes.any,
FORMULARIODEALTASondelay: PropTypes.any,
DesktoponeonClick: PropTypes.any,
DesktoponeonMouseEnter: PropTypes.any,
DesktoponeonMouseOver: PropTypes.any,
DesktoponeonKeyPress: PropTypes.any,
DesktoponeonDrag: PropTypes.any,
DesktoponeonMouseLeave: PropTypes.any,
DesktoponeonMouseUp: PropTypes.any,
DesktoponeonMouseDown: PropTypes.any,
DesktoponeonKeyDown: PropTypes.any,
DesktoponeonChange: PropTypes.any,
Desktoponeondelay: PropTypes.any,
FrameoneonClick: PropTypes.any,
FrameoneonMouseEnter: PropTypes.any,
FrameoneonMouseOver: PropTypes.any,
FrameoneonKeyPress: PropTypes.any,
FrameoneonDrag: PropTypes.any,
FrameoneonMouseLeave: PropTypes.any,
FrameoneonMouseUp: PropTypes.any,
FrameoneonMouseDown: PropTypes.any,
FrameoneonKeyDown: PropTypes.any,
FrameoneonChange: PropTypes.any,
Frameoneondelay: PropTypes.any,
FramethreeonClick: PropTypes.any,
FramethreeonMouseEnter: PropTypes.any,
FramethreeonMouseOver: PropTypes.any,
FramethreeonKeyPress: PropTypes.any,
FramethreeonDrag: PropTypes.any,
FramethreeonMouseLeave: PropTypes.any,
FramethreeonMouseUp: PropTypes.any,
FramethreeonMouseDown: PropTypes.any,
FramethreeonKeyDown: PropTypes.any,
FramethreeonChange: PropTypes.any,
Framethreeondelay: PropTypes.any,
TwemojijapanesebargainbuttononClick: PropTypes.any,
TwemojijapanesebargainbuttononMouseEnter: PropTypes.any,
TwemojijapanesebargainbuttononMouseOver: PropTypes.any,
TwemojijapanesebargainbuttononKeyPress: PropTypes.any,
TwemojijapanesebargainbuttononDrag: PropTypes.any,
TwemojijapanesebargainbuttononMouseLeave: PropTypes.any,
TwemojijapanesebargainbuttononMouseUp: PropTypes.any,
TwemojijapanesebargainbuttononMouseDown: PropTypes.any,
TwemojijapanesebargainbuttononKeyDown: PropTypes.any,
TwemojijapanesebargainbuttononChange: PropTypes.any,
Twemojijapanesebargainbuttonondelay: PropTypes.any,
FrametwooonClick: PropTypes.any,
FrametwooonMouseEnter: PropTypes.any,
FrametwooonMouseOver: PropTypes.any,
FrametwooonKeyPress: PropTypes.any,
FrametwooonDrag: PropTypes.any,
FrametwooonMouseLeave: PropTypes.any,
FrametwooonMouseUp: PropTypes.any,
FrametwooonMouseDown: PropTypes.any,
FrametwooonKeyDown: PropTypes.any,
FrametwooonChange: PropTypes.any,
Frametwooondelay: PropTypes.any,
FramefouronClick: PropTypes.any,
FramefouronMouseEnter: PropTypes.any,
FramefouronMouseOver: PropTypes.any,
FramefouronKeyPress: PropTypes.any,
FramefouronDrag: PropTypes.any,
FramefouronMouseLeave: PropTypes.any,
FramefouronMouseUp: PropTypes.any,
FramefouronMouseDown: PropTypes.any,
FramefouronKeyDown: PropTypes.any,
FramefouronChange: PropTypes.any,
Framefourondelay: PropTypes.any,
MingcuteuserfourfillonClick: PropTypes.any,
MingcuteuserfourfillonMouseEnter: PropTypes.any,
MingcuteuserfourfillonMouseOver: PropTypes.any,
MingcuteuserfourfillonKeyPress: PropTypes.any,
MingcuteuserfourfillonDrag: PropTypes.any,
MingcuteuserfourfillonMouseLeave: PropTypes.any,
MingcuteuserfourfillonMouseUp: PropTypes.any,
MingcuteuserfourfillonMouseDown: PropTypes.any,
MingcuteuserfourfillonKeyDown: PropTypes.any,
MingcuteuserfourfillonChange: PropTypes.any,
Mingcuteuserfourfillondelay: PropTypes.any,
GrouponClick: PropTypes.any,
GrouponMouseEnter: PropTypes.any,
GrouponMouseOver: PropTypes.any,
GrouponKeyPress: PropTypes.any,
GrouponDrag: PropTypes.any,
GrouponMouseLeave: PropTypes.any,
GrouponMouseUp: PropTypes.any,
GrouponMouseDown: PropTypes.any,
GrouponKeyDown: PropTypes.any,
GrouponChange: PropTypes.any,
Groupondelay: PropTypes.any,
FramefiveonClick: PropTypes.any,
FramefiveonMouseEnter: PropTypes.any,
FramefiveonMouseOver: PropTypes.any,
FramefiveonKeyPress: PropTypes.any,
FramefiveonDrag: PropTypes.any,
FramefiveonMouseLeave: PropTypes.any,
FramefiveonMouseUp: PropTypes.any,
FramefiveonMouseDown: PropTypes.any,
FramefiveonKeyDown: PropTypes.any,
FramefiveonChange: PropTypes.any,
Framefiveondelay: PropTypes.any,
USUARIOonClick: PropTypes.any,
USUARIOonMouseEnter: PropTypes.any,
USUARIOonMouseOver: PropTypes.any,
USUARIOonKeyPress: PropTypes.any,
USUARIOonDrag: PropTypes.any,
USUARIOonMouseLeave: PropTypes.any,
USUARIOonMouseUp: PropTypes.any,
USUARIOonMouseDown: PropTypes.any,
USUARIOonKeyDown: PropTypes.any,
USUARIOonChange: PropTypes.any,
USUARIOondelay: PropTypes.any,
FramesixonClick: PropTypes.any,
FramesixonMouseEnter: PropTypes.any,
FramesixonMouseOver: PropTypes.any,
FramesixonKeyPress: PropTypes.any,
FramesixonDrag: PropTypes.any,
FramesixonMouseLeave: PropTypes.any,
FramesixonMouseUp: PropTypes.any,
FramesixonMouseDown: PropTypes.any,
FramesixonKeyDown: PropTypes.any,
FramesixonChange: PropTypes.any,
Framesixondelay: PropTypes.any,
CONTRASEAonClick: PropTypes.any,
CONTRASEAonMouseEnter: PropTypes.any,
CONTRASEAonMouseOver: PropTypes.any,
CONTRASEAonKeyPress: PropTypes.any,
CONTRASEAonDrag: PropTypes.any,
CONTRASEAonMouseLeave: PropTypes.any,
CONTRASEAonMouseUp: PropTypes.any,
CONTRASEAonMouseDown: PropTypes.any,
CONTRASEAonKeyDown: PropTypes.any,
CONTRASEAonChange: PropTypes.any,
CONTRASEAondelay: PropTypes.any,
FramesevenonClick: PropTypes.any,
FramesevenonMouseEnter: PropTypes.any,
FramesevenonMouseOver: PropTypes.any,
FramesevenonKeyPress: PropTypes.any,
FramesevenonDrag: PropTypes.any,
FramesevenonMouseLeave: PropTypes.any,
FramesevenonMouseUp: PropTypes.any,
FramesevenonMouseDown: PropTypes.any,
FramesevenonKeyDown: PropTypes.any,
FramesevenonChange: PropTypes.any,
Framesevenondelay: PropTypes.any,
LOGINonClick: PropTypes.any,
LOGINonMouseEnter: PropTypes.any,
LOGINonMouseOver: PropTypes.any,
LOGINonKeyPress: PropTypes.any,
LOGINonDrag: PropTypes.any,
LOGINonMouseLeave: PropTypes.any,
LOGINonMouseUp: PropTypes.any,
LOGINonMouseDown: PropTypes.any,
LOGINonKeyDown: PropTypes.any,
LOGINonChange: PropTypes.any,
LOGINondelay: PropTypes.any
}
export default Desktopthree;