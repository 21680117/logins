import React from "react";
import "./IMac1.css";

function IMac1(props) {
  const { children } = props;

  return (
    <div class="container-center-horizontal">
      <div className="imac-1 screen ">
        <div className="rectangle-42">
          <img
            width="100%"
            height="100%"
            src="https://img1.picmix.com/output/stamp/normal/8/5/2/9/509258_fb107.gif"
          />
        </div>
        <div className="cargando valign-text-middle bwmodelica-bold-masala-15px">Cargando</div>
      </div>
    </div>
  );
}

export default IMac1;
