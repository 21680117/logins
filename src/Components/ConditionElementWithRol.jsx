import React from 'react'
import { useAuth0 } from "@auth0/auth0-react";

const evaluateConditions = (user, condition, rol, show) => {
  let result = false;
  const userRols = user[`http://${process.env.REACT_APP_NAME}.net/roles`];
  if(userRols.length) {
      if(condition === 'Contiene') {
          if(userRols.includes(rol)) {
              result = true;
          }
      } else {
          if(!userRols.includes(rol)) {
              result = true;
          }
      }
  }
  if(show === 'Sí') {
      result = result && true;
  } else {
      result = !result;
  }
  return result;
}

export const ConditionElementWithRol = ({ rolsWithCondition, children }) => {
  const { user } = useAuth0();
  let hasPermision = rolsWithCondition.map(rol => evaluateConditions(user, rol.condition, rol.value, rol.show)).join(' && ');
  return (
    <div style={{ display: hasPermision == 'true' ? 'block' : 'none'}}>
      {children}
    </div>
  )
}