import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Desktopfour.css'





const Desktopfour = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        

        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition nodeRef={nodeRef} in={true} appear={true} timeout={10} classNames={SingletoneNavigation.getTransitionInstance()?.Desktopfour?.cssClass || '' }>

    <div id="id_oneonezero_twoo" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } desktopfour ${ props.cssClass } `} style={ { ...{ ...{}, transitionDuration: `${((SingletoneNavigation.getTransitionInstance()?.Desktopfour?.duration || 0) * 1000).toFixed(0)}ms` }, ...props.style }} onClick={ props.DesktopfouronClick } onMouseEnter={ props.DesktopfouronMouseEnter } onMouseOver={ props.DesktopfouronMouseOver } onKeyPress={ props.DesktopfouronKeyPress } onDrag={ props.DesktopfouronDrag } onMouseLeave={ props.DesktopfouronMouseLeave } onMouseUp={ props.DesktopfouronMouseUp } onMouseDown={ props.DesktopfouronMouseDown } onKeyDown={ props.DesktopfouronKeyDown } onChange={ props.DesktopfouronChange } ondelay={ props.Desktopfourondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} classNames={transaction['frameonezero']?.animationClass || {}}>

          <div id="id_oneonezero_onetwoo" className={` frame frameonezero ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonezeroStyle , transitionDuration: transaction['frameonezero']?.duration, transitionTimingFunction: transaction['frameonezero']?.timingFunction } } onClick={ props.FrameonezeroonClick } onMouseEnter={ props.FrameonezeroonMouseEnter } onMouseOver={ props.FrameonezeroonMouseOver } onKeyPress={ props.FrameonezeroonKeyPress } onDrag={ props.FrameonezeroonDrag } onMouseLeave={ props.FrameonezeroonMouseLeave } onMouseUp={ props.FrameonezeroonMouseUp } onMouseDown={ props.FrameonezeroonMouseDown } onKeyDown={ props.FrameonezeroonKeyDown } onChange={ props.FrameonezeroonChange } ondelay={ props.Frameonezeroondelay }>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameonetwoo']?.animationClass || {}}>

              <div id="id_oneonezero_oneseven" className={` frame frameonetwoo ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonetwooStyle , transitionDuration: transaction['frameonetwoo']?.duration, transitionTimingFunction: transaction['frameonetwoo']?.timingFunction } } onClick={ props.FrameonetwooonClick } onMouseEnter={ props.FrameonetwooonMouseEnter } onMouseOver={ props.FrameonetwooonMouseOver } onKeyPress={ props.FrameonetwooonKeyPress } onDrag={ props.FrameonetwooonDrag } onMouseLeave={ props.FrameonetwooonMouseLeave } onMouseUp={ props.FrameonetwooonMouseUp } onMouseDown={ props.FrameonetwooonMouseDown } onKeyDown={ props.FrameonetwooonKeyDown } onChange={ props.FrameonetwooonChange } ondelay={ props.Frameonetwooondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['reportes']?.animationClass || {}}>

                  <span id="id_oneonezero_oneeight"  className={` text reportes    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.REPORTESStyle , transitionDuration: transaction['reportes']?.duration, transitionTimingFunction: transaction['reportes']?.timingFunction }} onClick={ props.REPORTESonClick } onMouseEnter={ props.REPORTESonMouseEnter } onMouseOver={ props.REPORTESonMouseOver } onKeyPress={ props.REPORTESonKeyPress } onDrag={ props.REPORTESonDrag } onMouseLeave={ props.REPORTESonMouseLeave } onMouseUp={ props.REPORTESonMouseUp } onMouseDown={ props.REPORTESonMouseDown } onKeyDown={ props.REPORTESonKeyDown } onChange={ props.REPORTESonChange } ondelay={ props.REPORTESondelay } >{props.REPORTES0 || `REPORTES`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameonefour']?.animationClass || {}}>

              <div id="id_oneonezero_twoofoureight" className={` frame frameonefour cursor ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonefourStyle , transitionDuration: transaction['frameonefour']?.duration, transitionTimingFunction: transaction['frameonefour']?.timingFunction } } onClick={ props.FrameonefouronClick || function(e){ SingletoneNavigation.setTransitionInstance(null, 'Desktopsix' ); history.push('/5'); }} onMouseEnter={ props.FrameonefouronMouseEnter } onMouseOver={ props.FrameonefouronMouseOver } onKeyPress={ props.FrameonefouronKeyPress } onDrag={ props.FrameonefouronDrag } onMouseLeave={ props.FrameonefouronMouseLeave } onMouseUp={ props.FrameonefouronMouseUp } onMouseDown={ props.FrameonefouronMouseDown } onKeyDown={ props.FrameonefouronKeyDown } onChange={ props.FrameonefouronChange } ondelay={ props.Frameonefourondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['usuarios']?.animationClass || {}}>

                  <span id="id_oneonezero_twoofournigth"  className={` text usuarios    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.USUARIOSStyle , transitionDuration: transaction['usuarios']?.duration, transitionTimingFunction: transaction['usuarios']?.timingFunction }} onClick={ props.USUARIOSonClick } onMouseEnter={ props.USUARIOSonMouseEnter } onMouseOver={ props.USUARIOSonMouseOver } onKeyPress={ props.USUARIOSonKeyPress } onDrag={ props.USUARIOSonDrag } onMouseLeave={ props.USUARIOSonMouseLeave } onMouseUp={ props.USUARIOSonMouseUp } onMouseDown={ props.USUARIOSonMouseDown } onKeyDown={ props.USUARIOSonKeyDown } onChange={ props.USUARIOSonChange } ondelay={ props.USUARIOSondelay } >{props.USUARIOS0 || `USUARIOS`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameonetwoo']?.animationClass || {}}>

              <div id="id_oneonezero_twooone" className={` frame frameonetwoo cursor ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonetwooStyle , transitionDuration: transaction['frameonetwoo']?.duration, transitionTimingFunction: transaction['frameonetwoo']?.timingFunction } } onClick={ props.FrameonetwooonClick || function(e){ SingletoneNavigation.setTransitionInstance(null, 'Desktopone' ); history.push('/'); }} onMouseEnter={ props.FrameonetwooonMouseEnter } onMouseOver={ props.FrameonetwooonMouseOver } onKeyPress={ props.FrameonetwooonKeyPress } onDrag={ props.FrameonetwooonDrag } onMouseLeave={ props.FrameonetwooonMouseLeave } onMouseUp={ props.FrameonetwooonMouseUp } onMouseDown={ props.FrameonetwooonMouseDown } onKeyDown={ props.FrameonetwooonKeyDown } onChange={ props.FrameonetwooonChange } ondelay={ props.Frameonetwooondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['salir']?.animationClass || {}}>

                  <span id="id_oneonezero_twootwoo"  className={` text salir    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.SALIRStyle , transitionDuration: transaction['salir']?.duration, transitionTimingFunction: transaction['salir']?.timingFunction }} onClick={ props.SALIRonClick } onMouseEnter={ props.SALIRonMouseEnter } onMouseOver={ props.SALIRonMouseOver } onKeyPress={ props.SALIRonKeyPress } onDrag={ props.SALIRonDrag } onMouseLeave={ props.SALIRonMouseLeave } onMouseUp={ props.SALIRonMouseUp } onMouseDown={ props.SALIRonMouseDown } onKeyDown={ props.SALIRonKeyDown } onChange={ props.SALIRonChange } ondelay={ props.SALIRondelay } >{props.SALIR0 || `SALIR`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['menu']?.animationClass || {}}>

              <span id="id_oneonezero_twoothree"  className={` text menu   cursor ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.MENUStyle , transitionDuration: transaction['menu']?.duration, transitionTimingFunction: transaction['menu']?.timingFunction }} onClick={ props.MENUonClick || function(e){ SingletoneNavigation.setTransitionInstance(null, 'Desktopfive');
history.push('/4'); }} onMouseEnter={ props.MENUonMouseEnter } onMouseOver={ props.MENUonMouseOver } onKeyPress={ props.MENUonKeyPress } onDrag={ props.MENUonDrag } onMouseLeave={ props.MENUonMouseLeave } onMouseUp={ props.MENUonMouseUp } onMouseDown={ props.MENUonMouseDown } onKeyDown={ props.MENUonKeyDown } onChange={ props.MENUonChange } ondelay={ props.MENUondelay } >{props.MENU0 || `MENU`}</span>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameoneone']?.animationClass || {}}>

              <div id="id_oneonezero_twoofour" className={` frame frameoneone cursor ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameoneoneStyle , transitionDuration: transaction['frameoneone']?.duration, transitionTimingFunction: transaction['frameoneone']?.timingFunction } } onClick={ props.FrameoneoneonClick || function(e){ const offset=0; window.scrollTo({ behavior: "smooth" , top: document.getElementById("id_oneonezero_onetwooeight").getBoundingClientRect().top - document.body.getBoundingClientRect().top - offset }); }} onMouseEnter={ props.FrameoneoneonMouseEnter } onMouseOver={ props.FrameoneoneonMouseOver } onKeyPress={ props.FrameoneoneonKeyPress } onDrag={ props.FrameoneoneonDrag } onMouseLeave={ props.FrameoneoneonMouseLeave } onMouseUp={ props.FrameoneoneonMouseUp } onMouseDown={ props.FrameoneoneonMouseDown } onKeyDown={ props.FrameoneoneonKeyDown } onChange={ props.FrameoneoneonChange } ondelay={ props.Frameoneoneondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['personal']?.animationClass || {}}>

                  <span id="id_oneonezero_twoofive"  className={` text personal    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.PERSONALStyle , transitionDuration: transaction['personal']?.duration, transitionTimingFunction: transaction['personal']?.timingFunction }} onClick={ props.PERSONALonClick } onMouseEnter={ props.PERSONALonMouseEnter } onMouseOver={ props.PERSONALonMouseOver } onKeyPress={ props.PERSONALonKeyPress } onDrag={ props.PERSONALonDrag } onMouseLeave={ props.PERSONALonMouseLeave } onMouseUp={ props.PERSONALonMouseUp } onMouseDown={ props.PERSONALonMouseDown } onKeyDown={ props.PERSONALonKeyDown } onChange={ props.PERSONALonChange } ondelay={ props.PERSONALondelay } >{props.PERSONAL0 || `PERSONAL`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameonethree']?.animationClass || {}}>

              <div id="id_oneonezero_sevennigth" className={` frame frameonethree cursor ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonethreeStyle , transitionDuration: transaction['frameonethree']?.duration, transitionTimingFunction: transaction['frameonethree']?.timingFunction } } onClick={ props.FrameonethreeonClick || function(e){ SingletoneNavigation.setTransitionInstance(null, 'Desktopeight' ); history.push('/7'); }} onMouseEnter={ props.FrameonethreeonMouseEnter } onMouseOver={ props.FrameonethreeonMouseOver } onKeyPress={ props.FrameonethreeonKeyPress } onDrag={ props.FrameonethreeonDrag } onMouseLeave={ props.FrameonethreeonMouseLeave } onMouseUp={ props.FrameonethreeonMouseUp } onMouseDown={ props.FrameonethreeonMouseDown } onKeyDown={ props.FrameonethreeonKeyDown } onChange={ props.FrameonethreeonChange } ondelay={ props.Frameonethreeondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['puestos']?.animationClass || {}}>

                  <span id="id_oneonezero_eightzero"  className={` text puestos    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.PUESTOSStyle , transitionDuration: transaction['puestos']?.duration, transitionTimingFunction: transaction['puestos']?.timingFunction }} onClick={ props.PUESTOSonClick } onMouseEnter={ props.PUESTOSonMouseEnter } onMouseOver={ props.PUESTOSonMouseOver } onKeyPress={ props.PUESTOSonKeyPress } onDrag={ props.PUESTOSonDrag } onMouseLeave={ props.PUESTOSonMouseLeave } onMouseUp={ props.PUESTOSonMouseUp } onMouseDown={ props.PUESTOSonMouseDown } onKeyDown={ props.PUESTOSonKeyDown } onChange={ props.PUESTOSonChange } ondelay={ props.PUESTOSondelay } >{props.PUESTOS0 || `PUESTOS`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} classNames={transaction['frameonefive']?.animationClass || {}}>

          <div id="id_oneonezero_threefive" className={` frame frameonefive ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonefiveStyle , transitionDuration: transaction['frameonefive']?.duration, transitionTimingFunction: transaction['frameonefive']?.timingFunction } } onClick={ props.FrameonefiveonClick } onMouseEnter={ props.FrameonefiveonMouseEnter } onMouseOver={ props.FrameonefiveonMouseOver } onKeyPress={ props.FrameonefiveonKeyPress } onDrag={ props.FrameonefiveonDrag } onMouseLeave={ props.FrameonefiveonMouseLeave } onMouseUp={ props.FrameonefiveonMouseUp } onMouseDown={ props.FrameonefiveonMouseDown } onKeyDown={ props.FrameonefiveonKeyDown } onChange={ props.FrameonefiveonChange } ondelay={ props.Frameonefiveondelay }>
            <CSSTransition in={_in} appear={true} classNames={transaction['formulariodeperdonal']?.animationClass || {}}>

              <span id="id_oneonezero_threesix"  className={` text formulariodeperdonal    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.FORMULARIODEPERDONALStyle , transitionDuration: transaction['formulariodeperdonal']?.duration, transitionTimingFunction: transaction['formulariodeperdonal']?.timingFunction }} onClick={ props.FORMULARIODEPERDONALonClick } onMouseEnter={ props.FORMULARIODEPERDONALonMouseEnter } onMouseOver={ props.FORMULARIODEPERDONALonMouseOver } onKeyPress={ props.FORMULARIODEPERDONALonKeyPress } onDrag={ props.FORMULARIODEPERDONALonDrag } onMouseLeave={ props.FORMULARIODEPERDONALonMouseLeave } onMouseUp={ props.FORMULARIODEPERDONALonMouseUp } onMouseDown={ props.FORMULARIODEPERDONALonMouseDown } onKeyDown={ props.FORMULARIODEPERDONALonKeyDown } onChange={ props.FORMULARIODEPERDONALonChange } ondelay={ props.FORMULARIODEPERDONALondelay } >{props.FORMULARIODEPERDONAL0 || `FORMULARIO DE PERDONAL`}</span>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} classNames={transaction['frameoneseven']?.animationClass || {}}>

          <div id="id_oneonezero_onezeronigth" className={` frame frameoneseven ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonesevenStyle , transitionDuration: transaction['frameoneseven']?.duration, transitionTimingFunction: transaction['frameoneseven']?.timingFunction } } onClick={ props.FrameonesevenonClick } onMouseEnter={ props.FrameonesevenonMouseEnter } onMouseOver={ props.FrameonesevenonMouseOver } onKeyPress={ props.FrameonesevenonKeyPress } onDrag={ props.FrameonesevenonDrag } onMouseLeave={ props.FrameonesevenonMouseLeave } onMouseUp={ props.FrameonesevenonMouseUp } onMouseDown={ props.FrameonesevenonMouseDown } onKeyDown={ props.FrameonesevenonKeyDown } onChange={ props.FrameonesevenonChange } ondelay={ props.Frameonesevenondelay }>
            <CSSTransition in={_in} appear={true} classNames={transaction['colegiojaponesdemorelos']?.animationClass || {}}>

              <span id="id_oneonezero_oneonezero"  className={` text colegiojaponesdemorelos    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.COLEGIOJAPONESDEMORELOSStyle , transitionDuration: transaction['colegiojaponesdemorelos']?.duration, transitionTimingFunction: transaction['colegiojaponesdemorelos']?.timingFunction }} onClick={ props.COLEGIOJAPONESDEMORELOSonClick } onMouseEnter={ props.COLEGIOJAPONESDEMORELOSonMouseEnter } onMouseOver={ props.COLEGIOJAPONESDEMORELOSonMouseOver } onKeyPress={ props.COLEGIOJAPONESDEMORELOSonKeyPress } onDrag={ props.COLEGIOJAPONESDEMORELOSonDrag } onMouseLeave={ props.COLEGIOJAPONESDEMORELOSonMouseLeave } onMouseUp={ props.COLEGIOJAPONESDEMORELOSonMouseUp } onMouseDown={ props.COLEGIOJAPONESDEMORELOSonMouseDown } onKeyDown={ props.COLEGIOJAPONESDEMORELOSonKeyDown } onChange={ props.COLEGIOJAPONESDEMORELOSonChange } ondelay={ props.COLEGIOJAPONESDEMORELOSondelay } >{props.COLEGIOJAPONESDEMORELOS0 || `COLEGIO JAPONES DE MORELOS`}</span>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} classNames={transaction['frameoneeight']?.animationClass || {}}>

          <div id="id_oneonezero_oneoneone" className={` frame frameoneeight ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameoneeightStyle , transitionDuration: transaction['frameoneeight']?.duration, transitionTimingFunction: transaction['frameoneeight']?.timingFunction } } onClick={ props.FrameoneeightonClick } onMouseEnter={ props.FrameoneeightonMouseEnter } onMouseOver={ props.FrameoneeightonMouseOver } onKeyPress={ props.FrameoneeightonKeyPress } onDrag={ props.FrameoneeightonDrag } onMouseLeave={ props.FrameoneeightonMouseLeave } onMouseUp={ props.FrameoneeightonMouseUp } onMouseDown={ props.FrameoneeightonMouseDown } onKeyDown={ props.FrameoneeightonKeyDown } onChange={ props.FrameoneeightonChange } ondelay={ props.Frameoneeightondelay }>
            <CSSTransition in={_in} appear={true} classNames={transaction['notojapanesebargainbutton']?.animationClass || {}}>

              <div id="id_oneonezero_oneonetwoo" className={` frame notojapanesebargainbutton ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.NotojapanesebargainbuttonStyle , transitionDuration: transaction['notojapanesebargainbutton']?.duration, transitionTimingFunction: transaction['notojapanesebargainbutton']?.timingFunction } } onClick={ props.NotojapanesebargainbuttononClick } onMouseEnter={ props.NotojapanesebargainbuttononMouseEnter } onMouseOver={ props.NotojapanesebargainbuttononMouseOver } onKeyPress={ props.NotojapanesebargainbuttononKeyPress } onDrag={ props.NotojapanesebargainbuttononDrag } onMouseLeave={ props.NotojapanesebargainbuttononMouseLeave } onMouseUp={ props.NotojapanesebargainbuttononMouseUp } onMouseDown={ props.NotojapanesebargainbuttononMouseDown } onKeyDown={ props.NotojapanesebargainbuttononKeyDown } onChange={ props.NotojapanesebargainbuttononChange } ondelay={ props.Notojapanesebargainbuttonondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_oneonezero_oneonethree" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="84.375" height="84.375">

                    <path d="M42.1875 84.375C65.487 84.375 84.375 65.487 84.375 42.1875C84.375 18.888 65.487 0 42.1875 0C18.888 0 0 18.888 0 42.1875C0 65.487 18.888 84.375 42.1875 84.375Z" />
                  </svg>
                </CSSTransition>
                <CSSTransition in={_in} appear={true} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_oneonezero_oneonefour" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="78.890625" height="78.890625">

                    <path d="M39.4453 78.8906C61.2304 78.8906 78.8906 61.2304 78.8906 39.4453C78.8906 17.6603 61.2304 0 39.4453 0C17.6603 0 0 17.6603 0 39.4453C0 61.2304 17.6603 78.8906 39.4453 78.8906Z" />
                  </svg>
                </CSSTransition>
                <CSSTransition in={_in} appear={true} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_oneonezero_oneonefive" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="27.075927734375" height="23.756103515625">

                    <path d="M3.52479 10.6523C6.68885 5.66016 13.4388 1.51172 20.4701 0.246093C22.2279 -0.0351565 23.9857 -0.175781 25.4623 0.386719C26.5873 0.808594 27.5014 1.86328 26.8685 3.05859C26.3764 4.04297 25.0404 4.46484 23.9857 4.81641C17.3899 6.9929 11.7023 11.2957 7.81385 17.0508C6.4076 19.1602 4.29822 24.9961 1.69666 23.5195C-1.04553 21.9023 -0.483026 16.8398 3.52479 10.6523L3.52479 10.6523Z" />
                  </svg>
                </CSSTransition>
                <CSSTransition in={_in} appear={true} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_oneonezero_oneonesix" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="57.9921875" height="57.3336181640625">

                    <path d="M19.3198 16.9039C18.0541 18.8727 16.6479 20.9118 15.1713 22.8805C14.9745 23.1196 14.876 23.4149 14.8901 23.7243L14.8901 55.9274C14.8901 56.7008 14.2573 57.3336 13.4838 57.3336L10.601 57.3336C9.82758 57.3336 9.19477 56.7008 9.19477 55.9274L9.19477 33.2164C9.19202 33.0318 9.15292 32.8494 9.0797 32.6799C9.00647 32.5103 8.90057 32.3568 8.76802 32.2282C8.63548 32.0996 8.47889 31.9983 8.30721 31.9302C8.13552 31.8621 7.95211 31.8285 7.76743 31.8313C7.40883 31.8383 7.05727 31.9789 6.80415 32.2321C5.89008 33.1461 4.90571 33.9196 3.99165 34.7633C3.84998 34.8821 3.68628 34.9718 3.50991 35.0273C3.33354 35.0828 3.14796 35.1029 2.96379 35.0866C2.77962 35.0703 2.60048 35.0178 2.43661 34.9322C2.27275 34.8465 2.12738 34.7294 2.00883 34.5875C1.93149 34.4891 1.86118 34.3836 1.81196 34.2711C1.31977 33.2868 0.757272 32.0914 0.194772 31.1071C-0.163822 30.4953 -0.0161657 29.7149 0.546334 29.2789C5.95894 25.1628 10.621 20.1439 14.3276 14.443C14.6932 13.8453 15.4385 13.6063 16.0854 13.8805L18.6166 14.9352C19.3127 15.1672 19.6854 15.9196 19.4534 16.6157C19.4182 16.7211 19.376 16.8125 19.3198 16.9039ZM18.1948 3.40394C14.3658 8.2768 9.86837 12.5849 4.8354 16.2008C4.20962 16.6578 3.33071 16.5172 2.87368 15.8914C2.84555 15.8563 2.82446 15.8141 2.79633 15.7789C2.37446 15.0055 1.81196 14.1618 1.31977 13.3883C0.897897 12.7414 1.05258 11.8766 1.67133 11.4196C5.89008 8.46644 10.3901 4.318 12.9916 0.591438C13.3432 0.0640947 14.0182 -0.139812 14.6088 0.0992503L17.4916 1.22425C18.2299 1.45628 18.6448 2.25081 18.4057 2.98909C18.3565 3.13675 18.2862 3.27738 18.1948 3.40394ZM56.5854 40.5993L50.3979 40.5993C49.6245 40.5993 48.9916 41.2321 48.9916 42.0055L48.9916 51.2868C48.9916 54.1696 48.4291 55.6461 46.3198 56.4196C44.4213 57.2633 41.7495 57.3336 37.812 57.3336C37.2002 57.3336 36.6588 56.9328 36.476 56.3493C36.2651 55.6461 35.9838 54.8024 35.7026 54.0289C35.4284 53.3047 35.801 52.4891 36.5252 52.2219C36.687 52.1586 36.8627 52.1305 37.0385 52.1305C39.2885 52.2008 41.187 52.2008 41.8901 52.1305C42.8041 52.0602 43.0854 51.8493 43.0854 51.1461L43.0854 42.0055C43.0854 41.2321 42.4526 40.5993 41.6791 40.5993L20.2338 40.5993C19.4604 40.5993 18.8276 39.9664 18.8276 39.193L18.8276 36.943C18.8276 36.1696 19.4604 35.5367 20.2338 35.5367L41.6791 35.5367C42.4526 35.5367 43.0854 34.9039 43.0854 34.1305L43.0854 32.3024C43.0854 31.5289 42.4526 30.8961 41.6791 30.8961L21.7807 30.8961C21.0073 30.8961 20.3745 30.2633 20.3745 29.4899L20.3745 27.4508C20.3745 26.6774 21.0073 26.0446 21.7807 26.0446L55.3901 26.0446C56.1635 26.0446 56.7963 26.6774 56.7963 27.4508L56.7963 29.4899C56.7963 30.2633 56.1635 30.8961 55.3901 30.8961L50.3979 30.8961C49.6245 30.8961 48.9916 31.5289 48.9916 32.3024L48.9916 34.1305C48.9916 34.9039 49.6245 35.5367 50.3979 35.5367L56.5854 35.5367C57.3588 35.5367 57.9916 36.1696 57.9916 36.943L57.9916 39.193C58.0127 39.9453 57.4221 40.5782 56.6698 40.5993L56.5854 40.5993ZM28.6713 41.7946C30.8581 43.8125 32.876 45.9993 34.7182 48.3336C35.1893 48.9243 35.0909 49.7821 34.5073 50.2532C34.4868 50.2734 34.463 50.2901 34.437 50.3024L32.3276 51.7789C31.6948 52.2289 30.8159 52.0742 30.3659 51.4414C30.3659 51.4344 30.3588 51.4344 30.3588 51.4274C28.6576 49.1243 26.8018 46.9396 24.8041 44.8883C24.2487 44.3117 24.2698 43.4047 24.8463 42.8493C24.9026 42.8 24.9588 42.7508 25.0151 42.7086L26.8432 41.5133C27.4479 41.211 28.1791 41.3235 28.6713 41.7946ZM52.8588 22.5289L25.2963 22.5289C24.5229 22.5289 23.8901 21.8961 23.8901 21.1227L23.8901 2.84144C23.8901 2.068 24.5229 1.43519 25.2963 1.43519L52.8588 1.43519C53.6323 1.43519 54.2651 2.068 54.2651 2.84144L54.2651 21.1227C54.2651 21.4956 54.1169 21.8533 53.8532 22.1171C53.5895 22.3808 53.2318 22.5289 52.8588 22.5289ZM46.9526 5.72425L30.9916 5.72425C30.2182 5.72425 29.5854 6.35706 29.5854 7.1305L29.5854 8.53675C29.5854 9.31019 30.2182 9.943 30.9916 9.943L46.9526 9.943C47.726 9.943 48.3588 9.31019 48.3588 8.53675L48.3588 7.1305C48.3588 6.35706 47.726 5.72425 46.9526 5.72425ZM46.9526 13.9508L30.9916 13.9508C30.2182 13.9508 29.5854 14.5836 29.5854 15.3571L29.5854 16.7633C29.5854 17.5368 30.2182 18.1696 30.9916 18.1696L46.9526 18.1696C47.726 18.1696 48.3588 17.5368 48.3588 16.7633L48.3588 15.3571C48.3588 14.5836 47.726 13.9508 46.9526 13.9508Z" />
                  </svg>
                </CSSTransition>
                <CSSTransition in={_in} appear={true} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_oneonezero_oneoneseven" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="27.075927734375" height="23.756103515625">

                    <path d="M3.52479 10.6523C6.68885 5.66016 13.4388 1.51172 20.4701 0.246093C22.2279 -0.0351565 23.9857 -0.175781 25.4623 0.386719C26.5873 0.808594 27.5014 1.86328 26.8685 3.05859C26.3764 4.04297 25.0404 4.46484 23.9857 4.81641C17.3899 6.9929 11.7023 11.2957 7.81385 17.0508C6.4076 19.1602 4.29822 24.9961 1.69666 23.5195C-1.04553 21.9023 -0.483026 16.8398 3.52479 10.6523L3.52479 10.6523Z" />
                  </svg>
                </CSSTransition>
              </div>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} classNames={transaction['frametwoozero']?.animationClass || {}}>

          <div id="id_oneonezero_onetwooeight" className={` frame frametwoozero ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrametwoozeroStyle , transitionDuration: transaction['frametwoozero']?.duration, transitionTimingFunction: transaction['frametwoozero']?.timingFunction } } onClick={ props.FrametwoozeroonClick } onMouseEnter={ props.FrametwoozeroonMouseEnter } onMouseOver={ props.FrametwoozeroonMouseOver } onKeyPress={ props.FrametwoozeroonKeyPress } onDrag={ props.FrametwoozeroonDrag } onMouseLeave={ props.FrametwoozeroonMouseLeave } onMouseUp={ props.FrametwoozeroonMouseUp } onMouseDown={ props.FrametwoozeroonMouseDown } onKeyDown={ props.FrametwoozeroonKeyDown } onChange={ props.FrametwoozeroonChange } ondelay={ props.Frametwoozeroondelay }>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameoneseven']?.animationClass || {}}>

              <div id="id_oneonezero_onetwoonigth" className={` frame frameoneseven ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonesevenStyle , transitionDuration: transaction['frameoneseven']?.duration, transitionTimingFunction: transaction['frameoneseven']?.timingFunction } } onClick={ props.FrameonesevenonClick } onMouseEnter={ props.FrameonesevenonMouseEnter } onMouseOver={ props.FrameonesevenonMouseOver } onKeyPress={ props.FrameonesevenonKeyPress } onDrag={ props.FrameonesevenonDrag } onMouseLeave={ props.FrameonesevenonMouseLeave } onMouseUp={ props.FrameonesevenonMouseUp } onMouseDown={ props.FrameonesevenonMouseDown } onKeyDown={ props.FrameonesevenonKeyDown } onChange={ props.FrameonesevenonChange } ondelay={ props.Frameonesevenondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['apellido']?.animationClass || {}}>

                  <span id="id_oneonezero_onethreezero"  className={` text apellido    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.APELLIDOStyle , transitionDuration: transaction['apellido']?.duration, transitionTimingFunction: transaction['apellido']?.timingFunction }} onClick={ props.APELLIDOonClick } onMouseEnter={ props.APELLIDOonMouseEnter } onMouseOver={ props.APELLIDOonMouseOver } onKeyPress={ props.APELLIDOonKeyPress } onDrag={ props.APELLIDOonDrag } onMouseLeave={ props.APELLIDOonMouseLeave } onMouseUp={ props.APELLIDOonMouseUp } onMouseDown={ props.APELLIDOonMouseDown } onKeyDown={ props.APELLIDOonKeyDown } onChange={ props.APELLIDOonChange } ondelay={ props.APELLIDOondelay } >{props.APELLIDO0 || `APELLIDO`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameoneeight']?.animationClass || {}}>

              <div id="id_oneonezero_onethreeone" className={` frame frameoneeight ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameoneeightStyle , transitionDuration: transaction['frameoneeight']?.duration, transitionTimingFunction: transaction['frameoneeight']?.timingFunction } } onClick={ props.FrameoneeightonClick } onMouseEnter={ props.FrameoneeightonMouseEnter } onMouseOver={ props.FrameoneeightonMouseOver } onKeyPress={ props.FrameoneeightonKeyPress } onDrag={ props.FrameoneeightonDrag } onMouseLeave={ props.FrameoneeightonMouseLeave } onMouseUp={ props.FrameoneeightonMouseUp } onMouseDown={ props.FrameoneeightonMouseDown } onKeyDown={ props.FrameoneeightonKeyDown } onChange={ props.FrameoneeightonChange } ondelay={ props.Frameoneeightondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['sexo']?.animationClass || {}}>

                  <span id="id_oneonezero_onethreetwoo"  className={` text sexo    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.SEXOStyle , transitionDuration: transaction['sexo']?.duration, transitionTimingFunction: transaction['sexo']?.timingFunction }} onClick={ props.SEXOonClick } onMouseEnter={ props.SEXOonMouseEnter } onMouseOver={ props.SEXOonMouseOver } onKeyPress={ props.SEXOonKeyPress } onDrag={ props.SEXOonDrag } onMouseLeave={ props.SEXOonMouseLeave } onMouseUp={ props.SEXOonMouseUp } onMouseDown={ props.SEXOonMouseDown } onKeyDown={ props.SEXOonKeyDown } onChange={ props.SEXOonChange } ondelay={ props.SEXOondelay } >{props.SEXO0 || `SEXO`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameonenigth']?.animationClass || {}}>

              <div id="id_oneonezero_onethreefive" className={` frame frameonenigth ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonenigthStyle , transitionDuration: transaction['frameonenigth']?.duration, transitionTimingFunction: transaction['frameonenigth']?.timingFunction } } onClick={ props.FrameonenigthonClick } onMouseEnter={ props.FrameonenigthonMouseEnter } onMouseOver={ props.FrameonenigthonMouseOver } onKeyPress={ props.FrameonenigthonKeyPress } onDrag={ props.FrameonenigthonDrag } onMouseLeave={ props.FrameonenigthonMouseLeave } onMouseUp={ props.FrameonenigthonMouseUp } onMouseDown={ props.FrameonenigthonMouseDown } onKeyDown={ props.FrameonenigthonKeyDown } onChange={ props.FrameonenigthonChange } ondelay={ props.Frameonenigthondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['puesto']?.animationClass || {}}>

                  <span id="id_oneonezero_onethreesix"  className={` text puesto    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.PUESTOStyle , transitionDuration: transaction['puesto']?.duration, transitionTimingFunction: transaction['puesto']?.timingFunction }} onClick={ props.PUESTOonClick } onMouseEnter={ props.PUESTOonMouseEnter } onMouseOver={ props.PUESTOonMouseOver } onKeyPress={ props.PUESTOonKeyPress } onDrag={ props.PUESTOonDrag } onMouseLeave={ props.PUESTOonMouseLeave } onMouseUp={ props.PUESTOonMouseUp } onMouseDown={ props.PUESTOonMouseDown } onKeyDown={ props.PUESTOonKeyDown } onChange={ props.PUESTOonChange } ondelay={ props.PUESTOondelay } >{props.PUESTO0 || `PUESTO`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameoneseven']?.animationClass || {}}>

              <div id="id_oneonezero_onethreeseven" className={` frame frameoneseven ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonesevenStyle , transitionDuration: transaction['frameoneseven']?.duration, transitionTimingFunction: transaction['frameoneseven']?.timingFunction } } onClick={ props.FrameonesevenonClick } onMouseEnter={ props.FrameonesevenonMouseEnter } onMouseOver={ props.FrameonesevenonMouseOver } onKeyPress={ props.FrameonesevenonKeyPress } onDrag={ props.FrameonesevenonDrag } onMouseLeave={ props.FrameonesevenonMouseLeave } onMouseUp={ props.FrameonesevenonMouseUp } onMouseDown={ props.FrameonesevenonMouseDown } onKeyDown={ props.FrameonesevenonKeyDown } onChange={ props.FrameonesevenonChange } ondelay={ props.Frameonesevenondelay }>

              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frametwooone']?.animationClass || {}}>

              <div id="id_oneonezero_onethreeeight" className={` frame frametwooone ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrametwoooneStyle , transitionDuration: transaction['frametwooone']?.duration, transitionTimingFunction: transaction['frametwooone']?.timingFunction } } onClick={ props.FrametwoooneonClick } onMouseEnter={ props.FrametwoooneonMouseEnter } onMouseOver={ props.FrametwoooneonMouseOver } onKeyPress={ props.FrametwoooneonKeyPress } onDrag={ props.FrametwoooneonDrag } onMouseLeave={ props.FrametwoooneonMouseLeave } onMouseUp={ props.FrametwoooneonMouseUp } onMouseDown={ props.FrametwoooneonMouseDown } onKeyDown={ props.FrametwoooneonKeyDown } onChange={ props.FrametwoooneonChange } ondelay={ props.Frametwoooneondelay }>

              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frametwootwoo']?.animationClass || {}}>

              <div id="id_oneonezero_onethreenigth" className={` frame frametwootwoo ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrametwootwooStyle , transitionDuration: transaction['frametwootwoo']?.duration, transitionTimingFunction: transaction['frametwootwoo']?.timingFunction } } onClick={ props.FrametwootwooonClick } onMouseEnter={ props.FrametwootwooonMouseEnter } onMouseOver={ props.FrametwootwooonMouseOver } onKeyPress={ props.FrametwootwooonKeyPress } onDrag={ props.FrametwootwooonDrag } onMouseLeave={ props.FrametwootwooonMouseLeave } onMouseUp={ props.FrametwootwooonMouseUp } onMouseDown={ props.FrametwootwooonMouseDown } onKeyDown={ props.FrametwootwooonKeyDown } onChange={ props.FrametwootwooonChange } ondelay={ props.Frametwootwooondelay }>

              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frametwoothree']?.animationClass || {}}>

              <div id="id_oneonezero_onefourzero" className={` frame frametwoothree ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrametwoothreeStyle , transitionDuration: transaction['frametwoothree']?.duration, transitionTimingFunction: transaction['frametwoothree']?.timingFunction } } onClick={ props.FrametwoothreeonClick } onMouseEnter={ props.FrametwoothreeonMouseEnter } onMouseOver={ props.FrametwoothreeonMouseOver } onKeyPress={ props.FrametwoothreeonKeyPress } onDrag={ props.FrametwoothreeonDrag } onMouseLeave={ props.FrametwoothreeonMouseLeave } onMouseUp={ props.FrametwoothreeonMouseUp } onMouseDown={ props.FrametwoothreeonMouseDown } onKeyDown={ props.FrametwoothreeonKeyDown } onChange={ props.FrametwoothreeonChange } ondelay={ props.Frametwoothreeondelay }>

              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frametwoofour']?.animationClass || {}}>

              <div id="id_oneonezero_onefourone" className={` frame frametwoofour ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrametwoofourStyle , transitionDuration: transaction['frametwoofour']?.duration, transitionTimingFunction: transaction['frametwoofour']?.timingFunction } } onClick={ props.FrametwoofouronClick } onMouseEnter={ props.FrametwoofouronMouseEnter } onMouseOver={ props.FrametwoofouronMouseOver } onKeyPress={ props.FrametwoofouronKeyPress } onDrag={ props.FrametwoofouronDrag } onMouseLeave={ props.FrametwoofouronMouseLeave } onMouseUp={ props.FrametwoofouronMouseUp } onMouseDown={ props.FrametwoofouronMouseDown } onKeyDown={ props.FrametwoofouronKeyDown } onChange={ props.FrametwoofouronChange } ondelay={ props.Frametwoofourondelay }>

              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frametwoosix']?.animationClass || {}}>

              <div id="id_oneonezero_onefourtwoo" className={` frame frametwoosix cursor ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrametwoosixStyle , transitionDuration: transaction['frametwoosix']?.duration, transitionTimingFunction: transaction['frametwoosix']?.timingFunction } } onClick={ props.FrametwoosixonClick || function(e){ SingletoneNavigation.setTransitionInstance(null, 'Desktopseven' ); history.push('/6'); }} onMouseEnter={ props.FrametwoosixonMouseEnter } onMouseOver={ props.FrametwoosixonMouseOver } onKeyPress={ props.FrametwoosixonKeyPress } onDrag={ props.FrametwoosixonDrag } onMouseLeave={ props.FrametwoosixonMouseLeave } onMouseUp={ props.FrametwoosixonMouseUp } onMouseDown={ props.FrametwoosixonMouseDown } onKeyDown={ props.FrametwoosixonKeyDown } onChange={ props.FrametwoosixonChange } ondelay={ props.Frametwoosixondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['pngtransparentmagnifyingglassmagnificationmagnifyingglassglasslogolightone']?.animationClass || {}}>
                  <img id="id_oneonezero_onefourthree" className={` rectangle pngtransparentmagnifyingglassmagnificationmagnifyingglassglasslogolightone ${ props.onClick ? 'cursor' : '' } `} style={{ ...{},  ...props.PngtransparentmagnifyingglassmagnificationmagnifyingglassglasslogolightoneStyle , transitionDuration: transaction['pngtransparentmagnifyingglassmagnificationmagnifyingglassglasslogolightone']?.duration, transitionTimingFunction: transaction['pngtransparentmagnifyingglassmagnificationmagnifyingglassglasslogolightone']?.timingFunction }} onClick={ props.PngtransparentmagnifyingglassmagnificationmagnifyingglassglasslogolightoneonClick } onMouseEnter={ props.PngtransparentmagnifyingglassmagnificationmagnifyingglassglasslogolightoneonMouseEnter } onMouseOver={ props.PngtransparentmagnifyingglassmagnificationmagnifyingglassglasslogolightoneonMouseOver } onKeyPress={ props.PngtransparentmagnifyingglassmagnificationmagnifyingglassglasslogolightoneonKeyPress } onDrag={ props.PngtransparentmagnifyingglassmagnificationmagnifyingglassglasslogolightoneonDrag } onMouseLeave={ props.PngtransparentmagnifyingglassmagnificationmagnifyingglassglasslogolightoneonMouseLeave } onMouseUp={ props.PngtransparentmagnifyingglassmagnificationmagnifyingglassglasslogolightoneonMouseUp } onMouseDown={ props.PngtransparentmagnifyingglassmagnificationmagnifyingglassglasslogolightoneonMouseDown } onKeyDown={ props.PngtransparentmagnifyingglassmagnificationmagnifyingglassglasslogolightoneonKeyDown } onChange={ props.PngtransparentmagnifyingglassmagnificationmagnifyingglassglasslogolightoneonChange } ondelay={ props.Pngtransparentmagnifyingglassmagnificationmagnifyingglassglasslogolightoneondelay } src={props.Pngtransparentmagnifyingglassmagnificationmagnifyingglassglasslogolightone0 || "https://cdn.uncodie.com/6u2XSDo7Ms6u8T8UBz7LVQ/e98f7f0e4bef2af6a57c7e3313eaa6fa1d2a7f46.png" } />
                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameeight']?.animationClass || {}}>

              <div id="id_oneonezero_onefourfour" className={` frame frameeight cursor ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameeightStyle , transitionDuration: transaction['frameeight']?.duration, transitionTimingFunction: transaction['frameeight']?.timingFunction } } onClick={ props.FrameeightonClick || function(e){ SingletoneNavigation.setTransitionInstance(null, 'Desktoptwoo' ); history.push('/1'); }} onMouseEnter={ props.FrameeightonMouseEnter } onMouseOver={ props.FrameeightonMouseOver } onKeyPress={ props.FrameeightonKeyPress } onDrag={ props.FrameeightonDrag } onMouseLeave={ props.FrameeightonMouseLeave } onMouseUp={ props.FrameeightonMouseUp } onMouseDown={ props.FrameeightonMouseDown } onKeyDown={ props.FrameeightonKeyDown } onChange={ props.FrameeightonChange } ondelay={ props.Frameeightondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['agregar']?.animationClass || {}}>

                  <span id="id_oneonezero_onefourfive"  className={` text agregar    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.AGREGARStyle , transitionDuration: transaction['agregar']?.duration, transitionTimingFunction: transaction['agregar']?.timingFunction }} onClick={ props.AGREGARonClick } onMouseEnter={ props.AGREGARonMouseEnter } onMouseOver={ props.AGREGARonMouseOver } onKeyPress={ props.AGREGARonKeyPress } onDrag={ props.AGREGARonDrag } onMouseLeave={ props.AGREGARonMouseLeave } onMouseUp={ props.AGREGARonMouseUp } onMouseDown={ props.AGREGARonMouseDown } onKeyDown={ props.AGREGARonKeyDown } onChange={ props.AGREGARonChange } ondelay={ props.AGREGARondelay } >{props.AGREGAR0 || `AGREGAR`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['framethreeseven']?.animationClass || {}}>

              <div id="id_oneonezero_threefivesix" className={` frame framethreeseven cursor ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FramethreesevenStyle , transitionDuration: transaction['framethreeseven']?.duration, transitionTimingFunction: transaction['framethreeseven']?.timingFunction } } onClick={ props.FramethreesevenonClick || function(e){ SingletoneNavigation.setTransitionInstance(null, 'Desktoptwoo' ); history.push('/1'); }} onMouseEnter={ props.FramethreesevenonMouseEnter } onMouseOver={ props.FramethreesevenonMouseOver } onKeyPress={ props.FramethreesevenonKeyPress } onDrag={ props.FramethreesevenonDrag } onMouseLeave={ props.FramethreesevenonMouseLeave } onMouseUp={ props.FramethreesevenonMouseUp } onMouseDown={ props.FramethreesevenonMouseDown } onKeyDown={ props.FramethreesevenonKeyDown } onChange={ props.FramethreesevenonChange } ondelay={ props.Framethreesevenondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['escanear']?.animationClass || {}}>

                  <span id="id_oneonezero_threefiveseven"  className={` text escanear    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.ESCANEARStyle , transitionDuration: transaction['escanear']?.duration, transitionTimingFunction: transaction['escanear']?.timingFunction }} onClick={ props.ESCANEARonClick } onMouseEnter={ props.ESCANEARonMouseEnter } onMouseOver={ props.ESCANEARonMouseOver } onKeyPress={ props.ESCANEARonKeyPress } onDrag={ props.ESCANEARonDrag } onMouseLeave={ props.ESCANEARonMouseLeave } onMouseUp={ props.ESCANEARonMouseUp } onMouseDown={ props.ESCANEARonMouseDown } onKeyDown={ props.ESCANEARonKeyDown } onChange={ props.ESCANEARonChange } ondelay={ props.ESCANEARondelay } >{props.ESCANEAR0 || `ESCANEAR`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frametwoofive']?.animationClass || {}}>

              <div id="id_oneonezero_onefoursix" className={` frame frametwoofive ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrametwoofiveStyle , transitionDuration: transaction['frametwoofive']?.duration, transitionTimingFunction: transaction['frametwoofive']?.timingFunction } } onClick={ props.FrametwoofiveonClick } onMouseEnter={ props.FrametwoofiveonMouseEnter } onMouseOver={ props.FrametwoofiveonMouseOver } onKeyPress={ props.FrametwoofiveonKeyPress } onDrag={ props.FrametwoofiveonDrag } onMouseLeave={ props.FrametwoofiveonMouseLeave } onMouseUp={ props.FrametwoofiveonMouseUp } onMouseDown={ props.FrametwoofiveonMouseDown } onKeyDown={ props.FrametwoofiveonKeyDown } onChange={ props.FrametwoofiveonChange } ondelay={ props.Frametwoofiveondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['onetwooonefourfivenigthfourone']?.animationClass || {}}>
                  <img id="id_oneonezero_onefourseven" className={` rectangle onetwooonefourfivenigthfourone ${ props.onClick ? 'cursor' : '' } `} style={{ ...{},  ...props.onetwooonefourfivenigthfouroneStyle , transitionDuration: transaction['onetwooonefourfivenigthfourone']?.duration, transitionTimingFunction: transaction['onetwooonefourfivenigthfourone']?.timingFunction }} onClick={ props.OnetwooonefourfivenigthfouroneonClick } onMouseEnter={ props.OnetwooonefourfivenigthfouroneonMouseEnter } onMouseOver={ props.OnetwooonefourfivenigthfouroneonMouseOver } onKeyPress={ props.OnetwooonefourfivenigthfouroneonKeyPress } onDrag={ props.OnetwooonefourfivenigthfouroneonDrag } onMouseLeave={ props.OnetwooonefourfivenigthfouroneonMouseLeave } onMouseUp={ props.OnetwooonefourfivenigthfouroneonMouseUp } onMouseDown={ props.OnetwooonefourfivenigthfouroneonMouseDown } onKeyDown={ props.OnetwooonefourfivenigthfouroneonKeyDown } onChange={ props.OnetwooonefourfivenigthfouroneonChange } ondelay={ props.Onetwooonefourfivenigthfouroneondelay } src={props.Onetwooonefourfivenigthfourone0 || "https://cdn.uncodie.com/6u2XSDo7Ms6u8T8UBz7LVQ/12457674480f3e27b5ad555034d20ed7813a9c95.png" } />
                </CSSTransition>
                <CSSTransition in={_in} appear={true} classNames={transaction['pngtransparentwritemodifytooleditpendocumentmultimediasolidpxiconthumbnailone']?.animationClass || {}}>
                  <img id="id_oneonezero_onefoureight" className={` rectangle pngtransparentwritemodifytooleditpendocumentmultimediasolidpxiconthumbnailone ${ props.onClick ? 'cursor' : '' } `} style={{ ...{},  ...props.PngtransparentwritemodifytooleditpendocumentmultimediasolidpxiconthumbnailoneStyle , transitionDuration: transaction['pngtransparentwritemodifytooleditpendocumentmultimediasolidpxiconthumbnailone']?.duration, transitionTimingFunction: transaction['pngtransparentwritemodifytooleditpendocumentmultimediasolidpxiconthumbnailone']?.timingFunction }} onClick={ props.PngtransparentwritemodifytooleditpendocumentmultimediasolidpxiconthumbnailoneonClick } onMouseEnter={ props.PngtransparentwritemodifytooleditpendocumentmultimediasolidpxiconthumbnailoneonMouseEnter } onMouseOver={ props.PngtransparentwritemodifytooleditpendocumentmultimediasolidpxiconthumbnailoneonMouseOver } onKeyPress={ props.PngtransparentwritemodifytooleditpendocumentmultimediasolidpxiconthumbnailoneonKeyPress } onDrag={ props.PngtransparentwritemodifytooleditpendocumentmultimediasolidpxiconthumbnailoneonDrag } onMouseLeave={ props.PngtransparentwritemodifytooleditpendocumentmultimediasolidpxiconthumbnailoneonMouseLeave } onMouseUp={ props.PngtransparentwritemodifytooleditpendocumentmultimediasolidpxiconthumbnailoneonMouseUp } onMouseDown={ props.PngtransparentwritemodifytooleditpendocumentmultimediasolidpxiconthumbnailoneonMouseDown } onKeyDown={ props.PngtransparentwritemodifytooleditpendocumentmultimediasolidpxiconthumbnailoneonKeyDown } onChange={ props.PngtransparentwritemodifytooleditpendocumentmultimediasolidpxiconthumbnailoneonChange } ondelay={ props.Pngtransparentwritemodifytooleditpendocumentmultimediasolidpxiconthumbnailoneondelay } src={props.Pngtransparentwritemodifytooleditpendocumentmultimediasolidpxiconthumbnailone0 || "https://cdn.uncodie.com/6u2XSDo7Ms6u8T8UBz7LVQ/706959636ac7a140b85a6604166002e6f72be3a6.png" } />
                </CSSTransition>
                <CSSTransition in={_in} appear={true} classNames={transaction['nombreapellidopuestosexotelefonoeditareliminar']?.animationClass || {}}>

                  <span id="id_oneonezero_onefournigth"  className={` text nombreapellidopuestosexotelefonoeditareliminar    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.NOMBREAPELLIDOPUESTOSEXOTELEFONOEDITARELIMINARStyle , transitionDuration: transaction['nombreapellidopuestosexotelefonoeditareliminar']?.duration, transitionTimingFunction: transaction['nombreapellidopuestosexotelefonoeditareliminar']?.timingFunction }} onClick={ props.NOMBREAPELLIDOPUESTOSEXOTELEFONOEDITARELIMINARonClick } onMouseEnter={ props.NOMBREAPELLIDOPUESTOSEXOTELEFONOEDITARELIMINARonMouseEnter } onMouseOver={ props.NOMBREAPELLIDOPUESTOSEXOTELEFONOEDITARELIMINARonMouseOver } onKeyPress={ props.NOMBREAPELLIDOPUESTOSEXOTELEFONOEDITARELIMINARonKeyPress } onDrag={ props.NOMBREAPELLIDOPUESTOSEXOTELEFONOEDITARELIMINARonDrag } onMouseLeave={ props.NOMBREAPELLIDOPUESTOSEXOTELEFONOEDITARELIMINARonMouseLeave } onMouseUp={ props.NOMBREAPELLIDOPUESTOSEXOTELEFONOEDITARELIMINARonMouseUp } onMouseDown={ props.NOMBREAPELLIDOPUESTOSEXOTELEFONOEDITARELIMINARonMouseDown } onKeyDown={ props.NOMBREAPELLIDOPUESTOSEXOTELEFONOEDITARELIMINARonKeyDown } onChange={ props.NOMBREAPELLIDOPUESTOSEXOTELEFONOEDITARELIMINARonChange } ondelay={ props.NOMBREAPELLIDOPUESTOSEXOTELEFONOEDITARELIMINARondelay } >{props.NOMBREAPELLIDOPUESTOSEXOTELEFONOEDITARELIMINAR0 || `NOMBRE     APELLIDO      PUESTO      SEXO      TELEFONO      EDITAR       ELIMINAR`}</span>

                </CSSTransition>
                <CSSTransition in={_in} appear={true} classNames={transaction['fulanitoveradocentemseventhreetwoosixthreethreetwooseventhreetwoo']?.animationClass || {}}>

                  <span id="id_oneonezero_onefivezero"  className={` text fulanitoveradocentemseventhreetwoosixthreethreetwooseventhreetwoo    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.FULANITOVERADocenteMseventhreetwoosixthreethreetwooseventhreetwooStyle , transitionDuration: transaction['fulanitoveradocentemseventhreetwoosixthreethreetwooseventhreetwoo']?.duration, transitionTimingFunction: transaction['fulanitoveradocentemseventhreetwoosixthreethreetwooseventhreetwoo']?.timingFunction }} onClick={ props.FULANITOVERADocenteMseventhreetwoosixthreethreetwooseventhreetwooonClick } onMouseEnter={ props.FULANITOVERADocenteMseventhreetwoosixthreethreetwooseventhreetwooonMouseEnter } onMouseOver={ props.FULANITOVERADocenteMseventhreetwoosixthreethreetwooseventhreetwooonMouseOver } onKeyPress={ props.FULANITOVERADocenteMseventhreetwoosixthreethreetwooseventhreetwooonKeyPress } onDrag={ props.FULANITOVERADocenteMseventhreetwoosixthreethreetwooseventhreetwooonDrag } onMouseLeave={ props.FULANITOVERADocenteMseventhreetwoosixthreethreetwooseventhreetwooonMouseLeave } onMouseUp={ props.FULANITOVERADocenteMseventhreetwoosixthreethreetwooseventhreetwooonMouseUp } onMouseDown={ props.FULANITOVERADocenteMseventhreetwoosixthreethreetwooseventhreetwooonMouseDown } onKeyDown={ props.FULANITOVERADocenteMseventhreetwoosixthreethreetwooseventhreetwooonKeyDown } onChange={ props.FULANITOVERADocenteMseventhreetwoosixthreethreetwooseventhreetwooonChange } ondelay={ props.FULANITOVERADocenteMseventhreetwoosixthreethreetwooseventhreetwooondelay } >{props.FULANITOVERADocenteMseventhreetwoosixthreethreetwooseventhreetwoo0 || `FULANITO     VERA          Docente      M        7326332732`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['framethreefive']?.animationClass || {}}>

              <div id="id_oneonezero_threefiveone" className={` frame framethreefive ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FramethreefiveStyle , transitionDuration: transaction['framethreefive']?.duration, transitionTimingFunction: transaction['framethreefive']?.timingFunction } } onClick={ props.FramethreefiveonClick } onMouseEnter={ props.FramethreefiveonMouseEnter } onMouseOver={ props.FramethreefiveonMouseOver } onKeyPress={ props.FramethreefiveonKeyPress } onDrag={ props.FramethreefiveonDrag } onMouseLeave={ props.FramethreefiveonMouseLeave } onMouseUp={ props.FramethreefiveonMouseUp } onMouseDown={ props.FramethreefiveonMouseDown } onKeyDown={ props.FramethreefiveonKeyDown } onChange={ props.FramethreefiveonChange } ondelay={ props.Framethreefiveondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['telefono']?.animationClass || {}}>

                  <span id="id_oneonezero_threefivetwoo"  className={` text telefono    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.TELEFONOStyle , transitionDuration: transaction['telefono']?.duration, transitionTimingFunction: transaction['telefono']?.timingFunction }} onClick={ props.TELEFONOonClick } onMouseEnter={ props.TELEFONOonMouseEnter } onMouseOver={ props.TELEFONOonMouseOver } onKeyPress={ props.TELEFONOonKeyPress } onDrag={ props.TELEFONOonDrag } onMouseLeave={ props.TELEFONOonMouseLeave } onMouseUp={ props.TELEFONOonMouseUp } onMouseDown={ props.TELEFONOonMouseDown } onKeyDown={ props.TELEFONOonKeyDown } onChange={ props.TELEFONOonChange } ondelay={ props.TELEFONOondelay } >{props.TELEFONO0 || `TELEFONO`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['framethreesix']?.animationClass || {}}>

              <div id="id_oneonezero_threefivefour" className={` frame framethreesix ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FramethreesixStyle , transitionDuration: transaction['framethreesix']?.duration, transitionTimingFunction: transaction['framethreesix']?.timingFunction } } onClick={ props.FramethreesixonClick } onMouseEnter={ props.FramethreesixonMouseEnter } onMouseOver={ props.FramethreesixonMouseOver } onKeyPress={ props.FramethreesixonKeyPress } onDrag={ props.FramethreesixonDrag } onMouseLeave={ props.FramethreesixonMouseLeave } onMouseUp={ props.FramethreesixonMouseUp } onMouseDown={ props.FramethreesixonMouseDown } onKeyDown={ props.FramethreesixonKeyDown } onChange={ props.FramethreesixonChange } ondelay={ props.Framethreesixondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['huella']?.animationClass || {}}>

                  <span id="id_oneonezero_threefivefive"  className={` text huella    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.HUELLAStyle , transitionDuration: transaction['huella']?.duration, transitionTimingFunction: transaction['huella']?.timingFunction }} onClick={ props.HUELLAonClick } onMouseEnter={ props.HUELLAonMouseEnter } onMouseOver={ props.HUELLAonMouseOver } onKeyPress={ props.HUELLAonKeyPress } onDrag={ props.HUELLAonDrag } onMouseLeave={ props.HUELLAonMouseLeave } onMouseUp={ props.HUELLAonMouseUp } onMouseDown={ props.HUELLAonMouseDown } onKeyDown={ props.HUELLAonKeyDown } onChange={ props.HUELLAonChange } ondelay={ props.HUELLAondelay } >{props.HUELLA0 || `HUELLA`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} classNames={transaction['frametwooone']?.animationClass || {}}>

          <div id="id_oneonezero_onefiveone" className={` frame frametwooone ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrametwoooneStyle , transitionDuration: transaction['frametwooone']?.duration, transitionTimingFunction: transaction['frametwooone']?.timingFunction } } onClick={ props.FrametwoooneonClick } onMouseEnter={ props.FrametwoooneonMouseEnter } onMouseOver={ props.FrametwoooneonMouseOver } onKeyPress={ props.FrametwoooneonKeyPress } onDrag={ props.FrametwoooneonDrag } onMouseLeave={ props.FrametwoooneonMouseLeave } onMouseUp={ props.FrametwoooneonMouseUp } onMouseDown={ props.FrametwoooneonMouseDown } onKeyDown={ props.FrametwoooneonKeyDown } onChange={ props.FrametwoooneonChange } ondelay={ props.Frametwoooneondelay }>
            <CSSTransition in={_in} appear={true} classNames={transaction['nombre']?.animationClass || {}}>

              <span id="id_oneonezero_onefivetwoo"  className={` text nombre    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.NOMBREStyle , transitionDuration: transaction['nombre']?.duration, transitionTimingFunction: transaction['nombre']?.timingFunction }} onClick={ props.NOMBREonClick } onMouseEnter={ props.NOMBREonMouseEnter } onMouseOver={ props.NOMBREonMouseOver } onKeyPress={ props.NOMBREonKeyPress } onDrag={ props.NOMBREonDrag } onMouseLeave={ props.NOMBREonMouseLeave } onMouseUp={ props.NOMBREonMouseUp } onMouseDown={ props.NOMBREonMouseDown } onKeyDown={ props.NOMBREonKeyDown } onChange={ props.NOMBREonChange } ondelay={ props.NOMBREondelay } >{props.NOMBRE0 || `NOMBRE`}</span>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} classNames={transaction['frametwootwoo']?.animationClass || {}}>

          <div id="id_oneonezero_onefivethree" className={` frame frametwootwoo ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrametwootwooStyle , transitionDuration: transaction['frametwootwoo']?.duration, transitionTimingFunction: transaction['frametwootwoo']?.timingFunction } } onClick={ props.FrametwootwooonClick } onMouseEnter={ props.FrametwootwooonMouseEnter } onMouseOver={ props.FrametwootwooonMouseOver } onKeyPress={ props.FrametwootwooonKeyPress } onDrag={ props.FrametwootwooonDrag } onMouseLeave={ props.FrametwootwooonMouseLeave } onMouseUp={ props.FrametwootwooonMouseUp } onMouseDown={ props.FrametwootwooonMouseDown } onKeyDown={ props.FrametwootwooonKeyDown } onChange={ props.FrametwootwooonChange } ondelay={ props.Frametwootwooondelay }>
            <CSSTransition in={_in} appear={true} classNames={transaction['formulariodepersonal']?.animationClass || {}}>

              <span id="id_oneonezero_onefivefour"  className={` text formulariodepersonal    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.FORMULARIODEPERSONALStyle , transitionDuration: transaction['formulariodepersonal']?.duration, transitionTimingFunction: transaction['formulariodepersonal']?.timingFunction }} onClick={ props.FORMULARIODEPERSONALonClick } onMouseEnter={ props.FORMULARIODEPERSONALonMouseEnter } onMouseOver={ props.FORMULARIODEPERSONALonMouseOver } onKeyPress={ props.FORMULARIODEPERSONALonKeyPress } onDrag={ props.FORMULARIODEPERSONALonDrag } onMouseLeave={ props.FORMULARIODEPERSONALonMouseLeave } onMouseUp={ props.FORMULARIODEPERSONALonMouseUp } onMouseDown={ props.FORMULARIODEPERSONALonMouseDown } onKeyDown={ props.FORMULARIODEPERSONALonKeyDown } onChange={ props.FORMULARIODEPERSONALonChange } ondelay={ props.FORMULARIODEPERSONALondelay } >{props.FORMULARIODEPERSONAL0 || `FORMULARIO DE PERSONAL`}</span>

            </CSSTransition>
          </div>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Desktopfour.propTypes = {
    style: PropTypes.any,
REPORTES0: PropTypes.any,
USUARIOS0: PropTypes.any,
SALIR0: PropTypes.any,
MENU0: PropTypes.any,
PERSONAL0: PropTypes.any,
PUESTOS0: PropTypes.any,
FORMULARIODEPERDONAL0: PropTypes.any,
COLEGIOJAPONESDEMORELOS0: PropTypes.any,
APELLIDO0: PropTypes.any,
SEXO0: PropTypes.any,
PUESTO0: PropTypes.any,
Pngtransparentmagnifyingglassmagnificationmagnifyingglassglasslogolightone0: PropTypes.any,
AGREGAR0: PropTypes.any,
ESCANEAR0: PropTypes.any,
Onetwooonefourfivenigthfourone0: PropTypes.any,
Pngtransparentwritemodifytooleditpendocumentmultimediasolidpxiconthumbnailone0: PropTypes.any,
NOMBREAPELLIDOPUESTOSEXOTELEFONOEDITARELIMINAR0: PropTypes.any,
FULANITOVERADocenteMseventhreetwoosixthreethreetwooseventhreetwoo0: PropTypes.any,
TELEFONO0: PropTypes.any,
HUELLA0: PropTypes.any,
NOMBRE0: PropTypes.any,
FORMULARIODEPERSONAL0: PropTypes.any,
DesktopfouronClick: PropTypes.any,
DesktopfouronMouseEnter: PropTypes.any,
DesktopfouronMouseOver: PropTypes.any,
DesktopfouronKeyPress: PropTypes.any,
DesktopfouronDrag: PropTypes.any,
DesktopfouronMouseLeave: PropTypes.any,
DesktopfouronMouseUp: PropTypes.any,
DesktopfouronMouseDown: PropTypes.any,
DesktopfouronKeyDown: PropTypes.any,
DesktopfouronChange: PropTypes.any,
Desktopfourondelay: PropTypes.any,
FrameonezeroonClick: PropTypes.any,
FrameonezeroonMouseEnter: PropTypes.any,
FrameonezeroonMouseOver: PropTypes.any,
FrameonezeroonKeyPress: PropTypes.any,
FrameonezeroonDrag: PropTypes.any,
FrameonezeroonMouseLeave: PropTypes.any,
FrameonezeroonMouseUp: PropTypes.any,
FrameonezeroonMouseDown: PropTypes.any,
FrameonezeroonKeyDown: PropTypes.any,
FrameonezeroonChange: PropTypes.any,
Frameonezeroondelay: PropTypes.any,
FrameonetwooonClick: PropTypes.any,
FrameonetwooonMouseEnter: PropTypes.any,
FrameonetwooonMouseOver: PropTypes.any,
FrameonetwooonKeyPress: PropTypes.any,
FrameonetwooonDrag: PropTypes.any,
FrameonetwooonMouseLeave: PropTypes.any,
FrameonetwooonMouseUp: PropTypes.any,
FrameonetwooonMouseDown: PropTypes.any,
FrameonetwooonKeyDown: PropTypes.any,
FrameonetwooonChange: PropTypes.any,
Frameonetwooondelay: PropTypes.any,
REPORTESonClick: PropTypes.any,
REPORTESonMouseEnter: PropTypes.any,
REPORTESonMouseOver: PropTypes.any,
REPORTESonKeyPress: PropTypes.any,
REPORTESonDrag: PropTypes.any,
REPORTESonMouseLeave: PropTypes.any,
REPORTESonMouseUp: PropTypes.any,
REPORTESonMouseDown: PropTypes.any,
REPORTESonKeyDown: PropTypes.any,
REPORTESonChange: PropTypes.any,
REPORTESondelay: PropTypes.any,
FrameonefouronClick: PropTypes.any,
FrameonefouronMouseEnter: PropTypes.any,
FrameonefouronMouseOver: PropTypes.any,
FrameonefouronKeyPress: PropTypes.any,
FrameonefouronDrag: PropTypes.any,
FrameonefouronMouseLeave: PropTypes.any,
FrameonefouronMouseUp: PropTypes.any,
FrameonefouronMouseDown: PropTypes.any,
FrameonefouronKeyDown: PropTypes.any,
FrameonefouronChange: PropTypes.any,
Frameonefourondelay: PropTypes.any,
USUARIOSonClick: PropTypes.any,
USUARIOSonMouseEnter: PropTypes.any,
USUARIOSonMouseOver: PropTypes.any,
USUARIOSonKeyPress: PropTypes.any,
USUARIOSonDrag: PropTypes.any,
USUARIOSonMouseLeave: PropTypes.any,
USUARIOSonMouseUp: PropTypes.any,
USUARIOSonMouseDown: PropTypes.any,
USUARIOSonKeyDown: PropTypes.any,
USUARIOSonChange: PropTypes.any,
USUARIOSondelay: PropTypes.any,
SALIRonClick: PropTypes.any,
SALIRonMouseEnter: PropTypes.any,
SALIRonMouseOver: PropTypes.any,
SALIRonKeyPress: PropTypes.any,
SALIRonDrag: PropTypes.any,
SALIRonMouseLeave: PropTypes.any,
SALIRonMouseUp: PropTypes.any,
SALIRonMouseDown: PropTypes.any,
SALIRonKeyDown: PropTypes.any,
SALIRonChange: PropTypes.any,
SALIRondelay: PropTypes.any,
MENUonClick: PropTypes.any,
MENUonMouseEnter: PropTypes.any,
MENUonMouseOver: PropTypes.any,
MENUonKeyPress: PropTypes.any,
MENUonDrag: PropTypes.any,
MENUonMouseLeave: PropTypes.any,
MENUonMouseUp: PropTypes.any,
MENUonMouseDown: PropTypes.any,
MENUonKeyDown: PropTypes.any,
MENUonChange: PropTypes.any,
MENUondelay: PropTypes.any,
FrameoneoneonClick: PropTypes.any,
FrameoneoneonMouseEnter: PropTypes.any,
FrameoneoneonMouseOver: PropTypes.any,
FrameoneoneonKeyPress: PropTypes.any,
FrameoneoneonDrag: PropTypes.any,
FrameoneoneonMouseLeave: PropTypes.any,
FrameoneoneonMouseUp: PropTypes.any,
FrameoneoneonMouseDown: PropTypes.any,
FrameoneoneonKeyDown: PropTypes.any,
FrameoneoneonChange: PropTypes.any,
Frameoneoneondelay: PropTypes.any,
PERSONALonClick: PropTypes.any,
PERSONALonMouseEnter: PropTypes.any,
PERSONALonMouseOver: PropTypes.any,
PERSONALonKeyPress: PropTypes.any,
PERSONALonDrag: PropTypes.any,
PERSONALonMouseLeave: PropTypes.any,
PERSONALonMouseUp: PropTypes.any,
PERSONALonMouseDown: PropTypes.any,
PERSONALonKeyDown: PropTypes.any,
PERSONALonChange: PropTypes.any,
PERSONALondelay: PropTypes.any,
FrameonethreeonClick: PropTypes.any,
FrameonethreeonMouseEnter: PropTypes.any,
FrameonethreeonMouseOver: PropTypes.any,
FrameonethreeonKeyPress: PropTypes.any,
FrameonethreeonDrag: PropTypes.any,
FrameonethreeonMouseLeave: PropTypes.any,
FrameonethreeonMouseUp: PropTypes.any,
FrameonethreeonMouseDown: PropTypes.any,
FrameonethreeonKeyDown: PropTypes.any,
FrameonethreeonChange: PropTypes.any,
Frameonethreeondelay: PropTypes.any,
PUESTOSonClick: PropTypes.any,
PUESTOSonMouseEnter: PropTypes.any,
PUESTOSonMouseOver: PropTypes.any,
PUESTOSonKeyPress: PropTypes.any,
PUESTOSonDrag: PropTypes.any,
PUESTOSonMouseLeave: PropTypes.any,
PUESTOSonMouseUp: PropTypes.any,
PUESTOSonMouseDown: PropTypes.any,
PUESTOSonKeyDown: PropTypes.any,
PUESTOSonChange: PropTypes.any,
PUESTOSondelay: PropTypes.any,
FrameonefiveonClick: PropTypes.any,
FrameonefiveonMouseEnter: PropTypes.any,
FrameonefiveonMouseOver: PropTypes.any,
FrameonefiveonKeyPress: PropTypes.any,
FrameonefiveonDrag: PropTypes.any,
FrameonefiveonMouseLeave: PropTypes.any,
FrameonefiveonMouseUp: PropTypes.any,
FrameonefiveonMouseDown: PropTypes.any,
FrameonefiveonKeyDown: PropTypes.any,
FrameonefiveonChange: PropTypes.any,
Frameonefiveondelay: PropTypes.any,
FORMULARIODEPERDONALonClick: PropTypes.any,
FORMULARIODEPERDONALonMouseEnter: PropTypes.any,
FORMULARIODEPERDONALonMouseOver: PropTypes.any,
FORMULARIODEPERDONALonKeyPress: PropTypes.any,
FORMULARIODEPERDONALonDrag: PropTypes.any,
FORMULARIODEPERDONALonMouseLeave: PropTypes.any,
FORMULARIODEPERDONALonMouseUp: PropTypes.any,
FORMULARIODEPERDONALonMouseDown: PropTypes.any,
FORMULARIODEPERDONALonKeyDown: PropTypes.any,
FORMULARIODEPERDONALonChange: PropTypes.any,
FORMULARIODEPERDONALondelay: PropTypes.any,
FrameonesevenonClick: PropTypes.any,
FrameonesevenonMouseEnter: PropTypes.any,
FrameonesevenonMouseOver: PropTypes.any,
FrameonesevenonKeyPress: PropTypes.any,
FrameonesevenonDrag: PropTypes.any,
FrameonesevenonMouseLeave: PropTypes.any,
FrameonesevenonMouseUp: PropTypes.any,
FrameonesevenonMouseDown: PropTypes.any,
FrameonesevenonKeyDown: PropTypes.any,
FrameonesevenonChange: PropTypes.any,
Frameonesevenondelay: PropTypes.any,
COLEGIOJAPONESDEMORELOSonClick: PropTypes.any,
COLEGIOJAPONESDEMORELOSonMouseEnter: PropTypes.any,
COLEGIOJAPONESDEMORELOSonMouseOver: PropTypes.any,
COLEGIOJAPONESDEMORELOSonKeyPress: PropTypes.any,
COLEGIOJAPONESDEMORELOSonDrag: PropTypes.any,
COLEGIOJAPONESDEMORELOSonMouseLeave: PropTypes.any,
COLEGIOJAPONESDEMORELOSonMouseUp: PropTypes.any,
COLEGIOJAPONESDEMORELOSonMouseDown: PropTypes.any,
COLEGIOJAPONESDEMORELOSonKeyDown: PropTypes.any,
COLEGIOJAPONESDEMORELOSonChange: PropTypes.any,
COLEGIOJAPONESDEMORELOSondelay: PropTypes.any,
FrameoneeightonClick: PropTypes.any,
FrameoneeightonMouseEnter: PropTypes.any,
FrameoneeightonMouseOver: PropTypes.any,
FrameoneeightonKeyPress: PropTypes.any,
FrameoneeightonDrag: PropTypes.any,
FrameoneeightonMouseLeave: PropTypes.any,
FrameoneeightonMouseUp: PropTypes.any,
FrameoneeightonMouseDown: PropTypes.any,
FrameoneeightonKeyDown: PropTypes.any,
FrameoneeightonChange: PropTypes.any,
Frameoneeightondelay: PropTypes.any,
NotojapanesebargainbuttononClick: PropTypes.any,
NotojapanesebargainbuttononMouseEnter: PropTypes.any,
NotojapanesebargainbuttononMouseOver: PropTypes.any,
NotojapanesebargainbuttononKeyPress: PropTypes.any,
NotojapanesebargainbuttononDrag: PropTypes.any,
NotojapanesebargainbuttononMouseLeave: PropTypes.any,
NotojapanesebargainbuttononMouseUp: PropTypes.any,
NotojapanesebargainbuttononMouseDown: PropTypes.any,
NotojapanesebargainbuttononKeyDown: PropTypes.any,
NotojapanesebargainbuttononChange: PropTypes.any,
Notojapanesebargainbuttonondelay: PropTypes.any,
VectoronClick: PropTypes.any,
VectoronMouseEnter: PropTypes.any,
VectoronMouseOver: PropTypes.any,
VectoronKeyPress: PropTypes.any,
VectoronDrag: PropTypes.any,
VectoronMouseLeave: PropTypes.any,
VectoronMouseUp: PropTypes.any,
VectoronMouseDown: PropTypes.any,
VectoronKeyDown: PropTypes.any,
VectoronChange: PropTypes.any,
Vectorondelay: PropTypes.any,
FrametwoozeroonClick: PropTypes.any,
FrametwoozeroonMouseEnter: PropTypes.any,
FrametwoozeroonMouseOver: PropTypes.any,
FrametwoozeroonKeyPress: PropTypes.any,
FrametwoozeroonDrag: PropTypes.any,
FrametwoozeroonMouseLeave: PropTypes.any,
FrametwoozeroonMouseUp: PropTypes.any,
FrametwoozeroonMouseDown: PropTypes.any,
FrametwoozeroonKeyDown: PropTypes.any,
FrametwoozeroonChange: PropTypes.any,
Frametwoozeroondelay: PropTypes.any,
APELLIDOonClick: PropTypes.any,
APELLIDOonMouseEnter: PropTypes.any,
APELLIDOonMouseOver: PropTypes.any,
APELLIDOonKeyPress: PropTypes.any,
APELLIDOonDrag: PropTypes.any,
APELLIDOonMouseLeave: PropTypes.any,
APELLIDOonMouseUp: PropTypes.any,
APELLIDOonMouseDown: PropTypes.any,
APELLIDOonKeyDown: PropTypes.any,
APELLIDOonChange: PropTypes.any,
APELLIDOondelay: PropTypes.any,
SEXOonClick: PropTypes.any,
SEXOonMouseEnter: PropTypes.any,
SEXOonMouseOver: PropTypes.any,
SEXOonKeyPress: PropTypes.any,
SEXOonDrag: PropTypes.any,
SEXOonMouseLeave: PropTypes.any,
SEXOonMouseUp: PropTypes.any,
SEXOonMouseDown: PropTypes.any,
SEXOonKeyDown: PropTypes.any,
SEXOonChange: PropTypes.any,
SEXOondelay: PropTypes.any,
FrameonenigthonClick: PropTypes.any,
FrameonenigthonMouseEnter: PropTypes.any,
FrameonenigthonMouseOver: PropTypes.any,
FrameonenigthonKeyPress: PropTypes.any,
FrameonenigthonDrag: PropTypes.any,
FrameonenigthonMouseLeave: PropTypes.any,
FrameonenigthonMouseUp: PropTypes.any,
FrameonenigthonMouseDown: PropTypes.any,
FrameonenigthonKeyDown: PropTypes.any,
FrameonenigthonChange: PropTypes.any,
Frameonenigthondelay: PropTypes.any,
PUESTOonClick: PropTypes.any,
PUESTOonMouseEnter: PropTypes.any,
PUESTOonMouseOver: PropTypes.any,
PUESTOonKeyPress: PropTypes.any,
PUESTOonDrag: PropTypes.any,
PUESTOonMouseLeave: PropTypes.any,
PUESTOonMouseUp: PropTypes.any,
PUESTOonMouseDown: PropTypes.any,
PUESTOonKeyDown: PropTypes.any,
PUESTOonChange: PropTypes.any,
PUESTOondelay: PropTypes.any,
FrametwoooneonClick: PropTypes.any,
FrametwoooneonMouseEnter: PropTypes.any,
FrametwoooneonMouseOver: PropTypes.any,
FrametwoooneonKeyPress: PropTypes.any,
FrametwoooneonDrag: PropTypes.any,
FrametwoooneonMouseLeave: PropTypes.any,
FrametwoooneonMouseUp: PropTypes.any,
FrametwoooneonMouseDown: PropTypes.any,
FrametwoooneonKeyDown: PropTypes.any,
FrametwoooneonChange: PropTypes.any,
Frametwoooneondelay: PropTypes.any,
FrametwootwooonClick: PropTypes.any,
FrametwootwooonMouseEnter: PropTypes.any,
FrametwootwooonMouseOver: PropTypes.any,
FrametwootwooonKeyPress: PropTypes.any,
FrametwootwooonDrag: PropTypes.any,
FrametwootwooonMouseLeave: PropTypes.any,
FrametwootwooonMouseUp: PropTypes.any,
FrametwootwooonMouseDown: PropTypes.any,
FrametwootwooonKeyDown: PropTypes.any,
FrametwootwooonChange: PropTypes.any,
Frametwootwooondelay: PropTypes.any,
FrametwoothreeonClick: PropTypes.any,
FrametwoothreeonMouseEnter: PropTypes.any,
FrametwoothreeonMouseOver: PropTypes.any,
FrametwoothreeonKeyPress: PropTypes.any,
FrametwoothreeonDrag: PropTypes.any,
FrametwoothreeonMouseLeave: PropTypes.any,
FrametwoothreeonMouseUp: PropTypes.any,
FrametwoothreeonMouseDown: PropTypes.any,
FrametwoothreeonKeyDown: PropTypes.any,
FrametwoothreeonChange: PropTypes.any,
Frametwoothreeondelay: PropTypes.any,
FrametwoofouronClick: PropTypes.any,
FrametwoofouronMouseEnter: PropTypes.any,
FrametwoofouronMouseOver: PropTypes.any,
FrametwoofouronKeyPress: PropTypes.any,
FrametwoofouronDrag: PropTypes.any,
FrametwoofouronMouseLeave: PropTypes.any,
FrametwoofouronMouseUp: PropTypes.any,
FrametwoofouronMouseDown: PropTypes.any,
FrametwoofouronKeyDown: PropTypes.any,
FrametwoofouronChange: PropTypes.any,
Frametwoofourondelay: PropTypes.any,
FrametwoosixonClick: PropTypes.any,
FrametwoosixonMouseEnter: PropTypes.any,
FrametwoosixonMouseOver: PropTypes.any,
FrametwoosixonKeyPress: PropTypes.any,
FrametwoosixonDrag: PropTypes.any,
FrametwoosixonMouseLeave: PropTypes.any,
FrametwoosixonMouseUp: PropTypes.any,
FrametwoosixonMouseDown: PropTypes.any,
FrametwoosixonKeyDown: PropTypes.any,
FrametwoosixonChange: PropTypes.any,
Frametwoosixondelay: PropTypes.any,
PngtransparentmagnifyingglassmagnificationmagnifyingglassglasslogolightoneonClick: PropTypes.any,
PngtransparentmagnifyingglassmagnificationmagnifyingglassglasslogolightoneonMouseEnter: PropTypes.any,
PngtransparentmagnifyingglassmagnificationmagnifyingglassglasslogolightoneonMouseOver: PropTypes.any,
PngtransparentmagnifyingglassmagnificationmagnifyingglassglasslogolightoneonKeyPress: PropTypes.any,
PngtransparentmagnifyingglassmagnificationmagnifyingglassglasslogolightoneonDrag: PropTypes.any,
PngtransparentmagnifyingglassmagnificationmagnifyingglassglasslogolightoneonMouseLeave: PropTypes.any,
PngtransparentmagnifyingglassmagnificationmagnifyingglassglasslogolightoneonMouseUp: PropTypes.any,
PngtransparentmagnifyingglassmagnificationmagnifyingglassglasslogolightoneonMouseDown: PropTypes.any,
PngtransparentmagnifyingglassmagnificationmagnifyingglassglasslogolightoneonKeyDown: PropTypes.any,
PngtransparentmagnifyingglassmagnificationmagnifyingglassglasslogolightoneonChange: PropTypes.any,
Pngtransparentmagnifyingglassmagnificationmagnifyingglassglasslogolightoneondelay: PropTypes.any,
FrameeightonClick: PropTypes.any,
FrameeightonMouseEnter: PropTypes.any,
FrameeightonMouseOver: PropTypes.any,
FrameeightonKeyPress: PropTypes.any,
FrameeightonDrag: PropTypes.any,
FrameeightonMouseLeave: PropTypes.any,
FrameeightonMouseUp: PropTypes.any,
FrameeightonMouseDown: PropTypes.any,
FrameeightonKeyDown: PropTypes.any,
FrameeightonChange: PropTypes.any,
Frameeightondelay: PropTypes.any,
AGREGARonClick: PropTypes.any,
AGREGARonMouseEnter: PropTypes.any,
AGREGARonMouseOver: PropTypes.any,
AGREGARonKeyPress: PropTypes.any,
AGREGARonDrag: PropTypes.any,
AGREGARonMouseLeave: PropTypes.any,
AGREGARonMouseUp: PropTypes.any,
AGREGARonMouseDown: PropTypes.any,
AGREGARonKeyDown: PropTypes.any,
AGREGARonChange: PropTypes.any,
AGREGARondelay: PropTypes.any,
FramethreesevenonClick: PropTypes.any,
FramethreesevenonMouseEnter: PropTypes.any,
FramethreesevenonMouseOver: PropTypes.any,
FramethreesevenonKeyPress: PropTypes.any,
FramethreesevenonDrag: PropTypes.any,
FramethreesevenonMouseLeave: PropTypes.any,
FramethreesevenonMouseUp: PropTypes.any,
FramethreesevenonMouseDown: PropTypes.any,
FramethreesevenonKeyDown: PropTypes.any,
FramethreesevenonChange: PropTypes.any,
Framethreesevenondelay: PropTypes.any,
ESCANEARonClick: PropTypes.any,
ESCANEARonMouseEnter: PropTypes.any,
ESCANEARonMouseOver: PropTypes.any,
ESCANEARonKeyPress: PropTypes.any,
ESCANEARonDrag: PropTypes.any,
ESCANEARonMouseLeave: PropTypes.any,
ESCANEARonMouseUp: PropTypes.any,
ESCANEARonMouseDown: PropTypes.any,
ESCANEARonKeyDown: PropTypes.any,
ESCANEARonChange: PropTypes.any,
ESCANEARondelay: PropTypes.any,
FrametwoofiveonClick: PropTypes.any,
FrametwoofiveonMouseEnter: PropTypes.any,
FrametwoofiveonMouseOver: PropTypes.any,
FrametwoofiveonKeyPress: PropTypes.any,
FrametwoofiveonDrag: PropTypes.any,
FrametwoofiveonMouseLeave: PropTypes.any,
FrametwoofiveonMouseUp: PropTypes.any,
FrametwoofiveonMouseDown: PropTypes.any,
FrametwoofiveonKeyDown: PropTypes.any,
FrametwoofiveonChange: PropTypes.any,
Frametwoofiveondelay: PropTypes.any,
OnetwooonefourfivenigthfouroneonClick: PropTypes.any,
OnetwooonefourfivenigthfouroneonMouseEnter: PropTypes.any,
OnetwooonefourfivenigthfouroneonMouseOver: PropTypes.any,
OnetwooonefourfivenigthfouroneonKeyPress: PropTypes.any,
OnetwooonefourfivenigthfouroneonDrag: PropTypes.any,
OnetwooonefourfivenigthfouroneonMouseLeave: PropTypes.any,
OnetwooonefourfivenigthfouroneonMouseUp: PropTypes.any,
OnetwooonefourfivenigthfouroneonMouseDown: PropTypes.any,
OnetwooonefourfivenigthfouroneonKeyDown: PropTypes.any,
OnetwooonefourfivenigthfouroneonChange: PropTypes.any,
Onetwooonefourfivenigthfouroneondelay: PropTypes.any,
PngtransparentwritemodifytooleditpendocumentmultimediasolidpxiconthumbnailoneonClick: PropTypes.any,
PngtransparentwritemodifytooleditpendocumentmultimediasolidpxiconthumbnailoneonMouseEnter: PropTypes.any,
PngtransparentwritemodifytooleditpendocumentmultimediasolidpxiconthumbnailoneonMouseOver: PropTypes.any,
PngtransparentwritemodifytooleditpendocumentmultimediasolidpxiconthumbnailoneonKeyPress: PropTypes.any,
PngtransparentwritemodifytooleditpendocumentmultimediasolidpxiconthumbnailoneonDrag: PropTypes.any,
PngtransparentwritemodifytooleditpendocumentmultimediasolidpxiconthumbnailoneonMouseLeave: PropTypes.any,
PngtransparentwritemodifytooleditpendocumentmultimediasolidpxiconthumbnailoneonMouseUp: PropTypes.any,
PngtransparentwritemodifytooleditpendocumentmultimediasolidpxiconthumbnailoneonMouseDown: PropTypes.any,
PngtransparentwritemodifytooleditpendocumentmultimediasolidpxiconthumbnailoneonKeyDown: PropTypes.any,
PngtransparentwritemodifytooleditpendocumentmultimediasolidpxiconthumbnailoneonChange: PropTypes.any,
Pngtransparentwritemodifytooleditpendocumentmultimediasolidpxiconthumbnailoneondelay: PropTypes.any,
NOMBREAPELLIDOPUESTOSEXOTELEFONOEDITARELIMINARonClick: PropTypes.any,
NOMBREAPELLIDOPUESTOSEXOTELEFONOEDITARELIMINARonMouseEnter: PropTypes.any,
NOMBREAPELLIDOPUESTOSEXOTELEFONOEDITARELIMINARonMouseOver: PropTypes.any,
NOMBREAPELLIDOPUESTOSEXOTELEFONOEDITARELIMINARonKeyPress: PropTypes.any,
NOMBREAPELLIDOPUESTOSEXOTELEFONOEDITARELIMINARonDrag: PropTypes.any,
NOMBREAPELLIDOPUESTOSEXOTELEFONOEDITARELIMINARonMouseLeave: PropTypes.any,
NOMBREAPELLIDOPUESTOSEXOTELEFONOEDITARELIMINARonMouseUp: PropTypes.any,
NOMBREAPELLIDOPUESTOSEXOTELEFONOEDITARELIMINARonMouseDown: PropTypes.any,
NOMBREAPELLIDOPUESTOSEXOTELEFONOEDITARELIMINARonKeyDown: PropTypes.any,
NOMBREAPELLIDOPUESTOSEXOTELEFONOEDITARELIMINARonChange: PropTypes.any,
NOMBREAPELLIDOPUESTOSEXOTELEFONOEDITARELIMINARondelay: PropTypes.any,
FULANITOVERADocenteMseventhreetwoosixthreethreetwooseventhreetwooonClick: PropTypes.any,
FULANITOVERADocenteMseventhreetwoosixthreethreetwooseventhreetwooonMouseEnter: PropTypes.any,
FULANITOVERADocenteMseventhreetwoosixthreethreetwooseventhreetwooonMouseOver: PropTypes.any,
FULANITOVERADocenteMseventhreetwoosixthreethreetwooseventhreetwooonKeyPress: PropTypes.any,
FULANITOVERADocenteMseventhreetwoosixthreethreetwooseventhreetwooonDrag: PropTypes.any,
FULANITOVERADocenteMseventhreetwoosixthreethreetwooseventhreetwooonMouseLeave: PropTypes.any,
FULANITOVERADocenteMseventhreetwoosixthreethreetwooseventhreetwooonMouseUp: PropTypes.any,
FULANITOVERADocenteMseventhreetwoosixthreethreetwooseventhreetwooonMouseDown: PropTypes.any,
FULANITOVERADocenteMseventhreetwoosixthreethreetwooseventhreetwooonKeyDown: PropTypes.any,
FULANITOVERADocenteMseventhreetwoosixthreethreetwooseventhreetwooonChange: PropTypes.any,
FULANITOVERADocenteMseventhreetwoosixthreethreetwooseventhreetwooondelay: PropTypes.any,
FramethreefiveonClick: PropTypes.any,
FramethreefiveonMouseEnter: PropTypes.any,
FramethreefiveonMouseOver: PropTypes.any,
FramethreefiveonKeyPress: PropTypes.any,
FramethreefiveonDrag: PropTypes.any,
FramethreefiveonMouseLeave: PropTypes.any,
FramethreefiveonMouseUp: PropTypes.any,
FramethreefiveonMouseDown: PropTypes.any,
FramethreefiveonKeyDown: PropTypes.any,
FramethreefiveonChange: PropTypes.any,
Framethreefiveondelay: PropTypes.any,
TELEFONOonClick: PropTypes.any,
TELEFONOonMouseEnter: PropTypes.any,
TELEFONOonMouseOver: PropTypes.any,
TELEFONOonKeyPress: PropTypes.any,
TELEFONOonDrag: PropTypes.any,
TELEFONOonMouseLeave: PropTypes.any,
TELEFONOonMouseUp: PropTypes.any,
TELEFONOonMouseDown: PropTypes.any,
TELEFONOonKeyDown: PropTypes.any,
TELEFONOonChange: PropTypes.any,
TELEFONOondelay: PropTypes.any,
FramethreesixonClick: PropTypes.any,
FramethreesixonMouseEnter: PropTypes.any,
FramethreesixonMouseOver: PropTypes.any,
FramethreesixonKeyPress: PropTypes.any,
FramethreesixonDrag: PropTypes.any,
FramethreesixonMouseLeave: PropTypes.any,
FramethreesixonMouseUp: PropTypes.any,
FramethreesixonMouseDown: PropTypes.any,
FramethreesixonKeyDown: PropTypes.any,
FramethreesixonChange: PropTypes.any,
Framethreesixondelay: PropTypes.any,
HUELLAonClick: PropTypes.any,
HUELLAonMouseEnter: PropTypes.any,
HUELLAonMouseOver: PropTypes.any,
HUELLAonKeyPress: PropTypes.any,
HUELLAonDrag: PropTypes.any,
HUELLAonMouseLeave: PropTypes.any,
HUELLAonMouseUp: PropTypes.any,
HUELLAonMouseDown: PropTypes.any,
HUELLAonKeyDown: PropTypes.any,
HUELLAonChange: PropTypes.any,
HUELLAondelay: PropTypes.any,
NOMBREonClick: PropTypes.any,
NOMBREonMouseEnter: PropTypes.any,
NOMBREonMouseOver: PropTypes.any,
NOMBREonKeyPress: PropTypes.any,
NOMBREonDrag: PropTypes.any,
NOMBREonMouseLeave: PropTypes.any,
NOMBREonMouseUp: PropTypes.any,
NOMBREonMouseDown: PropTypes.any,
NOMBREonKeyDown: PropTypes.any,
NOMBREonChange: PropTypes.any,
NOMBREondelay: PropTypes.any,
FORMULARIODEPERSONALonClick: PropTypes.any,
FORMULARIODEPERSONALonMouseEnter: PropTypes.any,
FORMULARIODEPERSONALonMouseOver: PropTypes.any,
FORMULARIODEPERSONALonKeyPress: PropTypes.any,
FORMULARIODEPERSONALonDrag: PropTypes.any,
FORMULARIODEPERSONALonMouseLeave: PropTypes.any,
FORMULARIODEPERSONALonMouseUp: PropTypes.any,
FORMULARIODEPERSONALonMouseDown: PropTypes.any,
FORMULARIODEPERSONALonKeyDown: PropTypes.any,
FORMULARIODEPERSONALonChange: PropTypes.any,
FORMULARIODEPERSONALondelay: PropTypes.any
}
export default Desktopfour;