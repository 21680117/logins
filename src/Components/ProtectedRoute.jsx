import React from 'react';
import {
    Route
  } from "react-router-dom";
import { withAuthenticationRequired } from '@auth0/auth0-react';
import { useAuth0 } from "@auth0/auth0-react";

const DefaultAccesDenied = ()=><h1>Acceso Denegado</h1>

const ProtectedRoute = ({ component, Rols, AccesDeniedComponent, NameSpace,  ...args }) => {
  const { user } = useAuth0()
  return (
    <Route
      render={(props) => {
        let Component = withAuthenticationRequired(component, {
          
        });
        //Compruebo que tenga las pripiedades necesarias para checkar el rol
        if(Rols && NameSpace && user){
          //Obtengo los roles del usuario
          const userRols = user[NameSpace];
          //Esta variable indica si tiene el rol esperado
          let hasRol = false;
          //Recorro la lista de roles del usuario
          for(let i = 0; i < userRols.length; i++){
            const rol = userRols[i];
            //Si Rols coincide con el del usuario hasRol pasa a ser true
            if(Rols.indexOf(rol) > -1){
              hasRol = true;
              break
            }
          }
          //Si has rol es false retorno el comoponente de acceso denegado
          if(!hasRol){
            if(AccesDeniedComponent)
              return <AccesDeniedComponent {...props} /> 
            else
              return <DefaultAccesDenied {...props} />
          }
            
        }
          
        return <Component {...props} />;
      }}
      {...args}
    />
    
  )};
export default React.memo(ProtectedRoute);
