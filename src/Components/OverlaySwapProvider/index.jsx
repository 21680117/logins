import React from 'react';

import { reducerOverlay } from '../../reducer/appReducer';
import Overlay from '../Overlay';

export const OverlaySwapContext = React.createContext();

const getColor = (value)=>{
    if(!value)
        return {};
    return { backgroundColor: value }
}

const OverlaySwapWrapper = ({}) => {
    const [ backProps, setBackProps ] = React.useState({ color:false, interaction: false});
    const [ nameSelectedComponent, setNameSelectedComponent ] = React.useContext(OverlaySwapContext);
    
    React.useEffect(()=>{
        const selectedComponent = ListComponents.find(item=>item.props.name === nameSelectedComponent.NameComponent);
        if(selectedComponent)
            setBackProps({ color: nameSelectedComponent.overlayBackground,  interaction:(nameSelectedComponent.prototyping.overlayBackgroundInteraction && nameSelectedComponent.prototyping.overlayBackgroundInteraction  !== 'NONE')});

    },[nameSelectedComponent])
    const ListComponents = [];
    const closeOverlaySwap = () => {
        if(backProps.interaction)
            setNameSelectedComponent(false);
    }


    if(!nameSelectedComponent)
        return null;
    return (
        <>
        <div className={`backdrop `} style={backProps.color !== '' ? getColor(backProps.color) : { visibility: 'hidden' }}  ></div>
        <div className="content-overlay-wrapper">
            <Overlay isOpen={true} hasBackGround={false} Components={ListComponents} selectedName={nameSelectedComponent} onClickBack={closeOverlaySwap} />
        </div>
        </>
    );
}

const OverlaySwapProvider = ({children}) => {
    const [ nameSelectedComponent, setNameSelectedComponent ] = React.useReducer(reducerOverlay, false);
    const store = React.useMemo(() => ([nameSelectedComponent, setNameSelectedComponent]), [nameSelectedComponent]);

    return(
        <OverlaySwapContext.Provider value={store}>
            <OverlaySwapWrapper  />
            {children}
        </OverlaySwapContext.Provider>
    )
}

export default OverlaySwapProvider