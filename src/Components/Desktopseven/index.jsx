import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Desktopseven.css'





const Desktopseven = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        

        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition nodeRef={nodeRef} in={true} appear={true} timeout={10} classNames={SingletoneNavigation.getTransitionInstance()?.Desktopseven?.cssClass || '' }>

    <div id="id_oneonezero_onezeroeight" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } desktopseven ${ props.cssClass } `} style={ { ...{ ...{}, transitionDuration: `${((SingletoneNavigation.getTransitionInstance()?.Desktopseven?.duration || 0) * 1000).toFixed(0)}ms` }, ...props.style }} onClick={ props.DesktopsevenonClick } onMouseEnter={ props.DesktopsevenonMouseEnter } onMouseOver={ props.DesktopsevenonMouseOver } onKeyPress={ props.DesktopsevenonKeyPress } onDrag={ props.DesktopsevenonDrag } onMouseLeave={ props.DesktopsevenonMouseLeave } onMouseUp={ props.DesktopsevenonMouseUp } onMouseDown={ props.DesktopsevenonMouseDown } onKeyDown={ props.DesktopsevenonKeyDown } onChange={ props.DesktopsevenonChange } ondelay={ props.Desktopsevenondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} classNames={transaction['frametwoonigth']?.animationClass || {}}>

          <div id="id_oneonezero_twoozeroone" className={` frame frametwoonigth ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrametwoonigthStyle , transitionDuration: transaction['frametwoonigth']?.duration, transitionTimingFunction: transaction['frametwoonigth']?.timingFunction } } onClick={ props.FrametwoonigthonClick } onMouseEnter={ props.FrametwoonigthonMouseEnter } onMouseOver={ props.FrametwoonigthonMouseOver } onKeyPress={ props.FrametwoonigthonKeyPress } onDrag={ props.FrametwoonigthonDrag } onMouseLeave={ props.FrametwoonigthonMouseLeave } onMouseUp={ props.FrametwoonigthonMouseUp } onMouseDown={ props.FrametwoonigthonMouseDown } onKeyDown={ props.FrametwoonigthonKeyDown } onChange={ props.FrametwoonigthonChange } ondelay={ props.Frametwoonigthondelay }>
            <CSSTransition in={_in} appear={true} classNames={transaction['colegiojaponesdemorelos']?.animationClass || {}}>

              <span id="id_oneonezero_twoozerotwoo"  className={` text colegiojaponesdemorelos    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.COLEGIOJAPONESDEMORELOSStyle , transitionDuration: transaction['colegiojaponesdemorelos']?.duration, transitionTimingFunction: transaction['colegiojaponesdemorelos']?.timingFunction }} onClick={ props.COLEGIOJAPONESDEMORELOSonClick } onMouseEnter={ props.COLEGIOJAPONESDEMORELOSonMouseEnter } onMouseOver={ props.COLEGIOJAPONESDEMORELOSonMouseOver } onKeyPress={ props.COLEGIOJAPONESDEMORELOSonKeyPress } onDrag={ props.COLEGIOJAPONESDEMORELOSonDrag } onMouseLeave={ props.COLEGIOJAPONESDEMORELOSonMouseLeave } onMouseUp={ props.COLEGIOJAPONESDEMORELOSonMouseUp } onMouseDown={ props.COLEGIOJAPONESDEMORELOSonMouseDown } onKeyDown={ props.COLEGIOJAPONESDEMORELOSonKeyDown } onChange={ props.COLEGIOJAPONESDEMORELOSonChange } ondelay={ props.COLEGIOJAPONESDEMORELOSondelay } >{props.COLEGIOJAPONESDEMORELOS0 || `COLEGIO JAPONES DE MORELOS`}</span>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} classNames={transaction['framethreezero']?.animationClass || {}}>

          <div id="id_oneonezero_twoozerothree" className={` frame framethreezero ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FramethreezeroStyle , transitionDuration: transaction['framethreezero']?.duration, transitionTimingFunction: transaction['framethreezero']?.timingFunction } } onClick={ props.FramethreezeroonClick } onMouseEnter={ props.FramethreezeroonMouseEnter } onMouseOver={ props.FramethreezeroonMouseOver } onKeyPress={ props.FramethreezeroonKeyPress } onDrag={ props.FramethreezeroonDrag } onMouseLeave={ props.FramethreezeroonMouseLeave } onMouseUp={ props.FramethreezeroonMouseUp } onMouseDown={ props.FramethreezeroonMouseDown } onKeyDown={ props.FramethreezeroonKeyDown } onChange={ props.FramethreezeroonChange } ondelay={ props.Framethreezeroondelay }>
            <CSSTransition in={_in} appear={true} classNames={transaction['notojapanesebargainbutton']?.animationClass || {}}>

              <div id="id_oneonezero_twoozerofour" className={` frame notojapanesebargainbutton ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.NotojapanesebargainbuttonStyle , transitionDuration: transaction['notojapanesebargainbutton']?.duration, transitionTimingFunction: transaction['notojapanesebargainbutton']?.timingFunction } } onClick={ props.NotojapanesebargainbuttononClick } onMouseEnter={ props.NotojapanesebargainbuttononMouseEnter } onMouseOver={ props.NotojapanesebargainbuttononMouseOver } onKeyPress={ props.NotojapanesebargainbuttononKeyPress } onDrag={ props.NotojapanesebargainbuttononDrag } onMouseLeave={ props.NotojapanesebargainbuttononMouseLeave } onMouseUp={ props.NotojapanesebargainbuttononMouseUp } onMouseDown={ props.NotojapanesebargainbuttononMouseDown } onKeyDown={ props.NotojapanesebargainbuttononKeyDown } onChange={ props.NotojapanesebargainbuttononChange } ondelay={ props.Notojapanesebargainbuttonondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_oneonezero_twoozerofive" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="84.375" height="84.375">

                    <path d="M42.1875 84.375C65.487 84.375 84.375 65.487 84.375 42.1875C84.375 18.888 65.487 0 42.1875 0C18.888 0 0 18.888 0 42.1875C0 65.487 18.888 84.375 42.1875 84.375Z" />
                  </svg>
                </CSSTransition>
                <CSSTransition in={_in} appear={true} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_oneonezero_twoozerosix" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="78.890625" height="78.890625">

                    <path d="M39.4453 78.8906C61.2304 78.8906 78.8906 61.2304 78.8906 39.4453C78.8906 17.6603 61.2304 0 39.4453 0C17.6603 0 0 17.6603 0 39.4453C0 61.2304 17.6603 78.8906 39.4453 78.8906Z" />
                  </svg>
                </CSSTransition>
                <CSSTransition in={_in} appear={true} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_oneonezero_twoozeroseven" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="27.075927734375" height="23.756103515625">

                    <path d="M3.52479 10.6523C6.68885 5.66016 13.4388 1.51172 20.4701 0.246093C22.2279 -0.0351565 23.9857 -0.175781 25.4623 0.386719C26.5873 0.808594 27.5014 1.86328 26.8685 3.05859C26.3764 4.04297 25.0404 4.46484 23.9857 4.81641C17.3899 6.9929 11.7023 11.2957 7.81385 17.0508C6.4076 19.1602 4.29822 24.9961 1.69666 23.5195C-1.04553 21.9023 -0.483026 16.8398 3.52479 10.6523L3.52479 10.6523Z" />
                  </svg>
                </CSSTransition>
                <CSSTransition in={_in} appear={true} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_oneonezero_twoozeroeight" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="57.9921875" height="57.3336181640625">

                    <path d="M19.3198 16.9039C18.0541 18.8727 16.6479 20.9118 15.1713 22.8805C14.9745 23.1196 14.876 23.4149 14.8901 23.7243L14.8901 55.9274C14.8901 56.7008 14.2573 57.3336 13.4838 57.3336L10.601 57.3336C9.82758 57.3336 9.19477 56.7008 9.19477 55.9274L9.19477 33.2164C9.19202 33.0318 9.15292 32.8494 9.0797 32.6799C9.00647 32.5103 8.90057 32.3568 8.76802 32.2282C8.63548 32.0996 8.47889 31.9983 8.30721 31.9302C8.13552 31.8621 7.95211 31.8285 7.76743 31.8313C7.40883 31.8383 7.05727 31.9789 6.80415 32.2321C5.89008 33.1461 4.90571 33.9196 3.99165 34.7633C3.84998 34.8821 3.68628 34.9718 3.50991 35.0273C3.33354 35.0828 3.14796 35.1029 2.96379 35.0866C2.77962 35.0703 2.60048 35.0178 2.43661 34.9322C2.27275 34.8465 2.12738 34.7294 2.00883 34.5875C1.93149 34.4891 1.86118 34.3836 1.81196 34.2711C1.31977 33.2868 0.757272 32.0914 0.194772 31.1071C-0.163822 30.4953 -0.0161657 29.7149 0.546334 29.2789C5.95894 25.1628 10.621 20.1439 14.3276 14.443C14.6932 13.8453 15.4385 13.6063 16.0854 13.8805L18.6166 14.9352C19.3127 15.1672 19.6854 15.9196 19.4534 16.6157C19.4182 16.7211 19.376 16.8125 19.3198 16.9039ZM18.1948 3.40394C14.3658 8.2768 9.86837 12.5849 4.8354 16.2008C4.20962 16.6578 3.33071 16.5172 2.87368 15.8914C2.84555 15.8563 2.82446 15.8141 2.79633 15.7789C2.37446 15.0055 1.81196 14.1618 1.31977 13.3883C0.897897 12.7414 1.05258 11.8766 1.67133 11.4196C5.89008 8.46644 10.3901 4.318 12.9916 0.591438C13.3432 0.0640947 14.0182 -0.139812 14.6088 0.0992503L17.4916 1.22425C18.2299 1.45628 18.6448 2.25081 18.4057 2.98909C18.3565 3.13675 18.2862 3.27738 18.1948 3.40394ZM56.5854 40.5993L50.3979 40.5993C49.6245 40.5993 48.9916 41.2321 48.9916 42.0055L48.9916 51.2868C48.9916 54.1696 48.4291 55.6461 46.3198 56.4196C44.4213 57.2633 41.7495 57.3336 37.812 57.3336C37.2002 57.3336 36.6588 56.9328 36.476 56.3493C36.2651 55.6461 35.9838 54.8024 35.7026 54.0289C35.4284 53.3047 35.801 52.4891 36.5252 52.2219C36.687 52.1586 36.8627 52.1305 37.0385 52.1305C39.2885 52.2008 41.187 52.2008 41.8901 52.1305C42.8041 52.0602 43.0854 51.8493 43.0854 51.1461L43.0854 42.0055C43.0854 41.2321 42.4526 40.5993 41.6791 40.5993L20.2338 40.5993C19.4604 40.5993 18.8276 39.9664 18.8276 39.193L18.8276 36.943C18.8276 36.1696 19.4604 35.5367 20.2338 35.5367L41.6791 35.5367C42.4526 35.5367 43.0854 34.9039 43.0854 34.1305L43.0854 32.3024C43.0854 31.5289 42.4526 30.8961 41.6791 30.8961L21.7807 30.8961C21.0073 30.8961 20.3745 30.2633 20.3745 29.4899L20.3745 27.4508C20.3745 26.6774 21.0073 26.0446 21.7807 26.0446L55.3901 26.0446C56.1635 26.0446 56.7963 26.6774 56.7963 27.4508L56.7963 29.4899C56.7963 30.2633 56.1635 30.8961 55.3901 30.8961L50.3979 30.8961C49.6245 30.8961 48.9916 31.5289 48.9916 32.3024L48.9916 34.1305C48.9916 34.9039 49.6245 35.5367 50.3979 35.5367L56.5854 35.5367C57.3588 35.5367 57.9916 36.1696 57.9916 36.943L57.9916 39.193C58.0127 39.9453 57.4221 40.5782 56.6698 40.5993L56.5854 40.5993ZM28.6713 41.7946C30.8581 43.8125 32.876 45.9993 34.7182 48.3336C35.1893 48.9243 35.0909 49.7821 34.5073 50.2532C34.4868 50.2734 34.463 50.2901 34.437 50.3024L32.3276 51.7789C31.6948 52.2289 30.8159 52.0742 30.3659 51.4414C30.3659 51.4344 30.3588 51.4344 30.3588 51.4274C28.6576 49.1243 26.8018 46.9396 24.8041 44.8883C24.2487 44.3117 24.2698 43.4047 24.8463 42.8493C24.9026 42.8 24.9588 42.7508 25.0151 42.7086L26.8432 41.5133C27.4479 41.211 28.1791 41.3235 28.6713 41.7946ZM52.8588 22.5289L25.2963 22.5289C24.5229 22.5289 23.8901 21.8961 23.8901 21.1227L23.8901 2.84144C23.8901 2.068 24.5229 1.43519 25.2963 1.43519L52.8588 1.43519C53.6323 1.43519 54.2651 2.068 54.2651 2.84144L54.2651 21.1227C54.2651 21.4956 54.1169 21.8533 53.8532 22.1171C53.5895 22.3808 53.2318 22.5289 52.8588 22.5289ZM46.9526 5.72425L30.9916 5.72425C30.2182 5.72425 29.5854 6.35706 29.5854 7.1305L29.5854 8.53675C29.5854 9.31019 30.2182 9.943 30.9916 9.943L46.9526 9.943C47.726 9.943 48.3588 9.31019 48.3588 8.53675L48.3588 7.1305C48.3588 6.35706 47.726 5.72425 46.9526 5.72425ZM46.9526 13.9508L30.9916 13.9508C30.2182 13.9508 29.5854 14.5836 29.5854 15.3571L29.5854 16.7633C29.5854 17.5368 30.2182 18.1696 30.9916 18.1696L46.9526 18.1696C47.726 18.1696 48.3588 17.5368 48.3588 16.7633L48.3588 15.3571C48.3588 14.5836 47.726 13.9508 46.9526 13.9508Z" />
                  </svg>
                </CSSTransition>
                <CSSTransition in={_in} appear={true} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_oneonezero_twoozeronigth" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="27.075927734375" height="23.756103515625">

                    <path d="M3.52479 10.6523C6.68885 5.66016 13.4388 1.51172 20.4701 0.246093C22.2279 -0.0351565 23.9857 -0.175781 25.4623 0.386719C26.5873 0.808594 27.5014 1.86328 26.8685 3.05859C26.3764 4.04297 25.0404 4.46484 23.9857 4.81641C17.3899 6.9929 11.7023 11.2957 7.81385 17.0508C6.4076 19.1602 4.29822 24.9961 1.69666 23.5195C-1.04553 21.9023 -0.483026 16.8398 3.52479 10.6523L3.52479 10.6523Z" />
                  </svg>
                </CSSTransition>
              </div>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} classNames={transaction['framethreetwoo']?.animationClass || {}}>

          <div id="id_oneonezero_twootwoozero" className={` frame framethreetwoo ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FramethreetwooStyle , transitionDuration: transaction['framethreetwoo']?.duration, transitionTimingFunction: transaction['framethreetwoo']?.timingFunction } } onClick={ props.FramethreetwooonClick } onMouseEnter={ props.FramethreetwooonMouseEnter } onMouseOver={ props.FramethreetwooonMouseOver } onKeyPress={ props.FramethreetwooonKeyPress } onDrag={ props.FramethreetwooonDrag } onMouseLeave={ props.FramethreetwooonMouseLeave } onMouseUp={ props.FramethreetwooonMouseUp } onMouseDown={ props.FramethreetwooonMouseDown } onKeyDown={ props.FramethreetwooonKeyDown } onChange={ props.FramethreetwooonChange } ondelay={ props.Framethreetwooondelay }>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameoneseven']?.animationClass || {}}>

              <div id="id_oneonezero_twootwooone" className={` frame frameoneseven ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonesevenStyle , transitionDuration: transaction['frameoneseven']?.duration, transitionTimingFunction: transaction['frameoneseven']?.timingFunction } } onClick={ props.FrameonesevenonClick } onMouseEnter={ props.FrameonesevenonMouseEnter } onMouseOver={ props.FrameonesevenonMouseOver } onKeyPress={ props.FrameonesevenonKeyPress } onDrag={ props.FrameonesevenonDrag } onMouseLeave={ props.FrameonesevenonMouseLeave } onMouseUp={ props.FrameonesevenonMouseUp } onMouseDown={ props.FrameonesevenonMouseDown } onKeyDown={ props.FrameonesevenonKeyDown } onChange={ props.FrameonesevenonChange } ondelay={ props.Frameonesevenondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['apellido']?.animationClass || {}}>

                  <span id="id_oneonezero_twootwootwoo"  className={` text apellido    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.APELLIDOStyle , transitionDuration: transaction['apellido']?.duration, transitionTimingFunction: transaction['apellido']?.timingFunction }} onClick={ props.APELLIDOonClick } onMouseEnter={ props.APELLIDOonMouseEnter } onMouseOver={ props.APELLIDOonMouseOver } onKeyPress={ props.APELLIDOonKeyPress } onDrag={ props.APELLIDOonDrag } onMouseLeave={ props.APELLIDOonMouseLeave } onMouseUp={ props.APELLIDOonMouseUp } onMouseDown={ props.APELLIDOonMouseDown } onKeyDown={ props.APELLIDOonKeyDown } onChange={ props.APELLIDOonChange } ondelay={ props.APELLIDOondelay } >{props.APELLIDO0 || `APELLIDO`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameoneeight']?.animationClass || {}}>

              <div id="id_oneonezero_twootwoothree" className={` frame frameoneeight ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameoneeightStyle , transitionDuration: transaction['frameoneeight']?.duration, transitionTimingFunction: transaction['frameoneeight']?.timingFunction } } onClick={ props.FrameoneeightonClick } onMouseEnter={ props.FrameoneeightonMouseEnter } onMouseOver={ props.FrameoneeightonMouseOver } onKeyPress={ props.FrameoneeightonKeyPress } onDrag={ props.FrameoneeightonDrag } onMouseLeave={ props.FrameoneeightonMouseLeave } onMouseUp={ props.FrameoneeightonMouseUp } onMouseDown={ props.FrameoneeightonMouseDown } onKeyDown={ props.FrameoneeightonKeyDown } onChange={ props.FrameoneeightonChange } ondelay={ props.Frameoneeightondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['email']?.animationClass || {}}>

                  <span id="id_oneonezero_twootwoofour"  className={` text email    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.EMAILStyle , transitionDuration: transaction['email']?.duration, transitionTimingFunction: transaction['email']?.timingFunction }} onClick={ props.EMAILonClick } onMouseEnter={ props.EMAILonMouseEnter } onMouseOver={ props.EMAILonMouseOver } onKeyPress={ props.EMAILonKeyPress } onDrag={ props.EMAILonDrag } onMouseLeave={ props.EMAILonMouseLeave } onMouseUp={ props.EMAILonMouseUp } onMouseDown={ props.EMAILonMouseDown } onKeyDown={ props.EMAILonKeyDown } onChange={ props.EMAILonChange } ondelay={ props.EMAILondelay } >{props.EMAIL0 || `SEXO`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frametwoozero']?.animationClass || {}}>

              <div id="id_oneonezero_twootwoofive" className={` frame frametwoozero ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrametwoozeroStyle , transitionDuration: transaction['frametwoozero']?.duration, transitionTimingFunction: transaction['frametwoozero']?.timingFunction } } onClick={ props.FrametwoozeroonClick } onMouseEnter={ props.FrametwoozeroonMouseEnter } onMouseOver={ props.FrametwoozeroonMouseOver } onKeyPress={ props.FrametwoozeroonKeyPress } onDrag={ props.FrametwoozeroonDrag } onMouseLeave={ props.FrametwoozeroonMouseLeave } onMouseUp={ props.FrametwoozeroonMouseUp } onMouseDown={ props.FrametwoozeroonMouseDown } onKeyDown={ props.FrametwoozeroonKeyDown } onChange={ props.FrametwoozeroonChange } ondelay={ props.Frametwoozeroondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['telefono']?.animationClass || {}}>

                  <span id="id_oneonezero_twootwoosix"  className={` text telefono    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.TELEFONOStyle , transitionDuration: transaction['telefono']?.duration, transitionTimingFunction: transaction['telefono']?.timingFunction }} onClick={ props.TELEFONOonClick } onMouseEnter={ props.TELEFONOonMouseEnter } onMouseOver={ props.TELEFONOonMouseOver } onKeyPress={ props.TELEFONOonKeyPress } onDrag={ props.TELEFONOonDrag } onMouseLeave={ props.TELEFONOonMouseLeave } onMouseUp={ props.TELEFONOonMouseUp } onMouseDown={ props.TELEFONOonMouseDown } onKeyDown={ props.TELEFONOonKeyDown } onChange={ props.TELEFONOonChange } ondelay={ props.TELEFONOondelay } >{props.TELEFONO0 || `TELEFONO`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameonenigth']?.animationClass || {}}>

              <div id="id_oneonezero_twootwooseven" className={` frame frameonenigth ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonenigthStyle , transitionDuration: transaction['frameonenigth']?.duration, transitionTimingFunction: transaction['frameonenigth']?.timingFunction } } onClick={ props.FrameonenigthonClick } onMouseEnter={ props.FrameonenigthonMouseEnter } onMouseOver={ props.FrameonenigthonMouseOver } onKeyPress={ props.FrameonenigthonKeyPress } onDrag={ props.FrameonenigthonDrag } onMouseLeave={ props.FrameonenigthonMouseLeave } onMouseUp={ props.FrameonenigthonMouseUp } onMouseDown={ props.FrameonenigthonMouseDown } onKeyDown={ props.FrameonenigthonKeyDown } onChange={ props.FrameonenigthonChange } ondelay={ props.Frameonenigthondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['puesto']?.animationClass || {}}>

                  <span id="id_oneonezero_twootwooeight"  className={` text puesto    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.PUESTOStyle , transitionDuration: transaction['puesto']?.duration, transitionTimingFunction: transaction['puesto']?.timingFunction }} onClick={ props.PUESTOonClick } onMouseEnter={ props.PUESTOonMouseEnter } onMouseOver={ props.PUESTOonMouseOver } onKeyPress={ props.PUESTOonKeyPress } onDrag={ props.PUESTOonDrag } onMouseLeave={ props.PUESTOonMouseLeave } onMouseUp={ props.PUESTOonMouseUp } onMouseDown={ props.PUESTOonMouseDown } onKeyDown={ props.PUESTOonKeyDown } onChange={ props.PUESTOonChange } ondelay={ props.PUESTOondelay } >{props.PUESTO0 || `PUESTO`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameoneseven']?.animationClass || {}}>

              <div id="id_oneonezero_twootwoonigth" className={` frame frameoneseven ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameonesevenStyle , transitionDuration: transaction['frameoneseven']?.duration, transitionTimingFunction: transaction['frameoneseven']?.timingFunction } } onClick={ props.FrameonesevenonClick } onMouseEnter={ props.FrameonesevenonMouseEnter } onMouseOver={ props.FrameonesevenonMouseOver } onKeyPress={ props.FrameonesevenonKeyPress } onDrag={ props.FrameonesevenonDrag } onMouseLeave={ props.FrameonesevenonMouseLeave } onMouseUp={ props.FrameonesevenonMouseUp } onMouseDown={ props.FrameonesevenonMouseDown } onKeyDown={ props.FrameonesevenonKeyDown } onChange={ props.FrameonesevenonChange } ondelay={ props.Frameonesevenondelay }>

              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frametwooone']?.animationClass || {}}>

              <div id="id_oneonezero_twoothreezero" className={` frame frametwooone ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrametwoooneStyle , transitionDuration: transaction['frametwooone']?.duration, transitionTimingFunction: transaction['frametwooone']?.timingFunction } } onClick={ props.FrametwoooneonClick } onMouseEnter={ props.FrametwoooneonMouseEnter } onMouseOver={ props.FrametwoooneonMouseOver } onKeyPress={ props.FrametwoooneonKeyPress } onDrag={ props.FrametwoooneonDrag } onMouseLeave={ props.FrametwoooneonMouseLeave } onMouseUp={ props.FrametwoooneonMouseUp } onMouseDown={ props.FrametwoooneonMouseDown } onKeyDown={ props.FrametwoooneonKeyDown } onChange={ props.FrametwoooneonChange } ondelay={ props.Frametwoooneondelay }>

              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frametwootwoo']?.animationClass || {}}>

              <div id="id_oneonezero_twoothreeone" className={` frame frametwootwoo ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrametwootwooStyle , transitionDuration: transaction['frametwootwoo']?.duration, transitionTimingFunction: transaction['frametwootwoo']?.timingFunction } } onClick={ props.FrametwootwooonClick } onMouseEnter={ props.FrametwootwooonMouseEnter } onMouseOver={ props.FrametwootwooonMouseOver } onKeyPress={ props.FrametwootwooonKeyPress } onDrag={ props.FrametwootwooonDrag } onMouseLeave={ props.FrametwootwooonMouseLeave } onMouseUp={ props.FrametwootwooonMouseUp } onMouseDown={ props.FrametwootwooonMouseDown } onKeyDown={ props.FrametwootwooonKeyDown } onChange={ props.FrametwootwooonChange } ondelay={ props.Frametwootwooondelay }>

              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frametwoothree']?.animationClass || {}}>

              <div id="id_oneonezero_twoothreetwoo" className={` frame frametwoothree ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrametwoothreeStyle , transitionDuration: transaction['frametwoothree']?.duration, transitionTimingFunction: transaction['frametwoothree']?.timingFunction } } onClick={ props.FrametwoothreeonClick } onMouseEnter={ props.FrametwoothreeonMouseEnter } onMouseOver={ props.FrametwoothreeonMouseOver } onKeyPress={ props.FrametwoothreeonKeyPress } onDrag={ props.FrametwoothreeonDrag } onMouseLeave={ props.FrametwoothreeonMouseLeave } onMouseUp={ props.FrametwoothreeonMouseUp } onMouseDown={ props.FrametwoothreeonMouseDown } onKeyDown={ props.FrametwoothreeonKeyDown } onChange={ props.FrametwoothreeonChange } ondelay={ props.Frametwoothreeondelay }>

              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frametwoofour']?.animationClass || {}}>

              <div id="id_oneonezero_twoothreethree" className={` frame frametwoofour ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrametwoofourStyle , transitionDuration: transaction['frametwoofour']?.duration, transitionTimingFunction: transaction['frametwoofour']?.timingFunction } } onClick={ props.FrametwoofouronClick } onMouseEnter={ props.FrametwoofouronMouseEnter } onMouseOver={ props.FrametwoofouronMouseOver } onKeyPress={ props.FrametwoofouronKeyPress } onDrag={ props.FrametwoofouronDrag } onMouseLeave={ props.FrametwoofouronMouseLeave } onMouseUp={ props.FrametwoofouronMouseUp } onMouseDown={ props.FrametwoofouronMouseDown } onKeyDown={ props.FrametwoofouronKeyDown } onChange={ props.FrametwoofouronChange } ondelay={ props.Frametwoofourondelay }>

              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['frameeight']?.animationClass || {}}>

              <div id="id_oneonezero_twoothreesix" className={` frame frameeight cursor ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FrameeightStyle , transitionDuration: transaction['frameeight']?.duration, transitionTimingFunction: transaction['frameeight']?.timingFunction } } onClick={ props.FrameeightonClick } onMouseEnter={ props.FrameeightonMouseEnter } onMouseOver={ props.FrameeightonMouseOver } onKeyPress={ props.FrameeightonKeyPress } onDrag={ props.FrameeightonDrag || function(e){ SingletoneNavigation.setTransitionInstance({"type":"SMART_ANIMATE","easing":{"type":"EASE_OUT"},"duration":0.3}, 'Desktopfour' ); history.push('/3'); }} onMouseLeave={ props.FrameeightonMouseLeave } onMouseUp={ props.FrameeightonMouseUp } onMouseDown={ props.FrameeightonMouseDown } onKeyDown={ props.FrameeightonKeyDown } onChange={ props.FrameeightonChange } ondelay={ props.Frameeightondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['actualizar']?.animationClass || {}}>

                  <span id="id_oneonezero_twoothreeseven"  className={` text actualizar    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.ACTUALIZARStyle , transitionDuration: transaction['actualizar']?.duration, transitionTimingFunction: transaction['actualizar']?.timingFunction }} onClick={ props.ACTUALIZARonClick } onMouseEnter={ props.ACTUALIZARonMouseEnter } onMouseOver={ props.ACTUALIZARonMouseOver } onKeyPress={ props.ACTUALIZARonKeyPress } onDrag={ props.ACTUALIZARonDrag } onMouseLeave={ props.ACTUALIZARonMouseLeave } onMouseUp={ props.ACTUALIZARonMouseUp } onMouseDown={ props.ACTUALIZARonMouseDown } onKeyDown={ props.ACTUALIZARonKeyDown } onChange={ props.ACTUALIZARonChange } ondelay={ props.ACTUALIZARondelay } >{props.ACTUALIZAR0 || `ACTUALIZAR`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['framethreethree']?.animationClass || {}}>

              <div id="id_oneonezero_twoofourthree" className={` frame framethreethree ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FramethreethreeStyle , transitionDuration: transaction['framethreethree']?.duration, transitionTimingFunction: transaction['framethreethree']?.timingFunction } } onClick={ props.FramethreethreeonClick } onMouseEnter={ props.FramethreethreeonMouseEnter } onMouseOver={ props.FramethreethreeonMouseOver } onKeyPress={ props.FramethreethreeonKeyPress } onDrag={ props.FramethreethreeonDrag } onMouseLeave={ props.FramethreethreeonMouseLeave } onMouseUp={ props.FramethreethreeonMouseUp } onMouseDown={ props.FramethreethreeonMouseDown } onKeyDown={ props.FramethreethreeonKeyDown } onChange={ props.FramethreethreeonChange } ondelay={ props.Framethreethreeondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['nombre']?.animationClass || {}}>

                  <span id="id_oneonezero_twoofourfour"  className={` text nombre    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.NOMBREStyle , transitionDuration: transaction['nombre']?.duration, transitionTimingFunction: transaction['nombre']?.timingFunction }} onClick={ props.NOMBREonClick } onMouseEnter={ props.NOMBREonMouseEnter } onMouseOver={ props.NOMBREonMouseOver } onKeyPress={ props.NOMBREonKeyPress } onDrag={ props.NOMBREonDrag } onMouseLeave={ props.NOMBREonMouseLeave } onMouseUp={ props.NOMBREonMouseUp } onMouseDown={ props.NOMBREonMouseDown } onKeyDown={ props.NOMBREonKeyDown } onChange={ props.NOMBREonChange } ondelay={ props.NOMBREondelay } >{props.NOMBRE0 || `NOMBRE`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} classNames={transaction['framethreefour']?.animationClass || {}}>

              <div id="id_oneonezero_twoofourfive" className={` frame framethreefour ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FramethreefourStyle , transitionDuration: transaction['framethreefour']?.duration, transitionTimingFunction: transaction['framethreefour']?.timingFunction } } onClick={ props.FramethreefouronClick } onMouseEnter={ props.FramethreefouronMouseEnter } onMouseOver={ props.FramethreefouronMouseOver } onKeyPress={ props.FramethreefouronKeyPress } onDrag={ props.FramethreefouronDrag } onMouseLeave={ props.FramethreefouronMouseLeave } onMouseUp={ props.FramethreefouronMouseUp } onMouseDown={ props.FramethreefouronMouseDown } onKeyDown={ props.FramethreefouronKeyDown } onChange={ props.FramethreefouronChange } ondelay={ props.Framethreefourondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['actualizarregistro']?.animationClass || {}}>

                  <span id="id_oneonezero_twoofoursix"  className={` text actualizarregistro    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.ACTUALIZARREGISTROStyle , transitionDuration: transaction['actualizarregistro']?.duration, transitionTimingFunction: transaction['actualizarregistro']?.timingFunction }} onClick={ props.ACTUALIZARREGISTROonClick } onMouseEnter={ props.ACTUALIZARREGISTROonMouseEnter } onMouseOver={ props.ACTUALIZARREGISTROonMouseOver } onKeyPress={ props.ACTUALIZARREGISTROonKeyPress } onDrag={ props.ACTUALIZARREGISTROonDrag } onMouseLeave={ props.ACTUALIZARREGISTROonMouseLeave } onMouseUp={ props.ACTUALIZARREGISTROonMouseUp } onMouseDown={ props.ACTUALIZARREGISTROonMouseDown } onKeyDown={ props.ACTUALIZARREGISTROonKeyDown } onChange={ props.ACTUALIZARREGISTROonChange } ondelay={ props.ACTUALIZARREGISTROondelay } >{props.ACTUALIZARREGISTRO0 || `ACTUALIZAR REGISTRO`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
          </div>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Desktopseven.propTypes = {
    style: PropTypes.any,
COLEGIOJAPONESDEMORELOS0: PropTypes.any,
APELLIDO0: PropTypes.any,
EMAIL0: PropTypes.any,
TELEFONO0: PropTypes.any,
PUESTO0: PropTypes.any,
ACTUALIZAR0: PropTypes.any,
NOMBRE0: PropTypes.any,
ACTUALIZARREGISTRO0: PropTypes.any,
DesktopsevenonClick: PropTypes.any,
DesktopsevenonMouseEnter: PropTypes.any,
DesktopsevenonMouseOver: PropTypes.any,
DesktopsevenonKeyPress: PropTypes.any,
DesktopsevenonDrag: PropTypes.any,
DesktopsevenonMouseLeave: PropTypes.any,
DesktopsevenonMouseUp: PropTypes.any,
DesktopsevenonMouseDown: PropTypes.any,
DesktopsevenonKeyDown: PropTypes.any,
DesktopsevenonChange: PropTypes.any,
Desktopsevenondelay: PropTypes.any,
FrametwoonigthonClick: PropTypes.any,
FrametwoonigthonMouseEnter: PropTypes.any,
FrametwoonigthonMouseOver: PropTypes.any,
FrametwoonigthonKeyPress: PropTypes.any,
FrametwoonigthonDrag: PropTypes.any,
FrametwoonigthonMouseLeave: PropTypes.any,
FrametwoonigthonMouseUp: PropTypes.any,
FrametwoonigthonMouseDown: PropTypes.any,
FrametwoonigthonKeyDown: PropTypes.any,
FrametwoonigthonChange: PropTypes.any,
Frametwoonigthondelay: PropTypes.any,
COLEGIOJAPONESDEMORELOSonClick: PropTypes.any,
COLEGIOJAPONESDEMORELOSonMouseEnter: PropTypes.any,
COLEGIOJAPONESDEMORELOSonMouseOver: PropTypes.any,
COLEGIOJAPONESDEMORELOSonKeyPress: PropTypes.any,
COLEGIOJAPONESDEMORELOSonDrag: PropTypes.any,
COLEGIOJAPONESDEMORELOSonMouseLeave: PropTypes.any,
COLEGIOJAPONESDEMORELOSonMouseUp: PropTypes.any,
COLEGIOJAPONESDEMORELOSonMouseDown: PropTypes.any,
COLEGIOJAPONESDEMORELOSonKeyDown: PropTypes.any,
COLEGIOJAPONESDEMORELOSonChange: PropTypes.any,
COLEGIOJAPONESDEMORELOSondelay: PropTypes.any,
FramethreezeroonClick: PropTypes.any,
FramethreezeroonMouseEnter: PropTypes.any,
FramethreezeroonMouseOver: PropTypes.any,
FramethreezeroonKeyPress: PropTypes.any,
FramethreezeroonDrag: PropTypes.any,
FramethreezeroonMouseLeave: PropTypes.any,
FramethreezeroonMouseUp: PropTypes.any,
FramethreezeroonMouseDown: PropTypes.any,
FramethreezeroonKeyDown: PropTypes.any,
FramethreezeroonChange: PropTypes.any,
Framethreezeroondelay: PropTypes.any,
NotojapanesebargainbuttononClick: PropTypes.any,
NotojapanesebargainbuttononMouseEnter: PropTypes.any,
NotojapanesebargainbuttononMouseOver: PropTypes.any,
NotojapanesebargainbuttononKeyPress: PropTypes.any,
NotojapanesebargainbuttononDrag: PropTypes.any,
NotojapanesebargainbuttononMouseLeave: PropTypes.any,
NotojapanesebargainbuttononMouseUp: PropTypes.any,
NotojapanesebargainbuttononMouseDown: PropTypes.any,
NotojapanesebargainbuttononKeyDown: PropTypes.any,
NotojapanesebargainbuttononChange: PropTypes.any,
Notojapanesebargainbuttonondelay: PropTypes.any,
VectoronClick: PropTypes.any,
VectoronMouseEnter: PropTypes.any,
VectoronMouseOver: PropTypes.any,
VectoronKeyPress: PropTypes.any,
VectoronDrag: PropTypes.any,
VectoronMouseLeave: PropTypes.any,
VectoronMouseUp: PropTypes.any,
VectoronMouseDown: PropTypes.any,
VectoronKeyDown: PropTypes.any,
VectoronChange: PropTypes.any,
Vectorondelay: PropTypes.any,
FramethreetwooonClick: PropTypes.any,
FramethreetwooonMouseEnter: PropTypes.any,
FramethreetwooonMouseOver: PropTypes.any,
FramethreetwooonKeyPress: PropTypes.any,
FramethreetwooonDrag: PropTypes.any,
FramethreetwooonMouseLeave: PropTypes.any,
FramethreetwooonMouseUp: PropTypes.any,
FramethreetwooonMouseDown: PropTypes.any,
FramethreetwooonKeyDown: PropTypes.any,
FramethreetwooonChange: PropTypes.any,
Framethreetwooondelay: PropTypes.any,
FrameonesevenonClick: PropTypes.any,
FrameonesevenonMouseEnter: PropTypes.any,
FrameonesevenonMouseOver: PropTypes.any,
FrameonesevenonKeyPress: PropTypes.any,
FrameonesevenonDrag: PropTypes.any,
FrameonesevenonMouseLeave: PropTypes.any,
FrameonesevenonMouseUp: PropTypes.any,
FrameonesevenonMouseDown: PropTypes.any,
FrameonesevenonKeyDown: PropTypes.any,
FrameonesevenonChange: PropTypes.any,
Frameonesevenondelay: PropTypes.any,
APELLIDOonClick: PropTypes.any,
APELLIDOonMouseEnter: PropTypes.any,
APELLIDOonMouseOver: PropTypes.any,
APELLIDOonKeyPress: PropTypes.any,
APELLIDOonDrag: PropTypes.any,
APELLIDOonMouseLeave: PropTypes.any,
APELLIDOonMouseUp: PropTypes.any,
APELLIDOonMouseDown: PropTypes.any,
APELLIDOonKeyDown: PropTypes.any,
APELLIDOonChange: PropTypes.any,
APELLIDOondelay: PropTypes.any,
FrameoneeightonClick: PropTypes.any,
FrameoneeightonMouseEnter: PropTypes.any,
FrameoneeightonMouseOver: PropTypes.any,
FrameoneeightonKeyPress: PropTypes.any,
FrameoneeightonDrag: PropTypes.any,
FrameoneeightonMouseLeave: PropTypes.any,
FrameoneeightonMouseUp: PropTypes.any,
FrameoneeightonMouseDown: PropTypes.any,
FrameoneeightonKeyDown: PropTypes.any,
FrameoneeightonChange: PropTypes.any,
Frameoneeightondelay: PropTypes.any,
EMAILonClick: PropTypes.any,
EMAILonMouseEnter: PropTypes.any,
EMAILonMouseOver: PropTypes.any,
EMAILonKeyPress: PropTypes.any,
EMAILonDrag: PropTypes.any,
EMAILonMouseLeave: PropTypes.any,
EMAILonMouseUp: PropTypes.any,
EMAILonMouseDown: PropTypes.any,
EMAILonKeyDown: PropTypes.any,
EMAILonChange: PropTypes.any,
EMAILondelay: PropTypes.any,
FrametwoozeroonClick: PropTypes.any,
FrametwoozeroonMouseEnter: PropTypes.any,
FrametwoozeroonMouseOver: PropTypes.any,
FrametwoozeroonKeyPress: PropTypes.any,
FrametwoozeroonDrag: PropTypes.any,
FrametwoozeroonMouseLeave: PropTypes.any,
FrametwoozeroonMouseUp: PropTypes.any,
FrametwoozeroonMouseDown: PropTypes.any,
FrametwoozeroonKeyDown: PropTypes.any,
FrametwoozeroonChange: PropTypes.any,
Frametwoozeroondelay: PropTypes.any,
TELEFONOonClick: PropTypes.any,
TELEFONOonMouseEnter: PropTypes.any,
TELEFONOonMouseOver: PropTypes.any,
TELEFONOonKeyPress: PropTypes.any,
TELEFONOonDrag: PropTypes.any,
TELEFONOonMouseLeave: PropTypes.any,
TELEFONOonMouseUp: PropTypes.any,
TELEFONOonMouseDown: PropTypes.any,
TELEFONOonKeyDown: PropTypes.any,
TELEFONOonChange: PropTypes.any,
TELEFONOondelay: PropTypes.any,
FrameonenigthonClick: PropTypes.any,
FrameonenigthonMouseEnter: PropTypes.any,
FrameonenigthonMouseOver: PropTypes.any,
FrameonenigthonKeyPress: PropTypes.any,
FrameonenigthonDrag: PropTypes.any,
FrameonenigthonMouseLeave: PropTypes.any,
FrameonenigthonMouseUp: PropTypes.any,
FrameonenigthonMouseDown: PropTypes.any,
FrameonenigthonKeyDown: PropTypes.any,
FrameonenigthonChange: PropTypes.any,
Frameonenigthondelay: PropTypes.any,
PUESTOonClick: PropTypes.any,
PUESTOonMouseEnter: PropTypes.any,
PUESTOonMouseOver: PropTypes.any,
PUESTOonKeyPress: PropTypes.any,
PUESTOonDrag: PropTypes.any,
PUESTOonMouseLeave: PropTypes.any,
PUESTOonMouseUp: PropTypes.any,
PUESTOonMouseDown: PropTypes.any,
PUESTOonKeyDown: PropTypes.any,
PUESTOonChange: PropTypes.any,
PUESTOondelay: PropTypes.any,
FrametwoooneonClick: PropTypes.any,
FrametwoooneonMouseEnter: PropTypes.any,
FrametwoooneonMouseOver: PropTypes.any,
FrametwoooneonKeyPress: PropTypes.any,
FrametwoooneonDrag: PropTypes.any,
FrametwoooneonMouseLeave: PropTypes.any,
FrametwoooneonMouseUp: PropTypes.any,
FrametwoooneonMouseDown: PropTypes.any,
FrametwoooneonKeyDown: PropTypes.any,
FrametwoooneonChange: PropTypes.any,
Frametwoooneondelay: PropTypes.any,
FrametwootwooonClick: PropTypes.any,
FrametwootwooonMouseEnter: PropTypes.any,
FrametwootwooonMouseOver: PropTypes.any,
FrametwootwooonKeyPress: PropTypes.any,
FrametwootwooonDrag: PropTypes.any,
FrametwootwooonMouseLeave: PropTypes.any,
FrametwootwooonMouseUp: PropTypes.any,
FrametwootwooonMouseDown: PropTypes.any,
FrametwootwooonKeyDown: PropTypes.any,
FrametwootwooonChange: PropTypes.any,
Frametwootwooondelay: PropTypes.any,
FrametwoothreeonClick: PropTypes.any,
FrametwoothreeonMouseEnter: PropTypes.any,
FrametwoothreeonMouseOver: PropTypes.any,
FrametwoothreeonKeyPress: PropTypes.any,
FrametwoothreeonDrag: PropTypes.any,
FrametwoothreeonMouseLeave: PropTypes.any,
FrametwoothreeonMouseUp: PropTypes.any,
FrametwoothreeonMouseDown: PropTypes.any,
FrametwoothreeonKeyDown: PropTypes.any,
FrametwoothreeonChange: PropTypes.any,
Frametwoothreeondelay: PropTypes.any,
FrametwoofouronClick: PropTypes.any,
FrametwoofouronMouseEnter: PropTypes.any,
FrametwoofouronMouseOver: PropTypes.any,
FrametwoofouronKeyPress: PropTypes.any,
FrametwoofouronDrag: PropTypes.any,
FrametwoofouronMouseLeave: PropTypes.any,
FrametwoofouronMouseUp: PropTypes.any,
FrametwoofouronMouseDown: PropTypes.any,
FrametwoofouronKeyDown: PropTypes.any,
FrametwoofouronChange: PropTypes.any,
Frametwoofourondelay: PropTypes.any,
FrameeightonClick: PropTypes.any,
FrameeightonMouseEnter: PropTypes.any,
FrameeightonMouseOver: PropTypes.any,
FrameeightonKeyPress: PropTypes.any,
FrameeightonDrag: PropTypes.any,
FrameeightonMouseLeave: PropTypes.any,
FrameeightonMouseUp: PropTypes.any,
FrameeightonMouseDown: PropTypes.any,
FrameeightonKeyDown: PropTypes.any,
FrameeightonChange: PropTypes.any,
Frameeightondelay: PropTypes.any,
ACTUALIZARonClick: PropTypes.any,
ACTUALIZARonMouseEnter: PropTypes.any,
ACTUALIZARonMouseOver: PropTypes.any,
ACTUALIZARonKeyPress: PropTypes.any,
ACTUALIZARonDrag: PropTypes.any,
ACTUALIZARonMouseLeave: PropTypes.any,
ACTUALIZARonMouseUp: PropTypes.any,
ACTUALIZARonMouseDown: PropTypes.any,
ACTUALIZARonKeyDown: PropTypes.any,
ACTUALIZARonChange: PropTypes.any,
ACTUALIZARondelay: PropTypes.any,
FramethreethreeonClick: PropTypes.any,
FramethreethreeonMouseEnter: PropTypes.any,
FramethreethreeonMouseOver: PropTypes.any,
FramethreethreeonKeyPress: PropTypes.any,
FramethreethreeonDrag: PropTypes.any,
FramethreethreeonMouseLeave: PropTypes.any,
FramethreethreeonMouseUp: PropTypes.any,
FramethreethreeonMouseDown: PropTypes.any,
FramethreethreeonKeyDown: PropTypes.any,
FramethreethreeonChange: PropTypes.any,
Framethreethreeondelay: PropTypes.any,
NOMBREonClick: PropTypes.any,
NOMBREonMouseEnter: PropTypes.any,
NOMBREonMouseOver: PropTypes.any,
NOMBREonKeyPress: PropTypes.any,
NOMBREonDrag: PropTypes.any,
NOMBREonMouseLeave: PropTypes.any,
NOMBREonMouseUp: PropTypes.any,
NOMBREonMouseDown: PropTypes.any,
NOMBREonKeyDown: PropTypes.any,
NOMBREonChange: PropTypes.any,
NOMBREondelay: PropTypes.any,
FramethreefouronClick: PropTypes.any,
FramethreefouronMouseEnter: PropTypes.any,
FramethreefouronMouseOver: PropTypes.any,
FramethreefouronKeyPress: PropTypes.any,
FramethreefouronDrag: PropTypes.any,
FramethreefouronMouseLeave: PropTypes.any,
FramethreefouronMouseUp: PropTypes.any,
FramethreefouronMouseDown: PropTypes.any,
FramethreefouronKeyDown: PropTypes.any,
FramethreefouronChange: PropTypes.any,
Framethreefourondelay: PropTypes.any,
ACTUALIZARREGISTROonClick: PropTypes.any,
ACTUALIZARREGISTROonMouseEnter: PropTypes.any,
ACTUALIZARREGISTROonMouseOver: PropTypes.any,
ACTUALIZARREGISTROonKeyPress: PropTypes.any,
ACTUALIZARREGISTROonDrag: PropTypes.any,
ACTUALIZARREGISTROonMouseLeave: PropTypes.any,
ACTUALIZARREGISTROonMouseUp: PropTypes.any,
ACTUALIZARREGISTROonMouseDown: PropTypes.any,
ACTUALIZARREGISTROonKeyDown: PropTypes.any,
ACTUALIZARREGISTROonChange: PropTypes.any,
ACTUALIZARREGISTROondelay: PropTypes.any
}
export default Desktopseven;