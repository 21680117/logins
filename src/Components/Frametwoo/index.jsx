import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Frametwoo.css'





const Frametwoo = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} classNames={transaction['frametwoo']?.animationClass || {}}>

    <div id="id_oneseveneight_twoo" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } frametwoo C_oneseveneight_twoo ${ props.cssClass } `} style={ { ...{ ...{}, transitionDuration: transaction['frametwoo']?.duration, transitionTimingFunction: transaction['frametwoo']?.timingFunction }, ...props.style }} onClick={ props.FrametwooonClick } onMouseEnter={ props.FrametwooonMouseEnter } onMouseOver={ props.FrametwooonMouseOver } onKeyPress={ props.FrametwooonKeyPress } onDrag={ props.FrametwooonDrag } onMouseLeave={ props.FrametwooonMouseLeave } onMouseUp={ props.FrametwooonMouseUp } onMouseDown={ props.FrametwooonMouseDown } onKeyDown={ props.FrametwooonKeyDown } onChange={ props.FrametwooonChange } ondelay={ props.Frametwooondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} classNames={transaction['framefour']?.animationClass || {}}>

          <div id="id_onefive_twoofour" className={` frame framefour ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.FramefourStyle , transitionDuration: transaction['framefour']?.duration, transitionTimingFunction: transaction['framefour']?.timingFunction } } onClick={ props.FramefouronClick } onMouseEnter={ props.FramefouronMouseEnter } onMouseOver={ props.FramefouronMouseOver } onKeyPress={ props.FramefouronKeyPress } onDrag={ props.FramefouronDrag } onMouseLeave={ props.FramefouronMouseLeave } onMouseUp={ props.FramefouronMouseUp } onMouseDown={ props.FramefouronMouseDown } onKeyDown={ props.FramefouronKeyDown } onChange={ props.FramefouronChange } ondelay={ props.Framefourondelay }>
            <CSSTransition in={_in} appear={true} classNames={transaction['mingcuteuserfourfill']?.animationClass || {}}>

              <div id="id_onefive_twoozero" className={` frame mingcuteuserfourfill ${ props.onClick ? 'cursor' : '' } `} style={ { ...{}, ...props.MingcuteuserfourfillStyle , transitionDuration: transaction['mingcuteuserfourfill']?.duration, transitionTimingFunction: transaction['mingcuteuserfourfill']?.timingFunction } } onClick={ props.MingcuteuserfourfillonClick } onMouseEnter={ props.MingcuteuserfourfillonMouseEnter } onMouseOver={ props.MingcuteuserfourfillonMouseOver } onKeyPress={ props.MingcuteuserfourfillonKeyPress } onDrag={ props.MingcuteuserfourfillonDrag } onMouseLeave={ props.MingcuteuserfourfillonMouseLeave } onMouseUp={ props.MingcuteuserfourfillonMouseUp } onMouseDown={ props.MingcuteuserfourfillonMouseDown } onKeyDown={ props.MingcuteuserfourfillonKeyDown } onChange={ props.MingcuteuserfourfillonChange } ondelay={ props.Mingcuteuserfourfillondelay }>
                <CSSTransition in={_in} appear={true} classNames={transaction['group']?.animationClass || {}}>

                  <div id="id_onefive_twooone" className={` group group ${ props.onClick ? 'cursor' : '' } `} style={{ ...{},  ...props.GroupStyle , transitionDuration: transaction['group']?.duration, transitionTimingFunction: transaction['group']?.timingFunction }} onClick={ props.GrouponClick } onMouseEnter={ props.GrouponMouseEnter } onMouseOver={ props.GrouponMouseOver } onKeyPress={ props.GrouponKeyPress } onDrag={ props.GrouponDrag } onMouseLeave={ props.GrouponMouseLeave } onMouseUp={ props.GrouponMouseUp } onMouseDown={ props.GrouponMouseDown } onKeyDown={ props.GrouponKeyDown } onChange={ props.GrouponChange } ondelay={ props.Groupondelay }>
                    <CSSTransition in={_in} appear={true} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_onefive_twootwoo" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="200" height="200">


                      </svg>
                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_onefive_twoothree" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="166.6666259765625" height="166.6666259765625">

                        <path d="M83.3333 0C37.3083 0 0 37.3083 0 83.3333C0 129.358 37.3083 166.667 83.3333 166.667C129.358 166.667 166.667 129.358 166.667 83.3333C166.667 37.3083 129.358 0 83.3333 0ZM54.1667 62.5C54.1667 58.6698 54.9211 54.8771 56.3868 51.3384C57.8526 47.7997 60.001 44.5844 62.7094 41.8761C65.4178 39.1677 68.6331 37.0193 72.1717 35.5535C75.7104 34.0878 79.5031 33.3333 83.3333 33.3333C87.1636 33.3333 90.9563 34.0878 94.4949 35.5535C98.0336 37.0193 101.249 39.1677 103.957 41.8761C106.666 44.5844 108.814 47.7997 110.28 51.3384C111.746 54.8771 112.5 58.6698 112.5 62.5C112.5 70.2355 109.427 77.6541 103.957 83.124C98.4875 88.5938 91.0688 91.6667 83.3333 91.6667C75.5979 91.6667 68.1792 88.5938 62.7094 83.124C57.2396 77.6541 54.1667 70.2355 54.1667 62.5ZM135.483 124.867C129.245 132.714 121.315 139.05 112.285 143.402C103.254 147.754 93.3576 150.009 83.3333 150C73.3091 150.009 63.4122 147.754 54.3819 143.402C45.3517 139.05 37.4214 132.714 31.1833 124.867C44.6917 115.175 63.125 108.333 83.3333 108.333C103.542 108.333 121.975 115.175 135.483 124.867Z" />
                      </svg>
                    </CSSTransition>
                  </div>

                </CSSTransition>
              </div>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} classNames={transaction['framefive']?.animationClass || {}}>
          <input id="id_onefive_twoofive" className="framefive   " onClick={ props.FramefiveonClick } onMouseEnter={ props.FramefiveonMouseEnter } onMouseOver={ props.FramefiveonMouseOver } onKeyPress={ props.FramefiveonKeyPress } onDrag={ props.FramefiveonDrag } onMouseLeave={ props.FramefiveonMouseLeave } onMouseUp={ props.FramefiveonMouseUp } onMouseDown={ props.FramefiveonMouseDown } onKeyDown={ props.FramefiveonKeyDown } onChange={ props.FramefiveonChange || function(e){ dispatchForm({ payload: { key: e.target.name, value: e.target.value } }) }} ondelay={ props.Framefiveondelay } style={ props.FramefiveStyle || {}} type="text" required="true" placeholder="USUARIO" pattern="ERROR" title="USUARIO" value="USUARIO" name="USUARIO" onInvalid={e=> e.target.setCustomValidity('USUARIO')} onInput={e => e.target.setCustomValidity('')} value={globalStateForm['USUARIO']} />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} classNames={transaction['framesix']?.animationClass || {}}>
          <input id="id_onefive_twooeight" className="framesix   " onClick={ props.FramesixonClick } onMouseEnter={ props.FramesixonMouseEnter } onMouseOver={ props.FramesixonMouseOver } onKeyPress={ props.FramesixonKeyPress } onDrag={ props.FramesixonDrag } onMouseLeave={ props.FramesixonMouseLeave } onMouseUp={ props.FramesixonMouseUp } onMouseDown={ props.FramesixonMouseDown } onKeyDown={ props.FramesixonKeyDown } onChange={ props.FramesixonChange || function(e){ dispatchForm({ payload: { key: e.target.name, value: e.target.value } }) }} ondelay={ props.Framesixondelay } style={ props.FramesixStyle || {}} type="password" required="true" placeholder="1234" pattern="ERROR" title="CONTRASEÑA" value="CONTRASEÑA" name="CONTRASEÑA" onInvalid={e=> e.target.setCustomValidity('CONTRASEÑA')} onInput={e => e.target.setCustomValidity('')} value={globalStateForm['CONTRASEÑA']} />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} classNames={transaction['frameseven']?.animationClass || {}}>

          <button id="id_onefive_threezero" className="frameseven    cursor" style={ props.FramesevenStyle || {}} type={'submit'} onClick={ props.FramesevenonClick || function(e){ SingletoneNavigation.setTransitionInstance(null, 'Desktopfive' ); history.push('/4'); }} onMouseEnter={ props.FramesevenonMouseEnter } onMouseOver={ props.FramesevenonMouseOver } onKeyPress={ props.FramesevenonKeyPress } onDrag={ props.FramesevenonDrag } onMouseLeave={ props.FramesevenonMouseLeave } onMouseUp={ props.FramesevenonMouseUp } onMouseDown={ props.FramesevenonMouseDown } onKeyDown={ props.FramesevenonKeyDown } onChange={ props.FramesevenonChange } ondelay={ props.Framesevenondelay }>
            <CSSTransition in={_in} appear={true} classNames={transaction['login']?.animationClass || {}}>

              <span id="id_onefive_threeone"  className={` text login    ${ props.onClick ? 'cursor' : ''}  `} style={{ ...{},  ...props.LOGINStyle , transitionDuration: transaction['login']?.duration, transitionTimingFunction: transaction['login']?.timingFunction }} onClick={ props.LOGINonClick } onMouseEnter={ props.LOGINonMouseEnter } onMouseOver={ props.LOGINonMouseOver } onKeyPress={ props.LOGINonKeyPress } onDrag={ props.LOGINonDrag } onMouseLeave={ props.LOGINonMouseLeave } onMouseUp={ props.LOGINonMouseUp } onMouseDown={ props.LOGINonMouseDown } onKeyDown={ props.LOGINonKeyDown } onChange={ props.LOGINonChange } ondelay={ props.LOGINondelay } >{props.LOGIN0 || `LOGIN`}</span>

            </CSSTransition>
          </button>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Frametwoo.propTypes = {
    style: PropTypes.any,
LOGIN0: PropTypes.any,
FrametwooonClick: PropTypes.any,
FrametwooonMouseEnter: PropTypes.any,
FrametwooonMouseOver: PropTypes.any,
FrametwooonKeyPress: PropTypes.any,
FrametwooonDrag: PropTypes.any,
FrametwooonMouseLeave: PropTypes.any,
FrametwooonMouseUp: PropTypes.any,
FrametwooonMouseDown: PropTypes.any,
FrametwooonKeyDown: PropTypes.any,
FrametwooonChange: PropTypes.any,
Frametwooondelay: PropTypes.any,
FramefouronClick: PropTypes.any,
FramefouronMouseEnter: PropTypes.any,
FramefouronMouseOver: PropTypes.any,
FramefouronKeyPress: PropTypes.any,
FramefouronDrag: PropTypes.any,
FramefouronMouseLeave: PropTypes.any,
FramefouronMouseUp: PropTypes.any,
FramefouronMouseDown: PropTypes.any,
FramefouronKeyDown: PropTypes.any,
FramefouronChange: PropTypes.any,
Framefourondelay: PropTypes.any,
MingcuteuserfourfillonClick: PropTypes.any,
MingcuteuserfourfillonMouseEnter: PropTypes.any,
MingcuteuserfourfillonMouseOver: PropTypes.any,
MingcuteuserfourfillonKeyPress: PropTypes.any,
MingcuteuserfourfillonDrag: PropTypes.any,
MingcuteuserfourfillonMouseLeave: PropTypes.any,
MingcuteuserfourfillonMouseUp: PropTypes.any,
MingcuteuserfourfillonMouseDown: PropTypes.any,
MingcuteuserfourfillonKeyDown: PropTypes.any,
MingcuteuserfourfillonChange: PropTypes.any,
Mingcuteuserfourfillondelay: PropTypes.any,
GrouponClick: PropTypes.any,
GrouponMouseEnter: PropTypes.any,
GrouponMouseOver: PropTypes.any,
GrouponKeyPress: PropTypes.any,
GrouponDrag: PropTypes.any,
GrouponMouseLeave: PropTypes.any,
GrouponMouseUp: PropTypes.any,
GrouponMouseDown: PropTypes.any,
GrouponKeyDown: PropTypes.any,
GrouponChange: PropTypes.any,
Groupondelay: PropTypes.any,
VectoronClick: PropTypes.any,
VectoronMouseEnter: PropTypes.any,
VectoronMouseOver: PropTypes.any,
VectoronKeyPress: PropTypes.any,
VectoronDrag: PropTypes.any,
VectoronMouseLeave: PropTypes.any,
VectoronMouseUp: PropTypes.any,
VectoronMouseDown: PropTypes.any,
VectoronKeyDown: PropTypes.any,
VectoronChange: PropTypes.any,
Vectorondelay: PropTypes.any,
FramefiveonClick: PropTypes.any,
FramefiveonMouseEnter: PropTypes.any,
FramefiveonMouseOver: PropTypes.any,
FramefiveonKeyPress: PropTypes.any,
FramefiveonDrag: PropTypes.any,
FramefiveonMouseLeave: PropTypes.any,
FramefiveonMouseUp: PropTypes.any,
FramefiveonMouseDown: PropTypes.any,
FramefiveonKeyDown: PropTypes.any,
FramefiveonChange: PropTypes.any,
Framefiveondelay: PropTypes.any,
FramesixonClick: PropTypes.any,
FramesixonMouseEnter: PropTypes.any,
FramesixonMouseOver: PropTypes.any,
FramesixonKeyPress: PropTypes.any,
FramesixonDrag: PropTypes.any,
FramesixonMouseLeave: PropTypes.any,
FramesixonMouseUp: PropTypes.any,
FramesixonMouseDown: PropTypes.any,
FramesixonKeyDown: PropTypes.any,
FramesixonChange: PropTypes.any,
Framesixondelay: PropTypes.any,
FramesevenonClick: PropTypes.any,
FramesevenonMouseEnter: PropTypes.any,
FramesevenonMouseOver: PropTypes.any,
FramesevenonKeyPress: PropTypes.any,
FramesevenonDrag: PropTypes.any,
FramesevenonMouseLeave: PropTypes.any,
FramesevenonMouseUp: PropTypes.any,
FramesevenonMouseDown: PropTypes.any,
FramesevenonKeyDown: PropTypes.any,
FramesevenonChange: PropTypes.any,
Framesevenondelay: PropTypes.any,
LOGINonClick: PropTypes.any,
LOGINonMouseEnter: PropTypes.any,
LOGINonMouseOver: PropTypes.any,
LOGINonKeyPress: PropTypes.any,
LOGINonDrag: PropTypes.any,
LOGINonMouseLeave: PropTypes.any,
LOGINonMouseUp: PropTypes.any,
LOGINonMouseDown: PropTypes.any,
LOGINonKeyDown: PropTypes.any,
LOGINonChange: PropTypes.any,
LOGINondelay: PropTypes.any
}
export default Frametwoo;