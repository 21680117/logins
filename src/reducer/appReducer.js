import InitialState from '../utils/InitialStates.js';

export const initialState = { ...InitialState };

export const allStates = (state = initialState, action) => {
  const { key, value } = action.payload;
  let newData = { ...state };
  newData[key] = value;
  return newData;
}

export const reducerSession = (state = initialState, action) => {
  const { key, value } = action.payload;
  let newData = { ...state };
  newData[key] = value;
  return newData;
}

export const reducerOverlay = (state = initialState, action) => {
 
  let newData = action ? { ...action } : false;
  return newData;
}