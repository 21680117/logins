import React, { Suspense } from 'react';
import {
    Router,
    Switch,
    Route,
    useLocation,
    Link
  } from "react-router-dom";




import LoadingComponent from "Components/Loading";

import Desktoptwoo from 'Components/Desktoptwoo';
import Desktopthree from 'Components/Desktopthree';
import Desktopfour from 'Components/Desktopfour';
import Desktopfive from 'Components/Desktopfive';
import Desktopsix from 'Components/Desktopsix';
import Desktopseven from 'Components/Desktopseven';
import Desktopeight from 'Components/Desktopeight';
import Desktopone from 'Components/Desktopone';




const Layout = (props)=>{
    let location = useLocation();
    React.useEffect(() => {
        window.scrollTo(0, 0)
    }, [location]);
    return (
        
        
          
                     
        <Switch>
         <Route    component={Desktoptwoo} path='/1' exact={true} />
 <Route    component={Desktopthree} path='/2' exact={true} />
 <Route    component={Desktopfour} path='/3' exact={true} />
 <Route    component={Desktopfive} path='/4' exact={true} />
 <Route    component={Desktopsix} path='/5' exact={true} />
 <Route    component={Desktopseven} path='/6' exact={true} />
 <Route    component={Desktopeight} path='/7' exact={true} />
 <Route    component={Desktopone} path='/' exact={true} />             
        </Switch>   
        
         
        
        
   
    )
};


const MobileLayout = (props)=>{
    let location = useLocation();
    React.useEffect(() => {
        window.scrollTo(0, 0)
    }, [location]);
    return (
        
        
          
                     
        <Switch>
                     
        </Switch>
            
        
         
        
        
    )
};

export default Layout;
