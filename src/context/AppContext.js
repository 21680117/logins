import React from 'react';
import { useAuth0 } from "@auth0/auth0-react";
import { reducerSession } from 'reducer/appReducer';
import { getUserData } from 'utils/User';
import Loading from 'Components/Loading';
const AppContext = React.createContext([{ }, () => {}]);
const SessionContextContext = React.createContext({});

export const useAppContext = () => React.useContext(AppContext);
export const useSessionContext = () => React.useContext(SessionContextContext);



const SessionProvider = ({ children, state, reducer})=>{
  const [ SessionContext, dispatchSessionContext ] = React.useReducer(reducer, state);
  const storeSession = React.useMemo(() => ([SessionContext, dispatchSessionContext]), [SessionContext]);
  React.useEffect(()=>{
    dispatchSessionContext({ payload: { key: 'user', value: state} })
  },[state])
  if(!SessionContext.user?.email)
    return null;
  return (<SessionContextContext.Provider value={storeSession} >{children}</SessionContextContext.Provider>)
}
const AppProviderWithUser = ({ children, initialState, reducer })=>{
  const [globalState, dispatch] = React.useReducer(reducer, initialState);
  const store = React.useMemo(() => ([globalState, dispatch]), [globalState]);
  const [ _user, setUser] = React.useState(false);
  const { user, isAuthenticated, isLoading, loginWithRedirect } = useAuth0();
 
  React.useEffect(()=>{

    if(!isLoading && isAuthenticated && user.email){
      const newUser = { ..._user, ...user}
      setUser(newUser);
      getUserData(user.email).then(data=>{
        setUser({ ...newUser, ...data });
      }).catch(e=>{})
    }

    if(!isLoading && !isAuthenticated)
      loginWithRedirect()
      
  }, [isLoading])

 
  return (
    <SessionProvider state={_user} reducer={reducerSession} >
      <AppContext.Provider value={store}>{children}</AppContext.Provider>
    </SessionProvider>
  )
}

const AppProviderWithOutUser = ({ children, initialState, reducer })=>{
  const [globalState, dispatch] = React.useReducer(reducer, initialState);
  const [ SessionContext, dispatchSessionContext ] = React.useReducer(reducerSession, {});
  const store = React.useMemo(() => ([globalState, dispatch]), [globalState]);
  const storeSession = React.useMemo(() => ([SessionContext, dispatchSessionContext]), [SessionContext]);

  return (<SessionContextContext.Provider value={storeSession} >
    <AppContext.Provider value={store}>{children}</AppContext.Provider>
  </SessionContextContext.Provider>)
}
export function AppProvider(props) {
  if(props.withUser)
    return <AppProviderWithUser {...props} />

  return <AppProviderWithOutUser {...props} />
  
}
