Binder puede ser utilizado de diferentes formas, en este texto vamos a mostrar la forma que el equipo de Corebooks recomienda.

# Primer Proyecto

Para iniciar deber copiar la base del repositorio

```bash
git clone git@gitlab.com:templates_books/binder.git
cd binder
npm i
```

Si usted usa MongoDB puede ejecutar el comando de abajo para generar el archivo de configuración de GraphQL, si usa otro servicio como Hasura entonces  puede descargar el archivo de configuración desde la página web.

```bash
gq https://_PROYECT_.hasura.app/v1/graphql -H "X-Hasura-Admin-Secret: _ADMIN_SECRET_" --introspect > schema.graphql
```

Por último pruebe que el sistema funcione

```bash
npm run start
```

Si no hay un error a la salida puede comenzar a configurar sus pantallas

## Configurar una Pantalla

Hasta este punto usted debe tener un proyecto que imite la siguiente estructura

```bash
.
├── Components
│   ├── HomeLayout
|   │   ├── HomeLayout.css
|   │   └── index.jsx
│   ├── LoadingComponent
|   │   ├── IMac1.css
|   │   └── index.jsx
│   └── Auth0Provider.jsx
├── Layout
│   ├── Home.jsx
│   └── index.jsx
├── App.css
├── App.js
├── index.css
├── index.js
├── logo.svg
├── reportWebVitals.js
├── setupTest.js
├── schema.graphql
└── index.html
```

Reemplace la carpeta *./Components/* con las pantallas de su proyecto y genere el archivo de configuración *./Config.js*

Los archivos principales que usa Binder son **Config.js** y **Binder.js**. El primer archivo contiene la configuraciones como la ubicación de los componentes, la configuración del enviroment de GraphQL, configuración del Layout. El segundo contiene la configuración particular de cada componente, por lo que este debe ir en la ruta correspondiente a cada componente.

Ambos archivos tienen la estrucutra de un archivo JSON.

### Config.js

El archivo *Config.js* va a crear directorios, archivos de configuración y archivos javascript. A continuación se muestra una estructura base que deberá ser rellenada en función de las necesidades del proyecto

```javascript
const config= {
	Components:{
			componentName:{
							Path: 'path/to/component'
					},
				},
	Pages:{	},
	Layouts:{
		root:{
			PathUrl:'path/web/to/component'
			Component:'componentName'
				}
			},
	Env:'Env.json',
	EnvMutation: 'EnvMutation.json',
	Auth0Provider:{	},
	}
```

### Binder.js

El uso de archivos Binder.js es recomendable para los componentes que, de alguna forma, necesiten tener comunicación con GraphQL. Además si requiere generar algún cambio deberá generarlos en este archivo y volver a construir el proyecto.

```javascript
const config = () => ({
  SourceFile: "path/to/source/index.jsx",
  Name: "ComponentName",
  NameOutput: "ComponentName",
  PathEnvironment: "path/to/enviroment",
  FileNameOutput:"path/to/output/index.js",
  Variebles: {},
  Query: "''",
  SourceErrorComponent: "path/to/componentError/ErrorComponent.jsx",
  SourceLoadingComponent: "path/to/componentLoading/LoadingComponent.jsx",
  Component:{
    Attrs: {
      title: "'Title Name'"
    }
  },
  Bind: [
    {
      Resolve: "res_users",
      Type: "Single",
      ParentComponent: "Perfil",
      Component: {
        Name: "ProfileImage",
        Attrs: {
          buttonchangeimage: "${window.location.origin}/icons/Profile/buttonchangeimage@2x.png"
        },
        Props: {
          profileimage: "res_partner.x_picture_url"
        }
      }
    },
    {
      Resolve: "res_users",
      Type: "Single",
      ParentComponent: "Perfil",
      Component: {
        Name: "NameInputText",
        Attrs: {
          inputType: "'text'",
          inputPlaceholder: "'Nombre(s)'",
          usericon: "${window.location.origin}/icons/Profile/usericon@2x.png"
        }
      }
    },
    {
      Resolve: "res_users",
      Type: "Single",
      ParentComponent: "Perfil",
      Component: {
        Name: "LastnameInputText",
        Attrs: {
          inputType: "'text'",
          inputPlaceholder: "'Apellidos'",
          usericon: "${window.location.origin}/icons/Profile/usericon@2x.png"
        }
      }
    }
  ]
});
module.exports = config;
```

-----------

* query: El query que se utiliza para obtener información de hasura (se hace la consulta primero en hasura y luego se copia aca). Tiene como prefijo el nombre de la carpeta (Pages/FindWorkshopCategory) más la palabra Query
* config: Toda la configuración para que Binder pueda interpretar las instrucciones
* SourceFile: Componente que va a ser encapsulado por el archivo generado por binder
* Name: Igual que el nombre de la carpeta
* NameOutput: Igual que el nombre de la carpeta
* PathEnvironment: Archivo que se utiliza para realizar la petición (default)
* FileNameOutput: Ubicación donde se genera el archivo que encapsula el componente de anima y que hace la petición a hasura
* Variebles: 
* Query: La petición de graphql que se hace en hasura
* SourceErrorComponent: Componente que se muestra cuando hay un error.
* SourceLoadingComponent: Componente que se muestra al esperar a que termine de cargarse la información
* Component: Nos permite añadir propiedades al archivo que se genera y encapsula. Se puede revisar lo que regresa props para entender el resultado
* Attrs: Nos permite añadir propiedades de cualquier tipo
* Props: Nos permite añadir propiedades pero ya directamente desde el objeto que retorna la consulta de graphql (x_workshop.name para poner una propiedad con el valor de name)
* Bind: Se añaden los componentes que van a cambiar las propiedades default que entrega la maqueta por un valor dinámica de la base de datos (<MiComponente name="Test"/> al momento de ponerlo en Bind cambiar por <MiComponente name={x_workshop.name}/>

~~~javascript
    ```
    const query = `\`query FindWorkshopCategoryQuery($idCategory: Int!, $idState: Int! ) { 
        workshops:x_workshop(where: {x_subcategory_id: {_eq: $idCategory}, x_state: {_eq: $idState}}) {
            _id
            limitShedule
            name:x_name
            price:x_price
        }
    }   
    \``;
    const config = () => ({
      SourceFile: "Components/Search/index.jsx",
      Name:"FindWorkshopCategory",
      NameOutput : "FindWorkshopCategory",
      PathEnvironment: "Environment",
      FileNameOutput:"Pages/FindWorkshopCategory/index.js",
      PathEnvironment: "Environment",
      Variebles:{},
      Query: `graphql${query}`,
      SourceErrorComponent: "Components/ErrorComponent.jsx",
      SourceLoadingComponent: "Components/LoadingComponent",
      Component:{
        "Attrs": {
            isWorshop:'true'
        }
      },
      "Bind":[ ]
    });
    module.exports = config;
    ```
~~~

### Ejecución



Una vez terminado de configurar si archivo Binder esta listo para hacer las pruebas. En la ruta del componte que esta configurando ejecute.

```bash
binder
binder-component -d Binder.js
```

Cuando termine de configurar todos los componentes y haya configurado el archivo *Config.js*

```bash
binder
binder-app
```





Nota: Es necesario instalar las dependencias del proyecto, para ello ejecute.

```bash
yarn star
```

